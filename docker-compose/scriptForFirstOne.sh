#install git
sudo apt-get update
sudo apt-get install git -y
git clone https://AHS-Projects@bitbucket.org/AHS-Projects/exam.git
cd /home/exam
sudo git config --global credential.helper store
git pull https://AHS-Projects@bitbucket.org/AHS-Projects/exam.git
#to build spring
sudo apt-get update
sudo apt install maven -y
#install docker
sudo apt install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker
sudo apt-get install docker-compose

echo "install docker-ce"
sudo apt install apt-transport-https software-properties-common ca-certificates -y
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable" >/etc/apt/sources.list.d/docker-ce.list
sudo apt update
sudo apt install docker-ce -y
systemctl start docker
systemctl enable docker
docker swarm init --advertise-addr 127.0.0.1

sudo nano /etc/docker/daemon.json
#{
#"experimental": true
#}
#sudo systemctl restart docker
