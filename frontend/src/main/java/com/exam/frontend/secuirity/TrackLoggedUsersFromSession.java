package com.exam.frontend.secuirity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TrackLoggedUsersFromSession {

    @Autowired
    private SessionRegistry sessionRegistry;

    public List<String> getUsersFromSessionRegistry() {
        return sessionRegistry.getAllPrincipals()
                .stream()
                .filter((u) -> !sessionRegistry.getAllSessions(u, false)
                        .isEmpty())
                .map(o -> {
                    if (o instanceof User) {
                        return ((User) o).getUsername();
                    } else {
                        return o.toString()
                                ;
                    }
                }).collect(Collectors.toList());
    }
}
