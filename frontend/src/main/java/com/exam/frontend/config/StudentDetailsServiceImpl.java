package com.exam.frontend.config;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.exam.common.model.Tblstudent;
import com.exam.common.model.Tblteacher;
import com.exam.common.model.Tblteacherrole;
import com.exam.common.service.StudentService;
import com.exam.common.service.TeacherService;


@Service
public class StudentDetailsServiceImpl implements UserDetailsService {
 
    @Autowired
    private StudentService studentService;
 
 
    @Override
    public UserDetails loadUserByUsername(String studentName) throws UsernameNotFoundException {
        Tblstudent student = studentService.findStudentByName(studentName);
 //System.out.println("student ip addressssssssssssssssss: "+ getRequestRemoteAddr());
        if (student == null || student.getStudentID()==null || student.getBlocked() == 1) {
    
            throw new UsernameNotFoundException("مستخدم " + student + " لا يوجد");
        }

        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
  
 
        UserDetails userDetails = (UserDetails) new User(student.getStudentName(), 
        		student.getStudentPassword(), grantList);
 
        return userDetails;
    }
    
//    public static String getRequestRemoteAddr(){
//        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes())
//                   .getRequest(); 
//        return request.getRemoteAddr();
// }
}
 