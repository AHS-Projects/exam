package com.exam.frontend.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


import com.exam.common.model.Tblstudent;
import com.exam.common.service.StudentService;


public class BaseFrontEnd 
{
    @Autowired
    private StudentService service;
    
	public Tblstudent getCurrentStudent()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String studentName=auth.getName();
		Tblstudent tblstudent=service.findStudentByName(studentName);
		return tblstudent;
	}
	
// Encryte Password with BCryptPasswordEncoder
   public static String encrytePassword(String password) {
       BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
       return encoder.encode(password);
   }
   
  

}
