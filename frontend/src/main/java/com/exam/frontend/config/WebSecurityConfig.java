package com.exam.frontend.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    private LogoutSuccessHandler myLogoutSuccessHandler;
    @Autowired
    private AuthenticationSuccessHandler myAuthenticationSuccessHandler;

    //@Autowired
    //private AuthenticationFailureHandler authenticationFailureHandler;


    @Override
    protected void configure(HttpSecurity http) throws Exception {


        http
                .headers()
                .frameOptions().sameOrigin()
                .and()
                .authorizeRequests()
                .antMatchers("/*").authenticated()
                //.antMatchers("/department/**","/section/**","/quantity/**","/product/**",
                //		"/subdepartment/**","/subdepartmentrest/**","/order/productorders/**"
                //		,"/orderrest/productorders/**").hasAnyRole("ADMIN","DEPARTMENT")
                // .antMatchers("/order/**","/orderrest/list").hasAnyRole("ADMIN","ORDER")
                // .antMatchers("/**").hasRole("ADMIN")
                .anyRequest().authenticated()

                .and()
                .formLogin()
//                .loginPage("/login")
//                .defaultSuccessUrl("/")
//                .failureUrl("/login?error=4")
                .loginPage("/login")
                .defaultSuccessUrl("/")
                .failureUrl("/login?error=4")
                .successHandler(myAuthenticationSuccessHandler)
                //.failureHandler(authenticationFailureHandler)
                //.authenticationDetailsSource(authenticationDetailsSource)
                .usernameParameter("username")
                .passwordParameter("password")
                .permitAll().and().
                sessionManagement()
                .invalidSessionUrl("/error")
                .maximumSessions(1).sessionRegistry(sessionRegistry()).and()
                .sessionFixation().none()

                .and()
//                .logout()
//                .logoutSuccessUrl("/login?logout=true")
//                .invalidateHttpSession(true)
                .logout()
                .logoutSuccessHandler(myLogoutSuccessHandler)
                .invalidateHttpSession(false)
                .logoutSuccessUrl("/login?logout=true")
                .deleteCookies("JSESSIONID")
                .permitAll()
                .and()
                .exceptionHandling() //exception handling configuration
                .accessDeniedPage("/error")
                .and()
                .csrf()
                .disable();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authenticationMgr) throws Exception {
//		 authenticationMgr.inMemoryAuthentication()
//	        .passwordEncoder(passwordEncoder)
//	        .withUser("hossamhany").password(passwordEncoder.encode("soofy12345")).roles("USER")
//	        .and()
//	        .withUser("alaahassan").password(passwordEncoder.encode("soofy12345")).roles("USER", "ADMIN");
        authenticationMgr.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);

    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
//			web.ignoring().antMatchers("/*.css");
//			web.ignoring().antMatchers("/*.js");
        web.ignoring().antMatchers("/resources/**");
    }


}
