package com.exam.frontend.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.exam.common.model.Tblstudent;
import com.exam.frontend.config.BaseFrontEnd;

@Controller
@RequestMapping("/student")
public class StudentController extends BaseFrontEnd{

	@Autowired
	BaseFrontEnd baseFrontEnd;
	
	@GetMapping("/studentHistoryDegrees")
	public String studentModelsHistory(Model theModel, HttpServletRequest request) {
	
		Tblstudent student =  getCurrentStudent();
		theModel.addAttribute("studentName", student.getStudentName());
		
				//List<Tblstudentanswer> studentMarks = studentService.getStudentMarks(student.getStudentID());
	//	theModel.addAttribute("studentMarks", studentMarks);
		return "studentHistoryDegrees";
	} 
	
	@GetMapping("/studentModel")
	public String studentModel(Model theModel, HttpServletRequest request) {
	
		Tblstudent student = baseFrontEnd.getCurrentStudent();
		theModel.addAttribute("studentName", student.getStudentName());
		return "studentModel";
	}

}
