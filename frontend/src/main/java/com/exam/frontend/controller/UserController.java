package com.exam.frontend.controller;

import com.exam.frontend.secuirity.ActiveUserStore;
import com.exam.frontend.secuirity.LoggedUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;
import java.util.Locale;

@Controller
public class UserController {

    @Autowired
    LoggedUsers loggedUsers;
    @Autowired
    ActiveUserStore activeUserStore;

    @GetMapping("/loggedUsers")
    public String getLoggedUsers(final Locale locale, Model model) {
        int usersSize = loggedUsers.getUsersSize();
        model.addAttribute("users", activeUserStore.getUsers());
        if (usersSize == 0 || activeUserStore.getUsers().size() > usersSize) {
            loggedUsers.setUsersSize(activeUserStore.getUsers().size());
            Date userDate = new Date();
            loggedUsers.setDate(userDate);
        }
        model.addAttribute("usersSize", loggedUsers.getUsersSize());
        model.addAttribute("usersDate", loggedUsers.getDate());


        return "users";
    }
}