package com.exam.frontend.controller;

import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblmodel;
import com.exam.common.model.Tblstudent;
import com.exam.common.service.ModelService;
import com.exam.common.service.StudentService;
import com.exam.frontend.config.BaseFrontEnd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
public class MainController extends BaseFrontEnd {

    @Autowired
    BaseFrontEnd baseFrontEnd;

    @Autowired
    StudentService studentService;
    @Autowired
    ModelService modelService;

    @GetMapping("/")
    public String Index(Model theModel, HttpServletRequest request) {
        Tblstudent student = getCurrentStudent();

        int totalDailyModels = 0;
        int totalMonthlyModels = 0;
        int nextModels = 0;
        double studentMark = 0;
        double studentDailyModelMarks = 0;
        double studentMonthlyModelMarks = 0;
        double dailyModelMarks = 0;
        double monthlyModelMarks = 0;
        System.out.println("student id indeeeex: " + student.getStudentID());
        List<Tblgroupseries> studentMarks = studentService.getAllStudentModels(student.getStudentID());
        if (studentMarks != null) {
            for (Tblgroupseries tblgroupseries : studentMarks) {

                Tblmodel model = modelService.getModelByID(tblgroupseries.getModelID().getModelID());

                if (model.getIsPublished() == 1) {
                    if (tblgroupseries.getIsModelFinished() == 1) {
                        studentMark += tblgroupseries.getStudentMark();
                        if (model.getModelType().equals("daily")) {
                            studentDailyModelMarks += tblgroupseries.getStudentMark();
                            dailyModelMarks += model.getModelMark();
                            totalDailyModels++;
                        } else if (model.getModelType().equals("monthly") && tblgroupseries.getIsModelCorrected() != null) {
                            studentMonthlyModelMarks += tblgroupseries.getStudentMark();

                            monthlyModelMarks += model.getModelMark();

                            totalMonthlyModels++;
                        }
                    } else if (tblgroupseries.getIsModelFinished() == 0) {
                        nextModels++;
                    }
                }
            }
        }
        //studentMark


        double studMonthModelRes = studentMonthlyModelMarks / monthlyModelMarks;
        studMonthModelRes *= 100;
        studMonthModelRes = Math.floor(studMonthModelRes);
        double studDailyModelRes = studentDailyModelMarks / dailyModelMarks;
        studDailyModelRes *= 100;
        studDailyModelRes = Math.floor(studDailyModelRes);
        theModel.addAttribute("studMonthModel", studMonthModelRes);
        theModel.addAttribute("studDailyModel", studDailyModelRes);
        theModel.addAttribute("studentMark", studentMark);
        theModel.addAttribute("totalDailyModels", totalDailyModels);
        theModel.addAttribute("totalMonthlyModels", totalMonthlyModels);
        theModel.addAttribute("nextModels", nextModels);


        theModel.addAttribute("studentName", student.getStudentName());
        return "index";
    }

//	@GetMapping("/getStudentDegree/{date}")
//	public String StudentDegree(@PathVariable("date") String date, HttpServletRequest request, Model model) throws ParseException {
//	    SimpleDateFormat formatter5=new SimpleDateFormat("E MMM dd yyyy HH:mm:ss");  
//	    Date formattedDate=formatter5.parse(date);  
//
//		return "contact";
//	}

}
