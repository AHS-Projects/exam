package com.exam.frontend.controller;

import com.exam.common.dto.StudentMarks;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblmodel;
import com.exam.common.model.Tblstudent;
import com.exam.common.service.ModelService;
import com.exam.common.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/student/rest")
public class StudentRestController {

    @Autowired
    StudentService studentService;
    @Autowired
    ModelService modelService;

    @GetMapping("/studentMarks")
    public List<com.exam.common.dto.Model> studentMarks(Model theModel, HttpServletRequest request) throws ParseException {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String studentName = auth.getName();

        Tblstudent student = studentService.findStudentByName(studentName);
        List<com.exam.common.dto.Model> studentMarks = studentService.getStudentMarksByName(studentName);

        if (studentMarks == null) {
            return new ArrayList<com.exam.common.dto.Model>();
        }
//        List<StudentMarks> studMarks = new ArrayList<StudentMarks>();
//
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
//
//        // Note, MM is months, not mm
//        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
//        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        for (Tblgroupseries studentMarks2 : studentMarks) {
//
//            Tblmodel model = modelService.getModelWithSections(studentMarks2.getModelID().getModelID());
//
//            StudentMarks stdMrks = new StudentMarks();
//            stdMrks.setModelMark(model.getModelMark());
//            stdMrks.setModelType(model.getModelType());
//            stdMrks.setModelName(model.getModelName());
//
//            //start date
//            String inputText = studentMarks2.getStartDate().toString();
//            Date date = inputFormat.parse(inputText);
//            String outputText = outputFormat.format(date);
//            stdMrks.setStartDate(outputText);
//
//            //finish date
//            String inputText2 = studentMarks2.getFinishedDate().toString();
//            Date date2 = inputFormat.parse(inputText2);
//            String outputText2 = outputFormat.format(date2);
//            stdMrks.setFinishDate(outputText2);
//
//            if (studentMarks2.getStudentMark() != null) {
//                stdMrks.setStudentMark(studentMarks2.getStudentMark());
//            }
//            studMarks.add(stdMrks);
//        }
//
//        theModel.addAttribute("studentMarks", studentMarks);

        return studentMarks;

    }

}
