package com.exam.frontend.controller;

import com.exam.common.dto.Model;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblmodel;
import com.exam.common.model.Tblstudent;
import com.exam.common.service.GroupSeriesService;
import com.exam.common.service.ModelService;
import com.exam.common.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@RestController
@RequestMapping("/model/rest")
public class ModelRestController {
    @Autowired
    private ModelService modelService;
    @Autowired
    private StudentService studentService;
    @Autowired
    GroupSeriesService groupSeriesService;


    @RequestMapping(value = "/listPrev", method = RequestMethod.GET)
    public List<Model> modelsList() throws ParseException {


        List<Model> models = new ArrayList<>();
        List<Tblmodel> studentModels = new ArrayList<>();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String studentName = auth.getName();

        Tblstudent student = studentService.findStudentByName(studentName);
        List<Model> getstudentModels = modelService.findPrevModelsByStudName(studentName);
        if (getstudentModels == null) {
            return models;
        }


//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
//
//        // Note, MM is months, not mm
//        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
//        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        for (Tblgroupseries tblgroupseries : getstudentModels) {
//            Tblmodel model = modelService.getModelWithSections(tblgroupseries.getModelID().getModelID());
//            Model modelObj = new Model();
//
//
//            //Adding number of Days to the current date
//            LocalDate currentDate = LocalDate.now();
//            LocalDate modelStartedDate = null;
//            Calendar c = Calendar.getInstance();
//            c.setTime(tblgroupseries.getStartDate());
//            System.out.println("calendarrrr: " + c.getTime());
//            int month = c.get(Calendar.MONTH);
//            month += 1;
//
//            if (model.getModelType().equals("daily")) {
//                //Adding one Day to the given date
//
//
//                modelStartedDate = LocalDate.of(c.get(Calendar.YEAR), month, c.get(Calendar.DAY_OF_MONTH)).plusDays(2);
//                System.out.println("started date daily : " + modelStartedDate);
//            } else if (model.getModelType().equals("monthly")) {
//                //Adding one Day to the given date
//                modelStartedDate = LocalDate.of(c.get(Calendar.YEAR), month, c.get(Calendar.DAY_OF_MONTH)).plusDays(6);
//                System.out.println("started date monthly : " + modelStartedDate);
//            }
//
//
//            if (modelStartedDate.isBefore(currentDate)) {
//
//                String inputText = tblgroupseries.getStartDate().toString();
//                Date date = inputFormat.parse(inputText);
//                String outputText = outputFormat.format(date);
//
//                modelObj.setStartDate(outputText);
//                modelObj.setStudentMark(tblgroupseries.getStudentMark());
//                String inputText2 = tblgroupseries.getFinishedDate().toString();
//                Date date2 = inputFormat.parse(inputText2);
//                String outputText2 = outputFormat.format(date2);
//                modelObj.setFinishDate(outputText2);
//                modelObj.setModelID(model.getModelID());
//                models.add(modelObj);
//                studentModels.add(model);
//            }
//        }
//
//
//        for (Tblmodel tblmodel : studentModels) {
//
//            for (Model model : models) {
//
//                if (tblmodel.getModelID() == model.getModelID()) {
//                    model.setModelDesc(tblmodel.getModelDesc());
//                    model.setModelMark(tblmodel.getModelMark());
//                    model.setModelName(tblmodel.getModelName());
//                    //model.setStageName(tblmodel.getStageID().getStageName());
//                    model.setModelType(tblmodel.getModelType());
//                    //models.add(model);
//                }
//            }
//        }

        return getstudentModels;
    }

    @RequestMapping(value = "/listNext", method = RequestMethod.GET)
    public List<Model> modelsListNext() throws ParseException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String studentName = auth.getName();
        List<Model> models = modelService.findNextModelsByStudName(studentName);
//        List<Model> models = new ArrayList<>();

//        List<Tblmodel> studentModels = new ArrayList<>();

//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        String studentName = auth.getName();

//        Tblstudent student = studentService.findStudentByName(studentName);

//        List<Tblgroupseries> getstudentModels = modelService.getstudentNextModels(student.getStudentID());
//        if (getstudentModels == null) {
//            return models;
//        }
//        for (Tblgroupseries tblgroupseries : getstudentModels) {
//            Tblmodel model = modelService.getModelWithSections(tblgroupseries.getModelID().getModelID());
//            if (model.getIsPublished() == 1) {
//                Model modelObj = new Model();
//                if (tblgroupseries.getStartDate() != null) {
//                    modelObj.setStartDate(tblgroupseries.getStartDate().toString());
//                }
//                modelObj.setModelID(model.getModelID());
//                models.add(modelObj);
//                studentModels.add(model);
//            }
//        }


//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

//        // Note, MM is months, not mm
//        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
//        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//        for (Tblmodel tblmodel : studentModels) {
//            // Model model = new Model();

//            String inputText = tblmodel.getCreatedDate().toString();
//            Date date = inputFormat.parse(inputText);
//            String outputText = outputFormat.format(date);

//            for (Model model : models) {

//                if (model.getModelID() == tblmodel.getModelID()) {

//                    model.setCreatedDate(outputText);
//                    model.setModelDesc(tblmodel.getModelDesc());
//                    model.setModelName(tblmodel.getModelName());
//                    //model.setStageName(tblmodel.getStageID().getStageName());
//                    model.setModelType(tblmodel.getModelType());
//                    //models.add(model);
//                    //continue;
//                }
//            }
//        }

        return models;
    }

    @GetMapping("/isModelFinished/{id}")
    public int isModelFinished(@PathVariable("id") long modelID) {
        System.out.println("model iddddd inside backendMethod : " + modelID);
        Tblstudent student = getCurrentStudent();

        Tblstudent student2 = studentService.getStudentWithCurrentSession(student.getStudentID());
        // System.out.println("student group id : "+student2.getGroupID().getGroupID());
        Long groupID = student2.getGroupID().getGroupID();

        Tblgroupseries groupSeries = groupSeriesService
                .findGroupSeriesByStudentAndModelAndGroupWithCurrentSession(groupID, modelID, student2.getStudentID());
        return groupSeries.getIsModelFinished();
    }

    public Tblstudent getCurrentStudent() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String studentName = auth.getName();

        Tblstudent student = studentService.findStudentByName(studentName);
        return student;
    }
}
