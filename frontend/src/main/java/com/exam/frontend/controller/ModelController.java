package com.exam.frontend.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.exam.common.model.Tblchoice;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblmodel;
import com.exam.common.model.Tblquestion;
import com.exam.common.model.Tblsection;
import com.exam.common.model.Tblstudent;
import com.exam.common.model.Tblstudentanswer;
import com.exam.common.service.ChoiceService;
import com.exam.common.service.GroupSeriesService;
import com.exam.common.service.ModelService;
import com.exam.common.service.StudentAnswerService;
import com.exam.common.service.StudentService;

@Controller
@RequestMapping("/model")
public class ModelController {

	@Autowired
	StudentService studentService;
	@Autowired
	ModelService modelService;
	@Autowired
	StudentAnswerService studentAnswerService;
	@Autowired
	GroupSeriesService groupSeriesService;
	@Autowired
	ChoiceService choiceService;

	public Tblmodel initModel(Tblmodel model) {
		List<Tblsection> tblsectionList = model.getTblsectionList();
		for (Tblsection tblsection : tblsectionList) {
			List<Tblquestion> tblquestionList = tblsection.getTblquestionList();
			for (Tblquestion tblquestion : tblquestionList) {
				tblquestion.setTblstudentanswerList(new ArrayList<Tblstudentanswer>());
			}
		}
		return model;
	}

	public Tblmodel bindStudentAnswerModel(Tblmodel model, long studentID) {
		List<Tblstudentanswer> studAnswerList = new ArrayList<Tblstudentanswer>();
		List<Tblsection> tblsectionList = model.getTblsectionList();
		for (Tblsection tblsection : tblsectionList) {
			List<Tblquestion> tblquestionList = tblsection.getTblquestionList();
			for (Tblquestion tblquestion : tblquestionList) {
				if (tblquestion.getTblstudentanswerList() == null) {
					tblquestion.setTblstudentanswerList(new ArrayList<Tblstudentanswer>());
				} else {
					for (Tblstudentanswer tblstudentanswer : tblquestion.getTblstudentanswerList()) {
						if (tblstudentanswer.getStudentID().getStudentID() == studentID) {
							studAnswerList.add(tblstudentanswer);
						}
					}
					tblquestion.setTblstudentanswerList(studAnswerList);
					studAnswerList = new ArrayList<Tblstudentanswer>();
				}
			}
		}
		return model;
	}

	@GetMapping("/studentModelDetails/{id}/{modelOldOrNew}")
	public String studentModelsHistory(@PathVariable("id") long modelID,
			@PathVariable("modelOldOrNew") String modelOldOrNew, Model theModel, HttpServletRequest request)
			throws ParseException {

		Tblstudent student = getCurrentStudent();
		Tblmodel model = new Tblmodel();

		double studentMark = 0;

		if (modelOldOrNew.equals("prev")) {
			List<Tblgroupseries> getstudentModels = modelService.getstudentPrevModels(student.getStudentID());
			theModel.addAttribute("isModelFinished", "true");

			for (Tblgroupseries tblgroupseries : getstudentModels) {
				if (tblgroupseries.getIsModelFinished() == 1 && tblgroupseries.getModelID().getModelID() == modelID) {
					studentMark = tblgroupseries.getStudentMark();
					Tblmodel model2 = modelService.getModelWithSections(tblgroupseries.getModelID().getModelID());
					Tblmodel bindStudentAnswerModel = bindStudentAnswerModel(model2, student.getStudentID());
					model = bindStudentAnswerModel;
				}
			}
		} else if (modelOldOrNew.equals("next")) {
			Tblgroupseries tblgroupseries = modelService.getstudentNextModelsWithStuiDAndModelID(student.getStudentID(),modelID);
			theModel.addAttribute("isModelFinished", "false");

				if (tblgroupseries != null && tblgroupseries.getIsModelFinished() == 0 ) {
					   //Calendar cal = Calendar.getInstance(); // creates calendar
					    //cal.setTime(new Date()); // sets calendar time/date
					   // cal.add(Calendar.HOUR_OF_DAY, 2); // adds 2 hours
					 //System.out.println("curr date : "+cal.getTime()); // returns new date object, one hour in the future
					Tblmodel model2 = modelService.getModelWithSections(tblgroupseries.getModelID().getModelID());

					if (model2.getIsPublished() == 1) {
						Date currentDate= new Date();
						Tblmodel bindStudentAnswerModel = bindStudentAnswerModel(model2, student.getStudentID());
						model = bindStudentAnswerModel;
						if (tblgroupseries.getStartDate() == null) {
							tblgroupseries.setStartDate(new Date());
							groupSeriesService.updateGroupSeries(tblgroupseries);
							theModel.addAttribute("remainingTime", model2.getModelTime()*60);
						}else {
						Date startDate = tblgroupseries.getStartDate();	
						long diff = currentDate.getTime() - startDate.getTime();
						int diffsec = (int) (diff / (1000));
						int modelTiime = (int) (model2.getModelTime()*60);
						modelTiime = modelTiime - diffsec;
						theModel.addAttribute("remainingTime", modelTiime);
						}
					}
				}
			
		}

		theModel.addAttribute("studentMark", studentMark);
		theModel.addAttribute("studentName", student.getStudentName());
		theModel.addAttribute("studentID", student.getStudentID());
		theModel.addAttribute("studModel", model);
		theModel.addAttribute("modelOldOrNew", modelOldOrNew);
		if (model.getModelID() == null) {
			return "index";
		}
		if (model.getModelType().equals("monthly")) {
			return "studentMonthlyModelDetails";
		}
		return "studentModelDetails";
	}

	@GetMapping("/studentPrevModel")
	public String studentPrevModel(Model theModel, HttpServletRequest request) {

		Tblstudent student = getCurrentStudent();
		theModel.addAttribute("studentName", student.getStudentName());
		return "studentPrevModel";
	}

	@GetMapping("/studentNextModel")
	public String studentNextModel(Model theModel, HttpServletRequest request) {

		Tblstudent student = getCurrentStudent();
		theModel.addAttribute("studentName", student.getStudentName());
		return "studentNextModel";
	}

	@PostMapping("/submitExam")
	public String finishModel(@Valid @ModelAttribute("studModel") Tblmodel studModel, BindingResult bindingResult,
			final RedirectAttributes redirectAttributes, Model theModel) {

		Tblstudent student = getCurrentStudent();
		double studentMark = 0;
		double modelMark = 0;
		if (studModel.getModelID() != null) {
			List<Tblsection> tblsectionList = studModel.getTblsectionList();

			for (Tblsection tblsection : tblsectionList) {
				modelMark += tblsection.getMark();
				List<Tblquestion> tblquestionList = tblsection.getTblquestionList();

				for (Tblquestion tblquestion : tblquestionList) {

					int i = -1;
					int counter = -1;

					if (tblquestion.getTblstudentanswerList() == null) {
						Tblstudentanswer studentAnswer = new Tblstudentanswer();
						studentAnswer.setAnswer(null);
						studentAnswer.setCreatedDate(new Date());
						studentAnswer.setMark(null);

						studentAnswer.setSectionID(tblsection);
						studentAnswer.setModelID(studModel);
						studentAnswer.setQuestionID(tblquestion);
						studentAnswer.setStudentID(getCurrentStudent());
						studentAnswerService.addStudAnswer(studentAnswer); // currentSessi
					}

					else if (tblquestion.getTblstudentanswerList() != null) {
						for (Tblstudentanswer studentAnswer : tblquestion.getTblstudentanswerList()) {
							counter++;
							if (!tblquestion.getQuestionType().equals("part")) {
								if (studentAnswer.getAnswer() == null
										&& counter == tblquestion.getTblstudentanswerList().size() - 1 && i == -1) {
									studentAnswer.setAnswer(null);
									studentAnswer.setCreatedDate(new Date());
									studentAnswer.setMark(null);
									studentAnswer.setSectionID(tblsection);
									studentAnswer.setModelID(studModel);
									studentAnswer.setQuestionID(tblquestion);
									studentAnswer.setStudentID(getCurrentStudent());
									studentAnswerService.addStudAnswer(studentAnswer); // currentSessi
								}

								if (studentAnswer.getAnswer() != null) {

									if (tblquestion.getQuestionType().equals("rightorwrong")) {

										if (studentAnswer.getAnswer().equals(tblquestion.getTrueFalseAnswer())) {
											i++;
											studentMark += tblquestion.getMark();
											studentAnswer.setSectionID(tblsection);
											studentAnswer.setAnswer(studentAnswer.getAnswer());
											studentAnswer.setCreatedDate(new Date());
											studentAnswer.setMark(tblquestion.getMark());
											studentAnswer.setModelID(studModel);
											studentAnswer.setQuestionID(tblquestion);
											studentAnswer.setStudentID(getCurrentStudent());
											studentAnswerService.addStudAnswer(studentAnswer); // currentSessi

										} else if (!studentAnswer.getAnswer()
												.equals(tblquestion.getTrueFalseAnswer())) {
											studentAnswer.setAnswer(studentAnswer.getAnswer());
											studentAnswer.setSectionID(tblsection);
											studentAnswer.setCreatedDate(new Date());
											studentAnswer.setMark(0.0);
											studentAnswer.setModelID(studModel);
											studentAnswer.setQuestionID(tblquestion);
											studentAnswer.setStudentID(getCurrentStudent());
											studentAnswerService.addStudAnswer(studentAnswer); // currentSessi
											i++;
										}
									}

									else if (tblquestion.getQuestionType().equals("choice")) {

										List<Tblchoice> tblchoiceList = choiceService
												.findAllChoicesByQuestionID(tblquestion.getQuestionID());

										if (tblchoiceList == null) {
										}
										for (Tblchoice tblchoice : tblchoiceList) {
											if (tblchoice.getIschoosed() == 1) {
												if (tblchoice.getChoice().equals(studentAnswer.getAnswer())) {
													studentMark += tblquestion.getMark();
													studentAnswer.setSectionID(tblsection);
													studentAnswer.setAnswer(studentAnswer.getAnswer());
													studentAnswer.setCreatedDate(new Date());
													studentAnswer.setMark(tblquestion.getMark());
													studentAnswer.setModelID(studModel);
													studentAnswer.setQuestionID(tblquestion);
													studentAnswer.setStudentID(getCurrentStudent());
													studentAnswerService.addStudAnswer(studentAnswer);
												} else {
												studentAnswer.setSectionID(tblsection);
													studentAnswer.setAnswer(studentAnswer.getAnswer());
													studentAnswer.setCreatedDate(new Date());
													studentAnswer.setMark(0.0);
													studentAnswer.setModelID(studModel);
													studentAnswer.setQuestionID(tblquestion);
													studentAnswer.setStudentID(getCurrentStudent());
													studentAnswerService.addStudAnswer(studentAnswer);

												}
											}
										}

									}

									else if (studentAnswer.getAnswer().equals(tblquestion.getAnwser())) {
										i++;

										studentMark += tblquestion.getMark();
										studentAnswer.setSectionID(tblsection);
										studentAnswer.setAnswer(studentAnswer.getAnswer());
										studentAnswer.setCreatedDate(new Date());
										studentAnswer.setMark(tblquestion.getMark());
										studentAnswer.setModelID(studModel);
										studentAnswer.setQuestionID(tblquestion);
										studentAnswer.setStudentID(getCurrentStudent());
										studentAnswerService.addStudAnswer(studentAnswer); // currentSessi

									} else if (!studentAnswer.getAnswer().equals(tblquestion.getAnwser())) {
										// studentMark += tblquestion.getMark();
										studentAnswer.setAnswer(studentAnswer.getAnswer());
										studentAnswer.setSectionID(tblsection);
										studentAnswer.setCreatedDate(new Date());
										if (tblquestion.getQuestionType().equals("explain")
												|| tblquestion.getQuestionType().equals("meaning")) {
											studentAnswer.setMark(null);
										} else {
											studentAnswer.setMark(0.0);
										}
										studentAnswer.setModelID(studModel);
										studentAnswer.setQuestionID(tblquestion);
										studentAnswer.setStudentID(getCurrentStudent());
										studentAnswerService.addStudAnswer(studentAnswer); // currentSessi
										i++;
									}

								}
							}
						}

					}
				}

			}

			Tblstudent student2 = studentService.getStudentWithCurrentSession(student.getStudentID());
			Long groupID = student2.getGroupID().getGroupID();
			Tblgroupseries tblgroupSeries = groupSeriesService.findGroupSeriesNotModelFinishByStudentAndModelAndGroup(
					groupID, studModel.getModelID(), student.getStudentID());

			tblgroupSeries.setFinishedDate(new Date());
			tblgroupSeries.setStudentMark(studentMark);
			tblgroupSeries.setIsModelFinished(1);
			if (studModel.getModelType().equals("daily")) {
				tblgroupSeries.setIsModelCorrected(1);
				
			}
			groupSeriesService.updateGroupSeries(tblgroupSeries); // currentSession

		} 

		theModel.addAttribute("studentName", student.getStudentName());

		redirectAttributes.addFlashAttribute("saveModel", "تم إنتهاء حل النموذج");
		return "redirect:/";
	}

	public Tblstudent getCurrentStudent() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String studentName = auth.getName();

		Tblstudent student = studentService.findStudentByName(studentName);
		return student;
	}

}
