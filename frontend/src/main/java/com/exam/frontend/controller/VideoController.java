package com.exam.frontend.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.exam.common.model.Tblstudent;
import com.exam.common.model.Tblvideo;
import com.exam.common.service.StudentService;
import com.exam.common.service.VideoService;
import com.exam.frontend.config.BaseFrontEnd;

@Controller
@RequestMapping("/video")
public class VideoController{
	

	@Autowired
	StudentService studentService;
	@Autowired
	VideoService videoService;
	
	@InitBinder
	 protected void initBinder(WebDataBinder binder) 
	 {
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 dateFormat.setLenient(false);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));  
	 }

	
	@GetMapping("/list")
	public String findAllVideos(Model thModel) {
		Tblstudent student =  getCurrentStudent();
		thModel.addAttribute("studentName", student.getStudentName());
	List<Tblvideo> findAllVideos = videoService.findAllVideos();

	thModel.addAttribute("videos",findAllVideos);
		return "videos";
	}
	
	public Tblstudent getCurrentStudent() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String studentName = auth.getName();

		Tblstudent student = studentService.findStudentByName(studentName);
		return student;
	}
	
}
