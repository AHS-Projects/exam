package com.exam.frontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.exam")
public class FrontEndApp{

	
	public static void main(String[] args) {
		SpringApplication.run(FrontEndApp.class, args);
	}

}
