package com.exam.frontend.scheduler;

import com.exam.frontend.secuirity.ActiveUserStore;
import com.exam.frontend.secuirity.LoggedUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UsersScheduler {

    @Autowired
    ActiveUserStore activeUserStore;
    @Autowired
    LoggedUsers loggedUsers;

    @Scheduled(fixedDelay = 600000)
    public void scheduleFixedDelayTask() {

        int usersSize = loggedUsers.getUsersSize();
        if (usersSize == 0 || activeUserStore.getUsers().size() > usersSize) {
            loggedUsers.setUsersSize(activeUserStore.getUsers().size());
            Date userDate = new Date();
            loggedUsers.setDate(userDate);
        }
    }
}
