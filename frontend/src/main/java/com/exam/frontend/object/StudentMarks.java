package com.exam.frontend.object;

public class StudentMarks {

	
	private long studentID;
	private double marks;
	private String createdDate;
	
	public StudentMarks() {}
	
	public StudentMarks(double marks, String createdDate) {
		this.marks = marks;
		this.createdDate = createdDate;
	}

	public long getStudentID() {
		return studentID;
	}

	public void setStudentID(long studentID) {
		this.studentID = studentID;
	}

	public double getMarks() {
		return marks;
	}

	public void setMarks(double marks) {
		this.marks = marks;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
}
