<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>المُدَرِّس</title>
    <!-- css -->
    <jsp:directive.include file="template/style.html"/>
</head>

<body class="bg-default" style="background-image: url(resources/img/bg-img/bg1.jpg);">
<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
        <div class="container px-4">
            <!--   <a class="navbar-brand" href="../index.html">
          <img src="${pageContext.request.contextPath}/resources/img/brand/white.png" />
        </a>-->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-main"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-collapse-main">
                <!-- Collapse header -->
                <div class="navbar-collapse-header d-md-none">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <!--  <a href="../index.html">
                  <img src="${pageContext.request.contextPath}/resources/img/brand/blue.png">
                </a>-->
                        </div>
                        <div class="col-6 collapse-close">
                            <button type="button" class="navbar-toggler" data-toggle="collapse"
                                    data-target="#navbar-collapse-main" aria-controls="sidenav-main"
                                    aria-expanded="false" aria-label="Toggle sidenav">
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- Navbar items -->
                <ul class="navbar-nav ml-auto">
                </ul>
            </div>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8">
        <div class="container">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-6">
                        <h1 class="text-white">مرحبا</h1>
                        <!--<p class="text-lead text-light">Use these awesome forms to login or create new account in your project for free.</p>-->
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-7">
                <div class="card bg-secondary shadow border-0">
                    <div class="card-header  pb-5" style="background-color: #3f444a;">

                        <div class="card-body px-lg-5 py-lg-5">
                            <!-- <div class="alert alert-success fade show" role="alert">
                                 <h3>الرجاء ثم الرجاء ثم الرجاء عدم الدخول نهائيا حتى تختفى هذه الرساله لوجود اعمال
                                     الصيانه!</h3>
                                 <p>عينه من الطلبه المخالفه </p>
                                 <h6>مريم إبراهيم محمد الطنطاوي</h6>
                                 <h6>عبد الرحمن عبد الغني حسن محمود</h6>
                                 <h6>رحمة أحمد محمد صبيح</h6>
                                 <h6>فرح جمال محمود محمود</h6>
                                 <h6>أسماء مجدي عبد المقصود شحاتة</h6>
                                 <h6>رحاب محمد يحيي محمد</h6>

                             </div>-->
                            <div class="text-center text-muted mb-4">
                                <small style="color: white;">ادخل عن طريق اسم المستخدم </small>
                            </div>
                            <form method="POST" action="${contextPath}/login">
                                <div class="form-group mb-3">
                                    <c:if test="${param.error == 4}">
                                        <p class="text-danger">اسم المستخدم او كلمه السر غير صحيحه</p>
                                    </c:if>
                                    <c:if test="${param.logout == 'true'}">
                                        <p class="text-success">تم الخروج بنجاح</p>
                                    </c:if>
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                        </div>

                                        <input name="username" type="text" class="form-control"
                                               placeholder="اسم المستخدم"
                                               autofocus="true"/>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                        </div>

                                        <input name="password" type="password" class="form-control"
                                               placeholder="كلمه السر"/>

                                    </div>
                                </div>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                                <div class="text-center">
                                    <input type="submit" name="submit" value="دخول" class="btn btn-primary my-4"/>

                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- js -->
    <jsp:directive.include file="template/js.html"/>
</body>

</html>