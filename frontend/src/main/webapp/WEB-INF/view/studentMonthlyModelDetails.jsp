<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="en" style="font-family: 'Cairo', sans-serif;">

<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>المُدَرِّس</title>

<jsp:directive.include file="template/style.html" />

</head>

<body>
	<div id="preloader">
		<div class="spinner"></div>
	</div>
	<jsp:directive.include file="template/topbar.html" />
	<form:form modelAttribute="studModel" id="studentModelFrm"
		action="${pageContext.request.contextPath}/model/submitExam"
		method="post">
		<form:hidden path="modelID" />
		<form:hidden path="modelName" />
		<form:hidden path="modelType" />
		<form:hidden path="isPublished" />
		<form:hidden path="createdDate" />
		<form:hidden path="updatedDate" />
		<form:hidden path="createdByUserID" />
		<form:hidden path="updatedByUserID" />
		<form:hidden path="stageID" />
		<form:hidden path="modelTime" />
		<form:hidden path="modelMark" />
		<form:hidden path="modelDesc" />

		<!-- Display the countdown timer in an element -->
		<div style="padding-right: 3%;">
			<span>مدة الإمتحان : ${studModel.modelTime} دقيقة </span> <br>

			الوقت الحالي:
			<p id="demo" style="text-align: left"></p>
		</div>

		<div style="text-align: left; padding-left: 3%;">
			<c:if test="${(modelOldOrNew == 'prev') }">
درجة الطالب : ${studentMark}

</c:if>
		</div>
		<div class="card-header border-0">
			<center>
				<h3 class="mb-0">${studModel.modelName}</h3>
			</center>
		</div>
		<c:forEach items="${studModel.tblsectionList}" varStatus="secLst"
			var="section">
			<div class="row">
				<div class="col-md-12">
					<div class="card shadow">
						<section style="padding-right: 2%">
							<form:hidden path="tblsectionList[${secLst.index}].sectionID" />
							<form:hidden path="tblsectionList[${secLst.index}].mark" />
							    
							<form:label path="tblsectionList[${secLst.index}].sectionName">
								<strong>${section.sectionName}</strong>
							</form:label>
							<br />

							<c:forEach items="${section.tblquestionList}" varStatus="quesLst"
								var="question">
								<form:hidden
									path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].questionID" />
								<form:hidden
									path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].questionType" />
								<form:hidden
									path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].mark" />
								<form:hidden
									path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].trueFalseAnswer" />
								<div>
									<span>-</span><strong>${question.question}</strong>
								</div>
								
								<c:choose>
									<c:when test="${(modelOldOrNew == 'prev') }">
										<c:forEach items="${question.tblstudentanswerList}"
											varStatus="stdAsrLst" var="stdAsr">

											<c:choose>

												<c:when
													test="${(question.questionType == 'explain') and stdAsr.studentID.studentID == studentID}">
											
										<strong style="color: navy; display: block;">	الإجابة النموذجية:  ${question.anwser}</strong>
											<form:textarea
														path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${stdAsrLst.index}].answer"
														id="${question.questionID}" />
												</c:when>
												<c:when
													test="${(question.questionType == 'meaning')  and stdAsr.studentID.studentID == studentID}">
											<strong style="color: navy;display: block;">الإجابة النموذجية:  ${question.anwser}</strong>
											<form:input
														path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${stdAsrLst.index}].answer"
														id="${question.questionID}" />
												</c:when>

												<c:when
													test="${(question.questionType == 'rightorwrong')  and stdAsr.studentID.studentID == studentID}">
									<strong style="color: navy;">	الإجابة النموذجية:  ${question.trueFalseAnswer}</strong>
									<br>
										<div>
														<div
															class="custom-control custom-radio custom-control-inline">
															<label class="" for="${choice.choiceID}">صح</label>
															<form:radiobutton style="margin-top: 8%;"
																path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${stdAsrLst.index}].answer"
																value="صح" id="${choice.choiceID}" />
															&nbsp;&nbsp; <label class="" for="${choice.choiceID}">خطأ</label>
															<form:radiobutton style="margin-top: 8%;"
																path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${stdAsrLst.index}].answer"
																value="خطأ" id="${choice.choiceID}" />
														</div>
													</div>
												</c:when>

												<c:when
													test="${(question.questionType == 'choice')  and stdAsr.studentID.studentID == studentID}">

		<c:forEach items="${question.tblchoiceList}"
														varStatus="choiceLst2" var="choice2">
														<c:if test="${choice2.ischoosed == 1}">

															<strong style="color: navy; padding-right: 2%;">
																الإجابة النموذجية: ${choice2.choice}</strong>
															<br>
														</c:if>
													</c:forEach>

													<c:forEach items="${question.tblchoiceList}"
														varStatus="choiceLst" var="choice">

														<div
															class="custom-control custom-radio custom-control-inline">
															<label class="" for="${choice.choiceID}">${choice.choice}</label>


															<c:choose>
																<c:when test="${fn:length(choice.choice) gt 18}">
																	<form:radiobutton style="margin-top: 4%;"
																		path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${stdAsrLst.index}].answer"
																		value="${choice.choice}" id="${choiceLst.index}" />
																</c:when>
																<c:otherwise>
																	<form:radiobutton style="margin-top: 8%;"
																		path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${stdAsrLst.index}].answer"
																		value="${choice.choice}" id="${choiceLst.index}" />
																</c:otherwise>
															</c:choose>
														</div>
														

													</c:forEach>
													
												

												</c:when>
											</c:choose>
										</c:forEach>

									</c:when>



									<c:when test="${(modelOldOrNew == 'next') }">
																	<br>
										<c:choose>

											<c:when test="${(question.questionType == 'explain')}">
												<form:textarea
													path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[0].answer"
													id="${question.questionID}" />
											</c:when>
											<c:when test="${(question.questionType == 'meaning')}">

												<form:input
													path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[0].answer"
													id="${question.questionID}" />
											</c:when>

											<c:when test="${(question.questionType == 'rightorwrong')}">
												<div>
													<div
														class="custom-control custom-radio custom-control-inline">
														<label class="" for="${choice.choiceID}">صح</label>
														<form:radiobutton style="margin-top: 8%;"
															path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[0].answer"
															value="صح" id="${choice.choiceID}" />
														&nbsp;&nbsp; <label class="" for="${choice.choiceID}">خطأ</label>
														<form:radiobutton style="margin-top: 8%;"
															path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[0].answer"
															value="خطأ" id="${choice.choiceID}" />
													</div>
												</div>
											</c:when>

											<c:when test="${(question.questionType == 'choice') }">

												<c:forEach items="${question.tblchoiceList}"
													varStatus="choiceLst" var="choice">

													<div
														class="custom-control custom-radio custom-control-inline">
														<label class="" for="${choice.choiceID}">${choice.choice}</label>


														<c:choose>
															<c:when test="${fn:length(choice.choice) gt 18}">
																<form:radiobutton style="margin-top: 4%;"
																	path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[0].answer"
																	value="${choice.choice}" id="${choiceLst.index}" />
															</c:when>
															<c:otherwise>
																<form:radiobutton style="margin-top: 8%;"
																	path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[0].answer"
																	value="${choice.choice}" id="${choiceLst.index}" />
															</c:otherwise>
														</c:choose>
													</div>
												</c:forEach>
											</c:when>
										</c:choose>

									</c:when>
								</c:choose>




								<br />
								<br />
							</c:forEach>
						</section>

					</div>
				</div>
			</div>
			<br />
			<br />
		</c:forEach>
		<center>
			<button type="button" class="btn btn-primary" id="btnSubmit"
				onclick="dispModal();">أنهيت النموذج</button>
			<!-- make it disabled when model is finished (check finishedDate !=null in tblgroupSeries OR updatedDate != null  in tblmodel) -->
		</center>

	</form:form>

	<!-- Modal Approval -->
	<div class="modal fade" id="startModel" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="approveLabel">إنهاء حل النموذج</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">هل انت متاكد؟</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">لا</button>
					<a role="button" id="displayLink" class="btn btn-primary" href="#"
						onclick="finishModel();">نعم</a>
				</div>
			</div>
		</div>
	</div>

	<jsp:directive.include file="template/footer.html" />


	<jsp:directive.include file="template/js.html" />



	<script>	

		var isModelFinished = "${isModelFinished}";
		//console.log("isModelFinished : " + isModelFinished);
		if (isModelFinished == 'true') {
			$("#btnSubmit").css("display", "none");
		}

		function dispModal() {
			
			//console.log("inside dsp modalllll	");

			var isModelFinished = "${isModelFinished}";
			//console.log("isModelFinished : " + isModelFinished);
			if (isModelFinished == 'false') {
				$("#startModel").modal();
			} else {
				$("#btnSubmit").css("display", "none");
			}
		}

		function finishModel() {
			$("#studentModelFrm").submit();
		}

		// Set the date we're counting down to
		var modelOldOrNew = "${modelOldOrNew}";

		if (modelOldOrNew == 'next') {
			var CurrentmodelTime = "${studModel.modelTime}";
			CurrentmodelTime = CurrentmodelTime;
			var modelTime = "${modelTimeByMinutes}";
			var diff = "${diff}";
			var start = "${startDate}";

			console.log("model timeeeeeeeeeeee: " + modelTime);
			var countDownDate = new Date();
			countDownDate.setMinutes(countDownDate.getMinutes() + modelTime);
			var countDateTime = countDownDate.getTime();
			// Update the count down every 1 second
			var x = setInterval(function() {

				// Get today's date and time
				var now = new Date().getTime();
				console.log("countdown date: " + countDownDate.getTime());
				console.log("now JS : " + now);
				// Find the distance between now and the count down date
				var distance = now - start;
				console.log("distance: " + distance);
				// Time calculations for days, hours, minutes and seconds
				var minutes = Math.floor((distance % (1000 * 60 * 60))
						/ (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);
				console.log("minutes: " + minutes);
				console.log("seconds: " + seconds);
				// Display the result in the element with id="demo"
				document.getElementById("demo").innerHTML = minutes + "m "
						+ seconds + "s ";

				// If the count down is finished, write some text
				if (modelTime > 0) {
					console.log("model akbr mn zerooooo" + modelTime);
				}
				if (minutes >= CurrentmodelTime) {
					//console.log("model finihsedddd : "+modelTime);

					$("#studentModelFrm").submit();
					clearInterval(x);
					//document.getElementById("demo").innerHTML = "EXPIRED";
				}
			}, 1000);
		}
	</script>

</body>

</html>