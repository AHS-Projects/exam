<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description"
	content="Start your development with a Dashboard for Bootstrap 4.">
<meta name="author" content="Creative Tim">
<title>النماذج</title>
<!-- css -->
<jsp:directive.include file="template/style.html" />

</head>

<body>
	<!-- Sidenav -->
	<!-- Main content -->
	<div class="main-content">
		<!-- Top navbar -->
		<jsp:directive.include file="template/topbar.html" />

		<!-- Page content -->
		<div class="container-fluid mt--7">

			<!-- Table -->
			<div class="row">
				<div class="col">
					<div class="card shadow">
						<div class="card-header border-0">
							<h3 class="mb-0">النماذج</h3>
						</div>

						<div class="table-responsive">

							<table id="tblmodelid"
								class="table align-items-center table-flush">
								<thead class="thead-light">
									<tr>
										<th scope="col">درجة الطالب</th>
										<th scope="col">درجة النموذج</th>
										<th scope="col">اسم النموذج</th>
										<th scope="col">نوع النموذج</th>
										<th scope="col">وصف النموذج</th>
										<th scope="col">تاريخ البدء</th>
										<th scope="col">تاريخ الانتهاء</th>
										<th scope="col">عرض</th>
									</tr>
								</thead>
								<tbody>

									<tr>
										<td>studentMark</td>
										<td>modelMark</td>
										<td>modelName</td>
										<td>modelType</td>
										<td>modelDesc</td>
										<td>startDate</td>
										<td>finishDate</td>
										<td>edit</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>

			<!-- Footer -->
			<jsp:directive.include file="template/footer.html" />
		</div>
	</div>
	<!-- Argon Scripts -->
	<!-- Core -->
	<!-- js -->
	<jsp:directive.include file="template/js.html" />

	<script type="text/javascript">
		$(document).ready(function() {
			var table = $('#tblmodelid').DataTable({
				"aLengthMenu" : [ [ 5, 10, 15, -1 ], [ 5, 10, 15, "All" ] ],
				"iDisplayLength" : 5,
				"sAjaxSource" : "/model/rest/listPrev",
				"sAjaxDataProp" : "",
				"order" : [ [ 0, "asc" ] ],
				"aoColumns" : [

				{
					"mData" : "studentMark"
				}, {
					"mData" : "modelMark"
				}, {
					"mData" : "modelName"
				}, {
					"mData" : "modelType",
					render : getModelType
				}, {
					"mData" : "modelDesc"
				}, {
					"mData" : "startDate"
				}, {
					"mData" : "finishDate"
				}, {
					"mData" : "edit",
					render : edit
				} ]
			})
		});

		function edit(data, type, row, meta) {

			return '<a class="btn btn-primary" href="/model/studentModelDetails/'+row.modelID+'/prev" role="button" >عرض</a>';

		}

		function getModelType(data, type, row, meta) {

			if (row.modelType == 'daily')
				return 'يومى';
			else if (row.modelType == 'monthly')
				return 'شهرى';
		}
	</script>


</body>

</html>