<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->
    <!-- Title -->
    <title>المُدَرِّس</title>
    
  <!-- css -->
  <jsp:directive.include file = "template/style.html" />
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="spinner"></div>
    </div>

    <!-- ##### Header Area Start ##### -->
<jsp:directive.include file = "template/topbar.html" />
 
   <c:if test="${not empty saveModel}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${saveModel}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>


    <section class="hero-area bg-img bg-overlay-2by5" style="background-image: url(resources/img/bg-img/bg1.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <!-- Hero Content -->
                    <div class="hero-content text-center">
                        <h2>Let's Study Together</h2>
                        <a href="#" class="btn clever-btn">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Cool Facts Area Start ##### -->
    <section class="cool-facts-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Single Cool Facts Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-cool-facts-area text-center mb-100">
                        <div class="icon">
                            <img src="img/core-img/docs.png" alt="">
                        </div>
                        <h2><span class="counter">${totalDailyModels}</span></h2>
                        <h5>إجمالي  عدد الإمتحانات اليومية السابقة</h5>
                    </div>
                </div>

                <!-- Single Cool Facts Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-cool-facts-area text-center mb-100">
                        <div class="icon">
                            <img src="img/core-img/star.png" alt="">
                        </div>
                        <h2><span class="counter">${totalMonthlyModels}</span></h2>
                        <h5>إجمالي عدد الإمتحانات الشهرية السابقة</h5>
                    </div>
                </div>

                <!-- Single Cool Facts Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-cool-facts-area text-center mb-100">
                        <div class="icon">
                            <img src="img/core-img/events.png" alt="">
                        </div>
                        <h2><span class="counter">${nextModels}</span></h2>
                        <h5>عدد الإمتحانات القادمة</h5>
                    </div>
                </div>
                
                 <c:if test="${studDailyModel ne 'NaN'}">
                  <!-- Single Cool Facts Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-cool-facts-area text-center mb-100">
                        <div class="icon">
                            <img src="img/core-img/events.png" alt="">
                        </div>
                        <h2><span class="counter">${studDailyModel}</span>%</h2>
                        <h5>نسبة نجاح الطالب في الإمتحانات اليومية</h5>
                    </div>
                </div>
                
                </c:if>
                
                                 <c:if test="${studMonthModel ne 'NaN'}">
                  <!-- Single Cool Facts Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-cool-facts-area text-center mb-100">
                        <div class="icon">
                            <img src="img/core-img/events.png" alt="">
                        </div>
                        <h2><span class="counter">${studMonthModel}</span>%</h2>
                        <h5>نسبة نجاح الطالب في الإمتحانات الشهرية</h5>
                    </div>
                </div>
                </c:if>

            </div>
        </div>
    </section>
    <!-- ##### Cool Facts Area End ##### -->

		


    <!-- ##### Footer Area Start ##### -->
   <jsp:directive.include file = "template/footer.html" />
    <!-- ##### Footer Area End ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
   
   <jsp:directive.include file = "template/js.html" />
   
   
</body>

</html>