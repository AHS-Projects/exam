<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>النماذج</title>
    <!-- css -->
    <jsp:directive.include file="template/style.html"/>

</head>

<body>
<!-- Sidenav -->
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <jsp:directive.include file="template/topbar.html"/>

    <!-- Page content -->
    <div class="container-fluid mt--7">

        <!-- Table -->
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">النماذج</h3>
                    </div>

                    <div class="table-responsive">

                        <table id="tblmodelid" class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">اسم النموذج</th>
                                <th scope="col">نوع النموذج</th>
                                <th scope="col">وصف النموذج</th>


                                <th scope="col">بدء الامتحان</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>

                                <td>
                                    modelName
                                </td>
                                <td>
                                    modelType
                                </td>
                                <td>
                                    modelDesc
                                </td>


                                <td>
                                    edit
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <!-- Modal Approval -->
        <div class="modal fade" id="startModel" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="approveLabel">بدء حل النموذج</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        سوف يبدأ الوقت, هل انت متاكد؟
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">لا</button>
                        <a role="button" id="displayLink" class="btn btn-primary"
                           href="/model/studentModelDetails/3/next">نعم</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <jsp:directive.include file="template/footer.html"/>
    </div>
</div>
<!-- Argon Scripts -->
<!-- Core -->
<!-- js -->
<jsp:directive.include file="template/js.html"/>

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#tblmodelid').DataTable({
            "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
            "iDisplayLength": 4,
            "sAjaxSource": "/model/rest/listNext",
            "sAjaxDataProp": "",
            "order": [[0, "asc"]],
            "aoColumns": [

                {"mData": "modelName"},
                {"mData": "modelType", render: getModelType},
                {"mData": "modelDesc"},


                {"mData": "edit", render: edit}
            ]
        })
    });


    function edit(data, type, row, meta) {
        console.log("start date : " + row.startDate);
        if (row.startDate == null) {
            console.log("start date  inside conditiooooon: " + row.startDate);
            return '<a class="btn btn-primary" href="#" role="button" data-toggle="modal" onclick="dispModel(' + row.modelID + ');">بدء</a>';

        } else {
            return '<a id= "cont" class="btn btn-primary" href="/model/studentModelDetails/7/next" role="button"  onclick="continueModel(' + row.modelID + ');">استمرار</a>';
        }
        //  return '<a class="btn btn-primary" href="/model/studentModelDetails/'+row.modelID+'/next" role="button" >عرض</a>';


    }

    function continueModel(modelID) {
        console.log("inside continue modeeeeel");
        var display = "/model/studentModelDetails/" + modelID + "/next";
        $("#cont").attr("href", display);
        // $("#startModel").modal();
    }

    function dispModel(modelID) {
        var display = "/model/studentModelDetails/" + modelID + "/next";
        $("#displayLink").attr("href", display);
        $("#startModel").modal();
    }

    function getModelType(data, type, row, meta) {

        if (row.modelType == 'daily')
            return 'يومى';
        else if (row.modelType == 'monthly')
            return 'شهرى';
    }


</script>


</body>

</html>