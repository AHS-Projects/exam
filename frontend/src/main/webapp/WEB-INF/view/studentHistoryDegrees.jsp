<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->

<!-- Title -->
<title>المُدَرِّس</title>

<!-- css -->
<jsp:directive.include file="template/style.html" />

</head>

<body>
	<!-- Preloader -->
	<div id="preloader">
		<div class="spinner"></div>
	</div>

	<!-- ##### Header Area Start ##### -->
	<jsp:directive.include file="template/topbar.html" />
	<!-- ##### Header Area End ##### -->

	<!-- ##### table Area Start ##### -->


	<!-- Table -->
	<div class="row">
		<div class="col">
			<div class="card shadow" style="padding-right: 3%; padding-top: 10%;">
				<div class="card-header border-0">
					<center>
						<h3 class="mb-0">درجات تقييم الطالب</h3>
					</center>
				</div>

				<div class="table-responsive">

					<table id="tblstudid" class="table align-items-center table-flush">
						<thead class="thead-light">
							<tr>

								<th scope="col">درجة الطالب</th>
								<th scope="col">درجة النموذج</th>
								<th scope="col">اسم النموذج</th>
								<th scope="col">نوع النموذج</th>
								<th scope="col">تاريخ البدء</th>
								<th scope="col">تاريخ الانتهاء</th>
							</tr>

						</thead>
						<tbody>

							<tr>
								<td>studentMark</td>
								<td>modelMark</td>
								<td>modelName</td>
								<td>modelType</td>
								<td>startDate</td>
								<td>finishDate</td>
							</tr>
					</table>
				</div>

			</div>
		</div>
	</div>


	<!-- ##### table Area End ##### -->

	<!-- ##### Footer Area Start ##### -->
	<jsp:directive.include file="template/footer.html" />
	<!-- ##### Footer Area End ##### -->

	<!-- ##### All Javascript Script ##### -->
	<!-- jQuery-2.2.4 js -->

	<jsp:directive.include file="template/js.html" />

	<script type="text/javascript">
		$(document).ready(function() {
			var table = $('#tblstudid').DataTable({
				"aLengthMenu" : [ [ 5, 10, 15, -1 ], [ 5, 10, 15, "All" ] ],
				"iDisplayLength" : 5,
				"sAjaxSource" : "/student/rest/studentMarks",
				"sAjaxDataProp" : "",
				"order" : [ [ 0, "asc" ] ],
				"aoColumns" : [

				{
					"mData" : "studentMark"
				}, {
					"mData" : "modelMark"
				}, {
					"mData" : "modelName"
				}, {
					"mData" : "modelType",
					render : getModelType
				}, {
					"mData" : "startDate"
				}, {
					"mData" : "finishDate"
				},

				]
			})
		});

		function getModelType(data, type, row, meta) {

			if (row.modelType == 'daily')
				return 'يومى';
			else if (row.modelType == 'monthly')
				return 'شهرى';
		}
	</script>
</body>

</html>