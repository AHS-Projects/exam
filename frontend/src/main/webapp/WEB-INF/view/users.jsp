<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en" style="font-family: 'Cairo', sans-serif;">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>المُدَرِّس</title>

    <jsp:directive.include file="template/style.html"/>
    <script src="${pageContext.request.contextPath}/resources/countdown.js"></script>

</head>

<body>
<div id="preloader">
    <div class="spinner"></div>
</div>
<jsp:directive.include file="template/topbar.html"/>

<div class="card-header border-0">
    <div style="text-align: right">

        <div id=timer></div>


    </div>
    <center>
        <h3 class="mb-0">Users</h3>
    </center>
</div>
<h2>Currently logged in users</h2>
<h2>${users.size()}</h2>
<h2>the biggest number of users</h2>
<h2>${usersSize}</h2>
<h2>Date</h2>
<h2>${usersDate}</h2>
<c:forEach items="${users}" varStatus="videoStat"
           var="user">
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow" style="width: 100%">
                <section>

                    <center>
                        <label path="${user}">
                            <strong>${user}</strong>
                        </label>


                    </center>
                </section>

            </div>
        </div>
    </div>
    <br/>
    <br/>
</c:forEach>
<center>

    <!-- make it disabled when model is finished (check finishedDate !=null in tblgroupSeries OR updatedDate != null  in tblmodel) -->
</center>


<jsp:directive.include file="template/footer.html"/>


<jsp:directive.include file="template/js.html"/>
</body>

</html>