package com.exam.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter 
{
	 @Autowired
	 PasswordEncoder passwordEncoder;
	 @Autowired
	 UserDetailsService userDetailsService;
	 
	 @Override
	 protected void configure(HttpSecurity http) throws Exception
	 {
	       
			 
//		  http.authorizeRequests()
//	        .antMatchers("/login")
//	            .permitAll()
//	            .antMatchers("/**")
//	            .hasAnyRole("ADMIN", "USER")
	            
        http         
        .headers()
         .frameOptions().sameOrigin()
         .and()
           .authorizeRequests()
            .antMatchers("/*").authenticated()
            //.antMatchers("/department/**","/section/**","/quantity/**","/product/**",
            //		"/subdepartment/**","/subdepartmentrest/**","/order/productorders/**"
            //		,"/orderrest/productorders/**").hasAnyRole("ADMIN","DEPARTMENT")
           // .antMatchers("/order/**","/orderrest/list").hasAnyRole("ADMIN","ORDER")
           // .antMatchers("/**").hasRole("ADMIN")
            .anyRequest().authenticated()
	      
	        .and()
	            .formLogin()
	            .loginPage("/login")
	            .defaultSuccessUrl("/")
	            .failureUrl("/login?error=4")
	            .usernameParameter("username")
                .passwordParameter("password")
	            .permitAll()
	        .and()
	            .logout()
	            .logoutSuccessUrl("/login?logout=true")
	            .invalidateHttpSession(true)
	            .permitAll()
	            .and()
	            .exceptionHandling() //exception handling configuration
	    		.accessDeniedPage("/error")
	            .and()
	            .csrf()
	            .disable();
	 }
	
	 @Autowired
	 public void configureGlobal(AuthenticationManagerBuilder authenticationMgr) throws Exception {
//		 authenticationMgr.inMemoryAuthentication()
//	        .passwordEncoder(passwordEncoder)
//	        .withUser("hossamhany").password(passwordEncoder.encode("soofy12345")).roles("USER")
//	        .and()
//	        .withUser("alaahassan").password(passwordEncoder.encode("soofy12345")).roles("USER", "ADMIN");
		    authenticationMgr.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder); 
		 
		}
	 
	 @Bean
	 public PasswordEncoder passwordEncoder() 
	 {
	        return new BCryptPasswordEncoder();
	 }
	 
	 @Override
		public void configure(WebSecurity web) throws Exception {
//			web.ignoring().antMatchers("/*.css");
//			web.ignoring().antMatchers("/*.js");
		    web.ignoring().antMatchers("/resources/**");
		}
	 

}
