package com.exam.backend.object;

public class StudentExam 
{
	private String studentName;
	private String modelName;
	private String modelType;
	private String modelStatus;
	private String modelCorrected;
	private double totalModelMarks;
	private double studentMarks;
	

	
	
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public double getTotalModelMarks() {
		return totalModelMarks;
	}
	public void setTotalModelMarks(double totalModelMarks) {
		this.totalModelMarks = totalModelMarks;
	}
	public double getStudentMarks() {
		return studentMarks;
	}
	public void setStudentMarks(double studentMarks) {
		this.studentMarks = studentMarks;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getModelStatus() {
		return modelStatus;
	}
	public void setModelStatus(String modelStatus) {
		this.modelStatus = modelStatus;
	}
	public String getModelType() {
		return modelType;
	}
	public void setModelType(String modelType) {
		this.modelType = modelType;
	}
	public String getModelCorrected() {
		return modelCorrected;
	}
	public void setModelCorrected(String modelCorrected) {
		this.modelCorrected = modelCorrected;
	}
	
	
	

}
