package com.exam.backend.object;



public class GroupSeries 
{
	 private long groupSeriesID;
	 private Integer isModelFinished;
	 private String startDate;
	 private String finishedDate;
	 private String assignedDate;
	 private long modelID;
	 private long groupID;
	 private long studentID;
	 private long stageID;
	 private String assignedByTecher;
	 private String groupName;
	 private String modelName;
	 private String modelType;
	 private double modelMark;
	 private double studentMark;
	 private String stageName;
	 private String studentName;
	 private String studentPhone;
	 private String studentDadPhone;
	 
	 
	 
	 
	 
	 
	public long getGroupSeriesID() {
		return groupSeriesID;
	}
	public void setGroupSeriesID(long groupSeriesID) {
		this.groupSeriesID = groupSeriesID;
	}
	public Integer getIsModelFinished() {
		return isModelFinished;
	}
	public void setIsModelFinished(Integer isModelFinished) {
		this.isModelFinished = isModelFinished;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getFinishedDate() {
		return finishedDate;
	}
	public void setFinishedDate(String finishedDate) {
		this.finishedDate = finishedDate;
	}
	public String getAssignedDate() {
		return assignedDate;
	}
	public void setAssignedDate(String assignedDate) {
		this.assignedDate = assignedDate;
	}
	public long getModelID() {
		return modelID;
	}
	public void setModelID(long modelID) {
		this.modelID = modelID;
	}
	public long getGroupID() {
		return groupID;
	}
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}
	public long getStudentID() {
		return studentID;
	}
	public void setStudentID(long studentID) {
		this.studentID = studentID;
	}
	
	public String getAssignedByTecher() {
		return assignedByTecher;
	}
	public void setAssignedByTecher(String assignedByTecher) {
		this.assignedByTecher = assignedByTecher;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getModelName() {
		return modelName;
	}
	
	public long getStageID() {
		return stageID;
	}
	public void setStageID(long stageID) {
		this.stageID = stageID;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getModelType() {
		return modelType;
	}
	public void setModelType(String modelType) {
		this.modelType = modelType;
	}
	public double getModelMark() {
		return modelMark;
	}
	public void setModelMark(double modelMark) {
		this.modelMark = modelMark;
	}
	public String getStageName() {
		return stageName;
	}
	public void setStageName(String stageName) {
		this.stageName = stageName;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getStudentPhone() {
		return studentPhone;
	}
	public void setStudentPhone(String studentPhone) {
		this.studentPhone = studentPhone;
	}
	public String getStudentDadPhone() {
		return studentDadPhone;
	}
	public void setStudentDadPhone(String studentDadPhone) {
		this.studentDadPhone = studentDadPhone;
	}
	public double getStudentMark() {
		return studentMark;
	}
	public void setStudentMark(double studentMark) {
		this.studentMark = studentMark;
	}
	
	 
}
