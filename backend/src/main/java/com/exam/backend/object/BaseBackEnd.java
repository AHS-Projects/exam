package com.exam.backend.object;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.exam.common.model.Tblteacher;
import com.exam.common.service.TeacherService;


public class BaseBackEnd 
{
	@Autowired
	private TeacherService service;
	
	protected Map<String, String> breadcrumbs ;
	
	public BaseBackEnd()
	{
		breadcrumbs=new LinkedHashMap<String, String>();
	}
	
	public Tblteacher getCurrentTeacher()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String teacherName=auth.getName();
		Tblteacher tblteacher=service.findTeacherByName(teacherName);
		return tblteacher;
	}
	
   // Encryte Password with BCryptPasswordEncoder
   public static String encrytePassword(String password) {
       BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
       return encoder.encode(password);
   }

	public Map<String, String> getBreadcrumbs() {
		return breadcrumbs;
	}
	
	public void setBreadcrumbs(Map<String, String> breadcrumbs) {
		this.breadcrumbs = breadcrumbs;
	}
   
   

}
