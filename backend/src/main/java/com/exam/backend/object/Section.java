package com.exam.backend.object;

public class Section 
{
	private String sectionName;
	private double mark;
	private String createdByTeacher;
	private long sectionID;
	private long modelID;
	
	
	
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public double getMark() {
		return mark;
	}
	public void setMark(double mark) {
		this.mark = mark;
	}
	public String getCreatedByTeacher() {
		return createdByTeacher;
	}
	public void setCreatedByTeacher(String createdByTeacher) {
		this.createdByTeacher = createdByTeacher;
	}
	public long getSectionID() {
		return sectionID;
	}
	public void setSectionID(long sectionID) {
		this.sectionID = sectionID;
	}
	public long getModelID() {
		return modelID;
	}
	public void setModelID(long modelID) {
		this.modelID = modelID;
	}
	
	
	

}
