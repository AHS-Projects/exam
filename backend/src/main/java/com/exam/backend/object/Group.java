package com.exam.backend.object;

public class Group 
{
	private long groupID;
	private String groupName;
	
	
	public long getGroupID() {
		return groupID;
	}
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	 
	 
	 

}
