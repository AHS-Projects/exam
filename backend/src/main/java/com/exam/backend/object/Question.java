package com.exam.backend.object;

public class Question 
{
	
	private String question;
	private String answer;
	private String trueFalseAnswer;
	private double mark;
	private String questionType;
	private String createdByTeacher;
	private long questionID;
	private long sectionID;
	
	
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public double getMark() {
		return mark;
	}
	public void setMark(double mark) {
		this.mark = mark;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	
	public String getCreatedByTeacher() {
		return createdByTeacher;
	}
	public void setCreatedByTeacher(String createdByTeacher) {
		this.createdByTeacher = createdByTeacher;
	}
	public long getQuestionID() {
		return questionID;
	}
	public void setQuestionID(long questionID) {
		this.questionID = questionID;
	}
	public long getSectionID() {
		return sectionID;
	}
	public void setSectionID(long sectionID) {
		this.sectionID = sectionID;
	}
	public String getTrueFalseAnswer() {
		return trueFalseAnswer;
	}
	public void setTrueFalseAnswer(String trueFalseAnswer) {
		this.trueFalseAnswer = trueFalseAnswer;
	}
	
	
	

}
