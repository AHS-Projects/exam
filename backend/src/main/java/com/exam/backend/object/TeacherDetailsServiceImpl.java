package com.exam.backend.object;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.exam.common.model.Tblteacher;
import com.exam.common.model.Tblteacherrole;
import com.exam.common.service.TeacherService;


@Service
public class TeacherDetailsServiceImpl implements UserDetailsService {
 
    @Autowired
    private TeacherService teacherService;
 
 
    @Override
    public UserDetails loadUserByUsername(String teacherName) throws UsernameNotFoundException {
        Tblteacher teacher = teacherService.findTeacherByName(teacherName);
 
        if (teacher == null || teacher.getTeacherID()==null) {
    
            throw new UsernameNotFoundException("مستخدم " + teacherName + " لا يوجد");
        }

        List<String> roleNames = getRoleNames(teacher);
 
        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
        if (roleNames != null) 
        {
            for (String role : roleNames) 
            {
        
                GrantedAuthority authority = new SimpleGrantedAuthority(role);
                grantList.add(authority);
            }
        }
 
        UserDetails userDetails = (UserDetails) new User(teacher.getTeacherName(), 
        		teacher.getTeacherPassword(), grantList);
 
        return userDetails;
    }
    
    public  List<String> getRoleNames(Tblteacher teacher)
    {
    	List<String> roleNames=new ArrayList<String>();
    	List<Tblteacherrole>teacherRoles= teacher.getTblteacherroleList();
    	for(Tblteacherrole teacherRole:teacherRoles)
    	{
    		roleNames.add(teacherRole.getRoleID().getRole());
    	}
    	return roleNames;
    }
 
}