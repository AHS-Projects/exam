package com.exam.backend.object;



public class Model 
{
	private Long modelID;
    private String modelName;
    private String modelType;
    private Long stageID;
    private String modelDesc;
    private String stageName;
    private String createdByUser;
    private Integer isPublished;
    private String modelTime;
    
    
    
    
    
    public String getModelType() {
		return modelType;
	}
	public void setModelType(String modelType) {
		this.modelType = modelType;
	}
	
	public Long getModelID() {
		return modelID;
	}
	public void setModelID(Long modelID) {
		this.modelID = modelID;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getModelDesc() {
		return modelDesc;
	}
	public void setModelDesc(String modelDesc) {
		this.modelDesc = modelDesc;
	}
	public String getStageName() {
		return stageName;
	}
	public void setStageName(String stageName) {
		this.stageName = stageName;
	}
	public String getCreatedByUser() {
		return createdByUser;
	}
	public void setCreatedByUser(String createdByUser) {
		this.createdByUser = createdByUser;
	}
	public Long getStageID() {
		return this.stageID;
	}
	public void setStageID(Long stageID) {
		this.stageID = stageID;
	}
	public Integer getIsPublished() {
		return isPublished;
	}
	public void setIsPublished(Integer isPublished) {
		this.isPublished = isPublished;
	}
	public String getModelTime() {
		return modelTime;
	}
	public void setModelTime(String modelTime) {
		this.modelTime = modelTime;
	}
	
 
	

}
