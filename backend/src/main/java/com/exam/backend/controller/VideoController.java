package com.exam.backend.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.exam.backend.object.BaseBackEnd;
import com.exam.common.model.Tblvideo;
import com.exam.common.service.VideoService;

@Controller
@RequestMapping("/video")
public class VideoController  extends BaseBackEnd {
	
	@Autowired
	VideoService videoService;
	
	@InitBinder
	 protected void initBinder(WebDataBinder binder) 
	 {
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 dateFormat.setLenient(false);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));  
	 }

	
	@GetMapping("/list")
	public String findAllVideos(Model thModel) {
		
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("روابط الفيديو", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		
		List<Tblvideo> findAllVideos = videoService.findAllVideos();	
		thModel.addAttribute("videos",findAllVideos);
		return "videolist";
	}
	
	@GetMapping("/redirectToAddVideo")
	public String redirectToaddVideoPage(Model thModel) {
		
	  //breadcrumb
				breadcrumbs.clear();
				breadcrumbs.put("الرئيسيه", "/");
				breadcrumbs.put("روابط الفيديو", "/video/list");
				breadcrumbs.put("اضافه او تعديل رابط فيديو", "#");
				thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		Tblvideo video = new Tblvideo();
		thModel.addAttribute("video", video);
		
		return "videoform";
	}
	
	@PostMapping("/add")
	public String addVideo(@Valid @ModelAttribute("video") Tblvideo video
			,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes,Model thModel) {
		
		if(bindingResult.hasErrors())
		{
			return "videoform";
		}
		if(video.getVideoID() == null) {
	 videoService.addVideo(video);
		
	 redirectAttributes.addFlashAttribute("saveVideo","تم تخزين البيانات بنجاح");
		return "redirect:/video/list/";
		}else {
			 videoService.updateVideo(video);	
			 redirectAttributes.addFlashAttribute("saveVideo","تم تعديل البيانات بنجاح");
				return "redirect:/video/list/";
		}

	}
	
	@GetMapping("/redirectToUpdateVideo/{videoID}")
	public String redirectToUpdateVideo(@PathVariable("videoID")long videoID, Model model) {
		
	 //breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("روابط الفيديو", "/video/list");
		breadcrumbs.put("اضافه او تعديل رابط فيديو", "#");
		model.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		Tblvideo video = videoService.getVideo(videoID);
		model.addAttribute("video", video);
		return "videoform";
	}
	

	
	@GetMapping("/delete/{videoID}")
	public String deleteVideo(@PathVariable("videoID")long videoID) {
		videoService.deleteVideo(videoID);
		return "redirect:/video/list/";
	}
	
}
