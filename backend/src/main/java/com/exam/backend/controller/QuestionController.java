package com.exam.backend.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.exam.backend.object.BaseBackEnd;
import com.exam.common.model.Tblchoice;
import com.exam.common.model.Tblquestion;
import com.exam.common.model.Tblsection;
import com.exam.common.service.QuestionService;
import com.exam.common.service.SectionService;


@Controller
@RequestMapping("/question")
public class QuestionController extends BaseBackEnd
{
	@Autowired
	private QuestionService quesService;
	@Autowired
	private SectionService sectionService;
	
	@InitBinder
	 protected void initBinder(WebDataBinder binder) 
	 {
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 dateFormat.setLenient(false);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));  
	 }
	
	@GetMapping("/list/{secID}")
	public String getQuestionList(Model thModel,@PathVariable("secID")long secID)
	{
		Tblsection section= sectionService.getSection(secID);
		thModel.addAttribute("secID",secID);
		thModel.addAttribute("secName",section.getSectionName());
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("اجزاء النموذج "+section.getModelID().getModelName(), "/section/list/"+section.getModelID().getModelID());
		breadcrumbs.put("اسئله "+section.getSectionName(), "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		return "questionlist";
	}
	
	@GetMapping("/add/{secID}")
	public String addQuestion(@PathVariable("secID")long secID,Model thModel)
	{
		Tblsection section= sectionService.getSection(secID);
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("اجزاء النموذج "+section.getModelID().getModelName(), "/section/list/"+section.getModelID().getModelID());
		breadcrumbs.put("اسئله "+section.getSectionName(), "/question/list/"+section.getSectionID());
		breadcrumbs.put("اضافه او تعديل سؤال", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		Tblquestion question=new Tblquestion();
		Tblchoice tblchoice=new Tblchoice();
		thModel.addAttribute("question",question);
		thModel.addAttribute("choice",tblchoice);
		thModel.addAttribute("secID",secID);
		return "questionform";
	}
	@GetMapping("/update/{quesID}/{secID}")
	public String addSection(@PathVariable("quesID")long quesID,@PathVariable("secID")long secID,Model thModel)
	{
		Tblquestion question=quesService.getQuestion(quesID);
		Tblsection section= sectionService.getSection(secID);
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("اجزاء النموذج "+section.getModelID().getModelName(), "/section/list/"+section.getModelID().getModelID());
		breadcrumbs.put("اسئله "+section.getSectionName(), "/question/list/"+section.getSectionID());
		breadcrumbs.put("اضافه او تعديل سؤال", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		thModel.addAttribute("question",question);
		thModel.addAttribute("secID",secID);
		return "questionform";
	}
	
	@PostMapping("/save/{secID}")
	public String saveQuestion(@PathVariable("secID")long secID,@Valid @ModelAttribute("question") Tblquestion question
			,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes)
	{
		
		if(bindingResult.hasErrors())
		{
			return "questionform";
		}
		if(question.getQuestionID()==null)
		{
			question.setCreatedDate(new Date());
			question.setUpdatedDate(null);
			question.setCreatedByUserID(getCurrentTeacher());
			question.setUpdatedByUserID(null);
			Tblsection sec=sectionService.getSection(secID);
			question.setSectionID(sec);
			quesService.addQuestion(question);
		}
		else
		{
			question.setUpdatedDate(new Date());
			question.setUpdatedByUserID(getCurrentTeacher());
			quesService.updateQuestion(question);
		}
		
		redirectAttributes.addFlashAttribute("saveQues","تم تخزين البيانات بنجاح");
		return "redirect:/question/list/"+secID;
	}
	@GetMapping("/delete/{quesID}/{secID}")
	public String deleteModel(@PathVariable("quesID")long quesID,@PathVariable("secID")long secID,
			final RedirectAttributes redirectAttributes)
	{
		quesService.deleteQuestion(quesID);
		redirectAttributes.addFlashAttribute("deleteQues","تم مسح السؤال بنجاح");
		return "redirect:/question/list/"+secID;
	}
	
	@ModelAttribute("questiontypes")
	public Map<String, String> getModelTypes()  {

		Map<String,String> types = new LinkedHashMap<String,String>();
		types.put("meaning", "مرادفات");
		types.put("explain", "شرح");
		types.put("choice", "اختيارى");
		types.put("rightorwrong", "صح/خطأ");
		types.put("part", "قطعه");
		return types;
	}
	
	@ModelAttribute("truefalsetypes")
	public Map<String, String> getTrueFalseTypes()  {

		Map<String,String> types = new LinkedHashMap<String,String>();
		types.put("صح", "صح");
		types.put("خطأ", "خطأ");
		return types;
	}
	

	
	

}
