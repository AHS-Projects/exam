package com.exam.backend.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.exam.backend.object.BaseBackEnd;
import com.exam.common.model.Tblstage;
import com.exam.common.service.StageService;



@Controller
@RequestMapping("/stage")
public class StageController extends BaseBackEnd
{
	@Autowired
	private StageService stageService;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 dateFormat.setLenient(false);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));  
	}
	
	@GetMapping("/list")
	public String getStageList(Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("المراحل", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		List<Tblstage>stages= stageService.findAllStages();
		thModel.addAttribute("stages",stages);
		return "stagelist";
	}
	
	@GetMapping("/add")
	public String addStage(Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("المراحل", "/stage/list");
		breadcrumbs.put("اضافه او تعديل مرحله", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblstage stage=new Tblstage();
		thModel.addAttribute("stage",stage);
		return "stageform";
	}
	@GetMapping("/update/{stgID}")
	public String updateStage(@PathVariable("stgID")long stgID,Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("المراحل", "/stage/list");
		breadcrumbs.put("اضافه او تعديل مرحله", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblstage stage=stageService.getStage(stgID);
		thModel.addAttribute("stage",stage);
		return "stageform";
	}
	@PostMapping("/save")
	public String saveSection(@Valid @ModelAttribute("stage") Tblstage stage
			,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes)
	{
		if(bindingResult.hasErrors())
		{
			return "stageform";
		}
		if(stage.getStageID()==null)
		{
			stage.setCreatedDate(new Date());
			stage.setUpdatedDate(null);
			stage.setCreatedByUserID(getCurrentTeacher());
			stage.setUpdatedByUserID(null);
			stageService.addStage(stage);
		}
		else
		{
			stage.setUpdatedDate(new Date());
			stage.setUpdatedByUserID(getCurrentTeacher());
			stageService.updateStage(stage);
		}
		
		redirectAttributes.addFlashAttribute("saveStage","تم تخزين البيانات بنجاح");
		return "redirect:/stage/list/";
	}
	
	@GetMapping("/delete/{stgID}")
	public String deleteTeacher(@PathVariable("stgID")long stageID,final RedirectAttributes redirectAttributes)
	{
		stageService.deleteStage(stageID);
		redirectAttributes.addFlashAttribute("deleteStage","تم مسح المرحله بنجاح");
		return "redirect:/stage/list";
	}
	@GetMapping("/correct/model/list")
	public String getAllStages(Model thModel)
	{
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("المراحل", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		List<Tblstage>stages=stageService.findAllStages();
		thModel.addAttribute("stages",stages);
		return "stagemodel";
	}
	
	

}
