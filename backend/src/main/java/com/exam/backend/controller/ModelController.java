package com.exam.backend.controller;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.exam.backend.object.BaseBackEnd;
import com.exam.common.model.Tblmodel;
import com.exam.common.model.Tblstage;
import com.exam.common.service.ModelService;
import com.exam.common.service.StageService;
import com.exam.editor.StageEditor;


@Controller
@RequestMapping("/model")
public class ModelController extends BaseBackEnd
{
	@Autowired
	private ModelService modelService;
	@Autowired
	private StageService stageService;
	
	@InitBinder
	 protected void initBinder(WebDataBinder binder) 
	 {
		 binder.registerCustomEditor(Tblstage.class, new StageEditor());
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 dateFormat.setLenient(false);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));  
	 } 
	
	@GetMapping("/list")
	public String getModelList(Model thModel)
	{
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		return "modellist";
	}
	
	@ModelAttribute("allStages")
	public List<Tblstage> getallStages()
	{
		return stageService.findAllStages();
	}
	
	@ModelAttribute("modeltypes")
	public Map<String, String> getModelTypes()  {

		Map<String,String> types = new LinkedHashMap<String,String>();
		types.put("daily", "يومى");
		types.put("monthly", "شهرى");
		return types;
	}
	@ModelAttribute("modelPublished")
	public Map<Integer, String> getModelPublished()  {

		Map<Integer,String> types = new LinkedHashMap<Integer,String>();
		types.put(0, "لا");
		types.put(1, "نعم");
		return types;
	}
	@ModelAttribute("modelTimes")
	public Map<Integer, Integer> getModelTimes()  {

		Map<Integer,Integer> types = new LinkedHashMap<Integer,Integer>();
		types.put(5 , 5);
		types.put(10, 10);
		types.put(15, 15);
		types.put(25, 25);
		types.put(30, 30);
		types.put(35, 35);
		types.put(40, 40);
		types.put(45, 45);
		types.put(60, 60);
		types.put(90, 90);
		types.put(120, 120);
		types.put(150, 150);
		types.put(180, 180);
		types.put(210, 210);
		types.put(240, 240);
		return types;
	}
	
	@GetMapping("/add")
	public String addModel(Model thModel)
	{
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("اضافه او تعديل نموذج", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		Tblmodel model=new Tblmodel();
		thModel.addAttribute("model",model);
		return "modelform";
	}
	
	@GetMapping("/update/{id}")
	public String updateModel(@PathVariable("id")long modelID,Model thModel)
	{
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("اضافه او تعديل نموذج", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		Tblmodel model=modelService.getModelByID(modelID);
		thModel.addAttribute("model",model);
		return "modelform";
	}
	
	@PostMapping("/save")
	public String saveModel(@Valid @ModelAttribute("model") Tblmodel model,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes)
	{
		if(bindingResult.hasErrors())
		{
			return "modelform";
		}
		else
		{
			if(model.getModelID()==null)
			{
				model.setCreatedDate(new Date());
				model.setUpdatedDate(null);
				model.setCreatedByUserID(getCurrentTeacher());
				model.setUpdatedByUserID(null);
				modelService.addModel(model);
			}
			else
			{
				model.setUpdatedDate(new Date());
				model.setUpdatedByUserID(getCurrentTeacher());
				modelService.updateModel(model);
			}
			
		}
		redirectAttributes.addFlashAttribute("saveModel","تم تخزين البيانات بنجاح");
		return "redirect:/model/list";
	}
	
	@GetMapping("/delete/{id}")
	public String deleteModel(@PathVariable("id")long modelID,final RedirectAttributes redirectAttributes)
	{
		modelService.deleteModel(modelID);
		redirectAttributes.addFlashAttribute("deleteModel","تم مسح النموذج بنجاح");
		return "redirect:/model/list";
	}

}
