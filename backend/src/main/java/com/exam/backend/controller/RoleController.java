package com.exam.backend.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.exam.backend.object.BaseBackEnd;
import com.exam.common.model.Tblrole;
import com.exam.common.service.RoleService;


@Controller
@RequestMapping("/role")
public class RoleController extends BaseBackEnd 
{
	@Autowired
	private RoleService roleService;
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 dateFormat.setLenient(false);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));  
	}
	
	@GetMapping("/list")
	public String roleList(Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("الادوار", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		List<Tblrole>roles=roleService.findAllRoles();
		thModel.addAttribute("roles",roles);
		return "roleList";
	}
	
	@GetMapping("/add")
	public String addRole(Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("الادوار", "/role/list");
		breadcrumbs.put("اضافه او تعديل دور", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblrole role=new Tblrole();
		thModel.addAttribute("role",role);
		return "roleform";
	}
	
	@GetMapping("/update/{roleID}")
	public String updateRole(@PathVariable("roleID")long roleID,Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("الادوار", "/role/list");
		breadcrumbs.put("اضافه او تعديل دور", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblrole role=roleService.getRole(roleID);
		thModel.addAttribute("role",role);
		return "roleform";
	}
	
	@PostMapping("/save")
    public String saveRole(@Valid @ModelAttribute("role")Tblrole role,BindingResult bindingResult,
    		final RedirectAttributes redirectAttributes)
    {
		if(bindingResult.hasErrors())
		{
		   return "roleform";
		}
		if(role.getRoleID()==null)
		{
			role.setCreatedDate(new Date());
			role.setCreatedByUserID(getCurrentTeacher());
			role.setUpdatedDate(null);
			role.setUpdatedByUserID(null);
			roleService.addRole(role);
		}
		else
		{
			role.setUpdatedDate(new Date());
			role.setUpdatedByUserID(getCurrentTeacher());
			roleService.updateRole(role);
		}
		redirectAttributes.addFlashAttribute("saveRole","تم تخزين البيانات بنجاح");
		return "redirect:/role/list";
    }
	@GetMapping("/delete/{roleID}")
	public String deleteRole(@PathVariable("roleID")long roleID,final RedirectAttributes redirectAttributes)
	{
		 try
		 {
			 roleService.deleteRole(roleID);
		 }
		 catch(Exception e)
		 {
//		   theModel.addAttribute("depList", listofDep());
//		   theModel.addAttribute("error","قم بمسح كل ما داخل القسم اولا");
//		   return "deparmentList";
		 }
		 redirectAttributes.addFlashAttribute("deleteRole","تم مسح الدور بنجاح");
		return "redirect:/role/list";
	}

}
