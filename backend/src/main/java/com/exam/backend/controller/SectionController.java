package com.exam.backend.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.exam.backend.object.BaseBackEnd;
import com.exam.common.model.Tblmodel;
import com.exam.common.model.Tblsection;
import com.exam.common.service.ModelService;
import com.exam.common.service.SectionService;

@Controller
@RequestMapping("/section")
public class SectionController extends BaseBackEnd 
{
	@Autowired
	private SectionService sectionService;
	@Autowired
	private ModelService modelService;
	
	
	@InitBinder
	 protected void initBinder(WebDataBinder binder) 
	 {
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 dateFormat.setLenient(false);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));  
	 }
	
	@GetMapping("/list/{modelID}")
	public String getSectionByModelID(Model thModel,@PathVariable("modelID")long modelID)
	{
		Tblmodel model= modelService.getModelByID(modelID);
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("اجزاء النموذج "+model.getModelName(), "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		thModel.addAttribute("modelID",modelID);
		thModel.addAttribute("modelName",model.getModelName());
		return "sectionlist";
	}
	@GetMapping("/add/{modID}")
	public String addSection(@PathVariable("modID")long modID,Model thModel)
	{
		Tblmodel model= modelService.getModelByID(modID);
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("اجزاء النموذج "+model.getModelName(), "/section/list/"+modID);
		breadcrumbs.put("اضافه او تعديل جزء", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		Tblsection section=new Tblsection();
		thModel.addAttribute("section",section);
		thModel.addAttribute("modID",modID);
		return "sectionform";
	}
	@GetMapping("/update/{secID}/{modID}")
	public String addSection(@PathVariable("secID")long secID,@PathVariable("modID")long modID,Model thModel)
	{
		Tblmodel model= modelService.getModelByID(modID);
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("اجزاء النموذج "+model.getModelName(), "/section/list/"+modID);
		breadcrumbs.put("اضافه او تعديل جزء", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		Tblsection section=sectionService.getSection(secID);
		thModel.addAttribute("section",section);
		thModel.addAttribute("modID",modID);
		return "sectionform";
	}
	
	@PostMapping("/save/{modID}")
	public String saveSection(@PathVariable("modID")long modID,@Valid @ModelAttribute("section") Tblsection section
			,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes)
	{
		if(bindingResult.hasErrors())
		{
			return "sectionform";
		}
		if(section.getSectionID()==null)
		{
			section.setCreatedDate(new Date());
			section.setUpdatedDate(null);
			section.setCreatedByUserID(getCurrentTeacher());
			section.setUpdatedByUserID(null);
			Tblmodel model=modelService.getModelByID(modID);
			section.setModelID(model);
			sectionService.addSection(section);
		}
		else
		{
			section.setUpdatedDate(new Date());
			section.setUpdatedByUserID(getCurrentTeacher());
			sectionService.updateSection(section);
		}
		
		redirectAttributes.addFlashAttribute("saveSection","تم تخزين البيانات بنجاح");
		return "redirect:/section/list/"+modID;
	}
	
	@GetMapping("/delete/{secID}/{modID}")
	public String deleteModel(@PathVariable("secID")long secID,@PathVariable("modID")long modID,
			final RedirectAttributes redirectAttributes)
	{
		sectionService.deleteSection(secID);;
		redirectAttributes.addFlashAttribute("deleteSection","تم مسح الجزء بنجاح");
		return "redirect:/section/list/"+modID;
	}
	

}
