package com.exam.backend.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.exam.backend.object.BaseBackEnd;
import com.exam.common.model.Tblgroup;
import com.exam.common.model.Tblstage;
import com.exam.common.service.GroupService;
import com.exam.common.service.StageService;

@Controller
@RequestMapping("/group")
public class GroupController extends BaseBackEnd
{
	@Autowired
	private GroupService groupService;
	@Autowired
	private StageService stageservice;
	
	@InitBinder
	 protected void initBinder(WebDataBinder binder) 
	 {
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 dateFormat.setLenient(false);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));  
	 }
	
	@GetMapping("/list/{stgID}")
	public String fetGroupListByStageID(@PathVariable("stgID")long stgID,Model thModel)
	{
		Tblstage tblstage=stageservice.getStage(stgID);
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("المراحل", "/stage/list");
		breadcrumbs.put("مجموعات "+tblstage.getStageName(), "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		List<Tblgroup>groups=groupService.findGroupsByStageID(stgID);
		thModel.addAttribute("groups",groups);
		thModel.addAttribute("stgID",stgID);
		thModel.addAttribute("stageName",tblstage.getStageName());
		return "grouplist";
	}
	
	@GetMapping("/add/{stgID}")
	public String addStage(@PathVariable("stgID")long stgID,Model thModel)
	{
		Tblstage tblstage=stageservice.getStage(stgID);
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("المراحل", "/stage/list");
		breadcrumbs.put("مجموعات "+tblstage.getStageName(), "/group/list/"+tblstage.getStageID());
		breadcrumbs.put("اضافه او تعديل مجموعه", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblgroup group=new Tblgroup();
		group.setStageID(tblstage);
		thModel.addAttribute("group",group);
		thModel.addAttribute("stgID",stgID);
		return "groupform";
	}
	@GetMapping("/update/{grpID}/{stgID}")
	public String updateStage(@PathVariable("grpID")long grpID,@PathVariable("stgID")long stgID,Model thModel)
	{
		Tblstage tblstage=stageservice.getStage(stgID);
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("المراحل", "/stage/list");
		breadcrumbs.put("مجموعات "+tblstage.getStageName(), "/group/list/"+tblstage.getStageID());
		breadcrumbs.put("اضافه او تعديل مجموعه", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblgroup group=groupService.getGroup(grpID);
		thModel.addAttribute("group",group);
		thModel.addAttribute("stgID",stgID);
		return "groupform";
	}
	@PostMapping("/save/{stgID}")
	public String saveSection(@PathVariable("stgID")long stgID,@Valid @ModelAttribute("group") Tblgroup group
			,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes)
	{
		if(bindingResult.hasErrors())
		{
			return "groupform";
		}
		if(group.getGroupID()==null)
		{
			group.setCreatedDate(new Date());
			group.setUpdatedDate(null);
			group.setCreatedByUserID(getCurrentTeacher());
			group.setUpdatedByUserID(null);
			groupService.addGroup(group);
		}
		else
		{
			group.setUpdatedDate(new Date());
			group.setUpdatedByUserID(getCurrentTeacher());
			groupService.updateGroup(group);
		}
		
		redirectAttributes.addFlashAttribute("saveGroup","تم تخزين البيانات بنجاح");
		return "redirect:/group/list/"+stgID;
	}
	
	@GetMapping("/delete/{grpID}/{stgID}")
	public String deleteTeacher(@PathVariable("grpID")long grpID,@PathVariable("stgID")long stageID
			,final RedirectAttributes redirectAttributes)
	{
		groupService.deleteGroup(grpID);
		redirectAttributes.addFlashAttribute("deleteGroup","تم مسح المجموعه بنجاح");
		return "redirect:/group/list/"+stageID;
	}
	
	
	

	@GetMapping("/list/{stageID}/{modelID}")
	public String getGroupsByStageID(@PathVariable("stageID")long stageID,@PathVariable("modelID")long modelID,Model thmodel)
	{
		Tblstage tblstage=stageservice.getStage(stageID);
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("مجموعات "+tblstage.getStageName(), "#");
		thmodel.addAttribute("breadcrumbs",breadcrumbs);
	    ////////////////////////////////////////
		List<Tblgroup>tblgroups=groupService.findGroupsByStageID(stageID);
		thmodel.addAttribute("groups",tblgroups);
		thmodel.addAttribute("stageName",tblstage.getStageName());
		thmodel.addAttribute("modelID",modelID);
		return "groupmodellist";
	}

}
