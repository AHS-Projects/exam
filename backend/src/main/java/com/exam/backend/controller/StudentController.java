package com.exam.backend.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.exam.backend.object.BaseBackEnd;
import com.exam.backend.object.Group;
import com.exam.common.model.Tblgroup;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblmodel;
import com.exam.common.model.Tblstage;
import com.exam.common.model.Tblstudent;
import com.exam.common.service.GroupSeriesService;
import com.exam.common.service.GroupService;
import com.exam.common.service.ModelService;
import com.exam.common.service.StageService;
import com.exam.common.service.StudentService;
import com.exam.editor.GroupEditor;
import com.exam.editor.StageEditor;

@Controller
@RequestMapping("/student")
public class StudentController extends BaseBackEnd
{

	@Autowired
	private StudentService studentService;
	@Autowired
	private StageService stageService;
	@Autowired
	private GroupService groupservice;
	@Autowired
	private GroupSeriesService groupseriesService;
	@Autowired
	private ModelService modelServicel;
	
	
	
	@InitBinder
	 protected void initBinder(WebDataBinder binder) 
	 {
		
		 binder.registerCustomEditor(Tblstage.class, new StageEditor());
		 binder.registerCustomEditor(Tblgroup.class, new GroupEditor());
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 dateFormat.setLenient(false);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));  
	      
	 }
	
	@GetMapping("/list")
	public String getAllStudents(Model thModel)
	{
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("الطلاب", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		return "studentlist";
	}
	
	@ModelAttribute("allStages")
	public List<Tblstage> getallStages()
	{
		return stageService.findAllStages();
	}
	
	@GetMapping("/groupbystage/{stageID}")
	public @ResponseBody  List<Group> getAllGroupsByStageID(@PathVariable("stageID") int stageID) {
		
		List<Group>groups=new ArrayList<Group>();
		List<Tblgroup>tblgroups=groupservice.findGroupsByStageID(stageID);
		for(Tblgroup tblgroup:tblgroups)
		{
			Group group=new Group();
			group.setGroupID(tblgroup.getGroupID());
			group.setGroupName(tblgroup.getGroupName());
			groups.add(group);
		}
		
	    return groups;
	}
	
	@ModelAttribute("allGroups")
	public List<Tblgroup> getallGroups()
	{
		return groupservice.findAllGroup();
	}
	
	
	@ModelAttribute("blocktypes")
	public Map<Integer, String> getBlockTypes()  {

		Map<Integer,String> types = new LinkedHashMap<Integer,String>();
		types.put(0, "نشط");
		types.put(1, "غير نشط");
		return types;
	}
	
	@GetMapping("/add")
	public String addstudent(Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("الطلاب", "/student/list");
		breadcrumbs.put("اضافه او تعديل طالب", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblstudent tblstudent=new Tblstudent();
		thModel.addAttribute("student",tblstudent);
		return "studentform";
	}
	
	@GetMapping("/update/{id}")
	public String updateModel(@PathVariable("id")long studentID,Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("الطلاب", "/student/list");
		breadcrumbs.put("اضافه او تعديل طالب", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblstudent student=studentService.getStudentWithCurrentSession(studentID);
		thModel.addAttribute("student",student);
		//thModel.addAttribute("stageID",student.getStageID().getStageID());
		return "studentform";
	}
	
	@GetMapping("/update/password/{id}")
	public String changePassword(@PathVariable("id")long studentID,Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("الطلاب", "/student/list");
		breadcrumbs.put("تغيير كلمه السر", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblstudent student=studentService.getStudentWithCurrentSession(studentID);
		thModel.addAttribute("student",student);
		return "studentpasswordform";
	}
	@PostMapping("/save")
	public String saveModel(@Valid @ModelAttribute("student") Tblstudent student,BindingResult bindingResult,Model thModel,
			final RedirectAttributes redirectAttributes)
	{
		if(bindingResult.hasErrors())
		{
			return "studentform";
		}
		else
		{
			if(student.getStudentID()==null)
			{
				Tblstudent tblstudent=studentService.findStudentByName(student.getStudentName());
				if(tblstudent!=null)
				{
					thModel.addAttribute("uniqueUser","هذا الاسم موجود من قبل");
					return "studentform";
				}
				student.setStudentPassword(encrytePassword(student.getStudentPassword()));
				student.setCreatedDate(new Date());
				student.setUpdatedDate(null);
				student.setCreatedByUserID(getCurrentTeacher());
				student.setUpdatedByUserID(null);
				studentService.addStudent(student);
			}
			else
			{
				//check if new password or no change
				if(student.getStudentPassword().equals(student.getConfirmationStudentPassword()))
				{
					//kda da new password wlazem a3ml encrypt law m4 bytswo yb2a da kda edit user info 3ade
				    student.setStudentPassword(encrytePassword(student.getStudentPassword()));
				}
				student.setUpdatedDate(new Date());
				student.setUpdatedByUserID(getCurrentTeacher());
				studentService.updateStudent(student);
			}
			
		}
		redirectAttributes.addFlashAttribute("saveStudent","تم تخزين البيانات بنجاح");
		return "redirect:/student/list";
	}
	
	@GetMapping("/delete/{id}")
	public String deleteStudent(@PathVariable("id")long studenID,final RedirectAttributes redirectAttributes)
	{
		studentService.deleteStudent(studenID);
		redirectAttributes.addFlashAttribute("deleteStudent","تم مسح الطالب بنجاح");
		return "redirect:/student/list";
	}
	
	@GetMapping("/block/{id}")
	public String blockStudent(@PathVariable("id")long studenID,final RedirectAttributes redirectAttributes)
	{
		Tblstudent tblstudent=studentService.getStudentWithCurrentSession(studenID);
		if (tblstudent.getBlocked()==0)
		{
			tblstudent.setBlocked(1);
			studentService.updateStudent(tblstudent);
		}
		else
		{
			tblstudent.setBlocked(0);
			studentService.updateStudent(tblstudent);
		}
		redirectAttributes.addFlashAttribute("blockStudent","تم تغيير حاله الطالب بنجاح");
		return "redirect:/student/list";
	}
	
	@GetMapping("/exam/{studentID}")
	public String getStudentExams(@PathVariable("studentID")long studentID,Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("الطلاب", "/student/list");
		breadcrumbs.put("نماذج الطالب", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		thModel.addAttribute("studentID",studentID);
		return "studentexam";
	}
	
	@GetMapping("/list/group/{groupID}/{stageID}/{modelID}")
	public String getAllStudentsByGroup(@PathVariable("groupID")long groupID,@PathVariable("stageID")long stageID
			,@PathVariable("modelID")long modelID,Model thModel)
	{
		Tblmodel tblmodel= modelServicel.getModelByID(modelID);
		Tblgroup tblgroup=groupservice.getGroup(groupID);
		Tblstage tblstage=stageService.getStage(stageID);
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("مجموعات "+tblstage.getStageName(), "/group/list/"+tblstage.getStageID()+"/"+tblmodel.getModelID());
		breadcrumbs.put("تعيين الطلاب المجموعه "+tblgroup.getGroupName()+" للنموذج "+tblmodel.getModelName()+" "+tblstage.getStageName(), "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
	    ////////////////////////////////////////
		thModel.addAttribute("groupID",groupID);
		thModel.addAttribute("stageID",stageID);
		thModel.addAttribute("modelID",modelID);
		thModel.addAttribute("modelName",tblmodel.getModelName());
		thModel.addAttribute("groupName",tblgroup.getGroupName());
		thModel.addAttribute("stageName",tblstage.getStageName());
		return "studentgrouplist";
	}
	
	@PostMapping("/assign/model/{groupID}/{stageID}/{modelID}")
	public String assignStudentToModel(HttpServletRequest httpServletRequest, @PathVariable("groupID")long groupID
			,@PathVariable("modelID")long modelID,@PathVariable("stageID")long stageID
			,final RedirectAttributes redirectAttributes )
	{
		Tblgroup tblgroup=groupservice.getGroup(groupID);
		Tblmodel tblmodel=modelServicel.getModelByID(modelID);
	    String[]paramValues=httpServletRequest.getParameterValues("studentName");
	    int countAssignedBefore=0;
	    if(paramValues!=null)
	    {
	    	for(String studentStrID:httpServletRequest.getParameterValues("studentName"))
			{
				long studentID=Long.parseLong(studentStrID);
				Tblgroupseries checkGrupSeries= groupseriesService.findGroupSeriesByStudentAndModelAndGroup(groupID, modelID, studentID);
				if(checkGrupSeries!=null)
				{
					countAssignedBefore=countAssignedBefore+1;
				}
				else
				{
					Tblstudent tblstudent=studentService.getStudentWithCurrentSession(studentID);
					Tblgroupseries tblgroupseries=new Tblgroupseries();
					tblgroupseries.setAssignedByTecherID(getCurrentTeacher());
					tblgroupseries.setAssignedDate(new Date());
					tblgroupseries.setGroupID(tblgroup);
					tblgroupseries.setModelID(tblmodel);
					tblgroupseries.setIsModelFinished(0);
					tblgroupseries.setStudentID(tblstudent);
				
				    groupseriesService.addGroupSeries(tblgroupseries);
				}
			}
	    	if(countAssignedBefore>0)
	    	{
	    		redirectAttributes.addFlashAttribute("assignStudent","تم اضافه الطلبه المحدده باستثناء "+countAssignedBefore+" طلبه تم اضافتهم من قبل");
	    	}
	    	else
	    	{
	    	   redirectAttributes.addFlashAttribute("assignStudent","تم اضافه الطلبه المحدده الى النموذج بنجاح");
	    	}
	    }
	    else
	    {
	    	redirectAttributes.addFlashAttribute("assignStudent","لم يتم تحديد طلبه");
	    }
	    
		
		
		return "redirect:/student/list/group/"+groupID+"/"+stageID+"/"+modelID;
	}
	

}
