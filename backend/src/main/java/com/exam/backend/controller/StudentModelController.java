package com.exam.backend.controller;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.exam.backend.object.BaseBackEnd;
import com.exam.common.model.Tblchoice;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblmodel;
import com.exam.common.model.Tblquestion;
import com.exam.common.model.Tblsection;
import com.exam.common.model.Tblstudentanswer;
import com.exam.common.service.ChoiceService;
import com.exam.common.service.GroupSeriesService;
import com.exam.common.service.QuestionService;
import com.exam.common.service.StudentAnswerService;


@Controller
@RequestMapping("/student/model")
public class StudentModelController extends BaseBackEnd
{
	@Autowired
	private StudentAnswerService studentAnswerService;
	@Autowired
	private GroupSeriesService groupSerService;
	@Autowired
	private QuestionService questionService;
	@Autowired 
	private ChoiceService choiceService; 
	
	
	@InitBinder
	 protected void initBinder(WebDataBinder binder) 
	 {
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 dateFormat.setLenient(false);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));  
	 }

	@Autowired
	private GroupSeriesService groupSeriesService;
	
	@GetMapping("/notcorrect/list/{stageID}")
	public String getAllNotCorrectedModel(@PathVariable("stageID")long stageID,Model thModel)
	{
		
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("المراحل", "/stage/correct/model/list");
		breadcrumbs.put("النماذج الغير مصححه", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		thModel.addAttribute("stageID",stageID);
		return "studentmodelnotcorrectlist";
	}
	
	@GetMapping("/correct/{groupSerID}")
	public String correctModel(@PathVariable("groupSerID")long groupSerID,Model thModel)
	{
		Tblgroupseries tblgroupsers=groupSeriesService.getGroupSeriesWithModelsAndSections(groupSerID);
		//load all related objects
		for(Tblsection tblsection :tblgroupsers.getModelID().getTblsectionList())
		{
			List<Tblquestion>questions= questionService.findQuestionsBySecID(tblsection.getSectionID());
			
			for(Tblquestion tblquestion:questions)
			{
				if(tblquestion.getQuestionType().equals("choice")) 
	              { 
	                   List<Tblchoice>choices=choiceService.findAllChoicesByQuestionID(tblquestion.getQuestionID()); 
	                   tblquestion.setTblchoiceList(choices); 
	              } 
				//load answer of question of each student
				List<Tblstudentanswer> anwers=studentAnswerService.findAllAnwsersByStudentAndQuestions(tblgroupsers.getStudentID().getStudentID(), tblquestion.getQuestionID());
				tblquestion.setTblstudentanswerList(anwers);
				
			}
			tblsection.setTblquestionList(questions);
			
		}
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("المراحل", "/stage/correct/model/list");
		breadcrumbs.put("النماذج الغير مصححه", "/student/model/notcorrect/list/"+tblgroupsers.getGroupID().getStageID().getStageID());
		breadcrumbs.put("تصحيح النموذج", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		//////////////////////////////////////////////
		Tblmodel model=tblgroupsers.getModelID();
		List<Tblstudentanswer>tblstudentanswers=new ArrayList<Tblstudentanswer>();
		
		int seciIndex=0;
		int quesIndex=0;
		for(Tblsection tblsection : model.getTblsectionList())
		{
			for(Tblquestion tblquestion : tblsection.getTblquestionList())
			{
				for(Tblstudentanswer tblstudentanswer : tblquestion.getTblstudentanswerList())
				{
					if(tblstudentanswer.getQuestionID().getQuestionID().equals(tblsection.getTblquestionList().get(quesIndex).getQuestionID())
					    && tblstudentanswer.getStudentID().getStudentID().equals(tblgroupsers.getStudentID().getStudentID())	)
					{
						tblstudentanswers.add(tblstudentanswer);
					}
					
				}
				
				model.getTblsectionList().get(seciIndex).getTblquestionList().get(quesIndex).setTblstudentanswerList(tblstudentanswers);
				tblstudentanswers=new ArrayList<Tblstudentanswer>();
				quesIndex=quesIndex+1;
			}
			quesIndex=0;
			seciIndex=seciIndex+1;
		}

		thModel.addAttribute("model",model);
		thModel.addAttribute("groupserID",groupSerID);
		return "correctmodelform";
	}
	
	@PostMapping("/correct/save/{groupserID}")
	public String saveCorrection(@PathVariable("groupserID")long groupserID,@ModelAttribute("model")Tblmodel model,
			final RedirectAttributes redirectAttributes)
	{
        double studentMark=0;
		for(Tblsection tblsection : model.getTblsectionList())
		{
			for(Tblquestion tblquestion : tblsection.getTblquestionList())
			{
				if(tblquestion.getQuestionID()!=null && tblquestion.getTblstudentanswerList()!=null)
				{
					for(Tblstudentanswer tblstudentanswer : tblquestion.getTblstudentanswerList())
					{
						if(tblstudentanswer.getStudentAnswerID()!=null)
						{
	                       //System.out.println(tblstudentanswer);
						   if(tblstudentanswer.getMark()!=null)
						   {
							   studentMark=studentMark+tblstudentanswer.getMark();
							   studentAnswerService.updateStudAnswer(tblstudentanswer);
						   }
						}
					}
				}
			}
			
		}

		Tblgroupseries tblgroupseries= groupSerService.getGroupSeries(groupserID);
		tblgroupseries.setStudentMark(studentMark);
		tblgroupseries.setIsModelCorrected(1);
		groupSerService.updateGroupSeries(tblgroupseries);
		redirectAttributes.addFlashAttribute("correctedModel", "تم تصحيح النموذج بنجاح");
		
		return "redirect:/student/model/corrected/list/"+model.getStageID().getStageID();
	}
	@GetMapping("/corrected/list/{stageID}")
	public String getAllCorrectedModel(@PathVariable("stageID")long stageID,Model thModel)
	{
		
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("المراحل", "/stage/correct/model/list");
		breadcrumbs.put("النماذج المصححه", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		thModel.addAttribute("stageID",stageID);
		return "studentmodelcorrectlist";
	}

}
