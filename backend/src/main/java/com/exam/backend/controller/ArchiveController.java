package com.exam.backend.controller;

import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.TblgroupseriesArchive;
import com.exam.common.model.TblgroupseriesarchivePK;
import com.exam.common.service.GroupSeriesArchService;
import com.exam.common.service.GroupSeriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/archive")
public class ArchiveController {

    @Autowired
    GroupSeriesArchService groupSeriesArchService;
    @Autowired
    GroupSeriesService groupSeriesService;

    @GetMapping
    public String gelAllchoiceByQuestionID() {

        return "archive";
    }

    @GetMapping("/startgropseries")
    public String startArchiveGroipSeries(final RedirectAttributes redirectAttributes) {
        List<Tblgroupseries> tblgroupserieses = groupSeriesService.findAllCorrectedModelForArchive();
        for (Tblgroupseries tblgroupseries : tblgroupserieses) {
            TblgroupseriesArchive tblgroupseriesArchive = new TblgroupseriesArchive();
            TblgroupseriesarchivePK tblgroupseriesarchivePK = new TblgroupseriesarchivePK();
            tblgroupseriesarchivePK.setGroupID(tblgroupseries.getGroupID().getGroupID());
            tblgroupseriesarchivePK.setModelID(tblgroupseries.getModelID().getModelID());
            tblgroupseriesarchivePK.setStudentID(tblgroupseries.getStudentID().getStudentID());
            tblgroupseriesArchive.setTblgroupseriesarchivePK(tblgroupseriesarchivePK);
            tblgroupseriesArchive.setAssignedDate(tblgroupseries.getAssignedDate());
            tblgroupseriesArchive.setAssignedByTecherID(tblgroupseries.getAssignedByTecherID().getTeacherID());
            tblgroupseriesArchive.setFinishedDate(tblgroupseries.getFinishedDate());
            tblgroupseriesArchive.setStartDate(tblgroupseries.getStartDate());
            tblgroupseriesArchive.setIsModelCorrected(tblgroupseries.getIsModelCorrected());
            tblgroupseriesArchive.setIsModelFinished(tblgroupseries.getIsModelFinished());
            tblgroupseriesArchive.setStudentMark(tblgroupseries.getStudentMark());
            groupSeriesArchService.addGroupSeries(tblgroupseriesArchive);
            groupSeriesService.deleteGroupSeries(tblgroupseries.getGroupSeriesID());

        }
        redirectAttributes.addFlashAttribute("msginfo", "تم الارشفه بنجاح");
        return "redirect:/archive/";
    }

    @GetMapping("/startarchmonth")
    public String startArchiveMonth(final RedirectAttributes redirectAttributes) {

        List<Tblgroupseries> tblgroupserieses = groupSeriesService.findAllNotFinishedBeforeLast2Months();
        //System.out.println("hossssssssaaaaaaaaam " + tblgroupserieses.size());
        for (Tblgroupseries tblgroupseries : tblgroupserieses) {
            //System.out.println(tblgroupseries.getAssignedDate());
            try {


                TblgroupseriesArchive tblgroupseriesArchive = new TblgroupseriesArchive();
                TblgroupseriesarchivePK tblgroupseriesarchivePK = new TblgroupseriesarchivePK();
                tblgroupseriesarchivePK.setGroupID(tblgroupseries.getGroupID().getGroupID());
                tblgroupseriesarchivePK.setModelID(tblgroupseries.getModelID().getModelID());
                tblgroupseriesarchivePK.setStudentID(tblgroupseries.getStudentID().getStudentID());
                tblgroupseriesArchive.setTblgroupseriesarchivePK(tblgroupseriesarchivePK);
                tblgroupseriesArchive.setAssignedDate(tblgroupseries.getAssignedDate());
                tblgroupseriesArchive.setAssignedByTecherID(tblgroupseries.getAssignedByTecherID().getTeacherID());
                tblgroupseriesArchive.setFinishedDate(tblgroupseries.getFinishedDate());
                tblgroupseriesArchive.setStartDate(tblgroupseries.getStartDate());
                tblgroupseriesArchive.setIsModelCorrected(tblgroupseries.getIsModelCorrected());
                tblgroupseriesArchive.setIsModelFinished(tblgroupseries.getIsModelFinished());
                tblgroupseriesArchive.setStudentMark(tblgroupseries.getStudentMark());
                groupSeriesService.deleteGroupSeries(tblgroupseries.getGroupSeriesID());
                groupSeriesArchService.addGroupSeries(tblgroupseriesArchive);

            } catch (Exception e) {

            }
        }
        redirectAttributes.addFlashAttribute("msginfo", "تم الارشفه بنجاح");
        return "redirect:/archive/";
    }
}
