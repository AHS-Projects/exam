package com.exam.backend.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.exam.backend.object.BaseBackEnd;
import com.exam.common.model.Tblstage;
import com.exam.common.service.GroupSeriesService;
import com.exam.common.service.ModelService;
import com.exam.common.service.StageService;
import com.exam.common.service.StudentService;

@Controller
public class HomeController extends BaseBackEnd
{
	@Autowired
	private GroupSeriesService groupSerService;
	@Autowired
	private ModelService modelService;
	@Autowired
	private StudentService studentService;
	@Autowired
	private StageService stageService;
	
	@GetMapping("/")
	public String viewHome(Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		List<Tblstage>tblstages=stageService.findAllStagesWithStudents();
		Map<String, Integer>studentPerStage=new LinkedHashMap<String, Integer>();
		for(Tblstage tblstage:tblstages)
		{
			studentPerStage.put(tblstage.getStageName(), tblstage.getTblstudentList().size());
		}
		thModel.addAttribute("studentPerStage",studentPerStage);
		long countAllNotCorrectedModel=groupSerService.countAllModelsNotCorrected();
		long countNotCorrectedDailyModel=groupSerService.countAllModelsNotCorrectedByModelType("daily");
		long countNotCorrectedMonthlyModel=groupSerService.countAllModelsNotCorrectedByModelType("monthly");
		long countDailyModels=modelService.countAllModelsByModelType("daily");
		long countMonthlyModels=modelService.countAllModelsByModelType("monthly");
		long countActiveStudents=studentService.countAllStudentBuStatsus(0);
		long countBlockedStudents=studentService.countAllStudentBuStatsus(1);
		thModel.addAttribute("countActiveStudents",countActiveStudents);
		thModel.addAttribute("countBlockedStudents",countBlockedStudents);
		thModel.addAttribute("countDailyModels",countDailyModels);
		thModel.addAttribute("countMonthlyModels",countMonthlyModels);
		thModel.addAttribute("countAllNotCorrectedModel",countAllNotCorrectedModel);
		thModel.addAttribute("countNotCorrectedDailyModel",countNotCorrectedDailyModel);
		thModel.addAttribute("countNotCorrectedMonthlyModel",countNotCorrectedMonthlyModel);
		return "home";
	}

}
