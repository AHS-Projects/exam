package com.exam.backend.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.exam.backend.object.BaseBackEnd;
import com.exam.backend.object.TeacherRole;
import com.exam.common.model.Tblrole;
import com.exam.common.model.Tblteacher;
import com.exam.common.model.Tblteacherrole;
import com.exam.common.service.RoleService;
import com.exam.common.service.TeacherRoleService;
import com.exam.common.service.TeacherService;

@Controller
@RequestMapping("/teacher")
public class TeacherController extends BaseBackEnd 
{
	@Autowired
	private TeacherService teacherService;
	@Autowired
	private TeacherRoleService teacherRoleService;
	@Autowired
	private RoleService roleService;
	
	@InitBinder
	 protected void initBinder(WebDataBinder binder) 
	 {
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 dateFormat.setLenient(false);
	     binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));  
	 } 
	
	@GetMapping("/list")
	public String teacherList(Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("اداره المُدَرِّسين", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		List<Tblteacher>teachers=teacherService.findAllTeachers();
		thModel.addAttribute("teachers",teachers);
		return "teacherlist";
	}
	
	@GetMapping("/add")
	public String addTeacher(Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("اداره المُدَرِّسين", "/teacher/list");
		breadcrumbs.put("اضافه او تعديل البيانات", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblteacher tblteacher=new Tblteacher();
		thModel.addAttribute("teacher",tblteacher);
		return "teacherform";
	}
	@GetMapping("/update/{id}")
	public String updateTeacher(@PathVariable("id")long teacherID,Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("اداره المُدَرِّسين", "/teacher/list");
		breadcrumbs.put("اضافه او تعديل البيانات", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblteacher tblteacher=teacherService.getTeacher(teacherID);
		thModel.addAttribute("teacher",tblteacher);
		return "teacherform";
	}
	@GetMapping("/update")
	public String updateCurrentTeacher(Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("اداره المُدَرِّسين", "/teacher/list");
		breadcrumbs.put("اضافه او تعديل البيانات", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblteacher tblteacher=getCurrentTeacher();
		thModel.addAttribute("teacher",tblteacher);
		return "teacherform";
	}
	@PostMapping("/save")
	public String saveTeacher(@Valid @ModelAttribute("teacher")Tblteacher tblteacher,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes)
	{
		if(bindingResult.hasErrors())
		{
			return "teacherform";
		}
		else
		{
			if(tblteacher.getTeacherID()==null)
			{
				tblteacher.setCreatedDate(new Date());
				tblteacher.setUpdatedDate(null);
				tblteacher.setConfirmationTeacherPassword(tblteacher.getTeacherPassword());
				tblteacher.setTeacherPassword(encrytePassword(tblteacher.getTeacherPassword()));
				teacherService.addTeacher(tblteacher);
			}
			else
			{
				tblteacher.setUpdatedDate(new Date());
				tblteacher.setConfirmationTeacherPassword(tblteacher.getTeacherPassword());
				tblteacher.setTeacherPassword(encrytePassword(tblteacher.getTeacherPassword()));
				teacherService.updateTeacher(tblteacher);
			}
		}
		redirectAttributes.addFlashAttribute("saveTeacher","تم تخزين البيانات بنجاح");
		return "redirect:/teacher/list";
	}
	
	@GetMapping("/delete/{id}")
	public String deleteTeacher(@PathVariable("id")long teacherID,final RedirectAttributes redirectAttributes)
	{
		teacherService.deleteTeacher(teacherID);
		redirectAttributes.addFlashAttribute("deleteTeacher","تم مسح المدرس بنجاح");
		return "redirect:/teacher/list";
	}
	@GetMapping("/role/{teacherID}")
	public String getTeacherRoles(@PathVariable("teacherID")long teacherID,Model thModel)
	{
		///////////////////////////
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("اداره المُدَرِّسين", "/teacher/list");
		breadcrumbs.put("ادوار المستخدم", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////
		Tblteacher teacher=teacherService.getTeacher(teacherID);
		List<Tblteacherrole>teacherRolesList=teacher.getTblteacherroleList();
		List<Tblrole>allRoles=roleService.findAllRoles();
		List<Tblrole>teacherRoles=new ArrayList<>();
		TeacherRole teacherRole=new TeacherRole();
		for(Tblteacherrole tblteacherrole:teacherRolesList)
		{
			teacherRoles.add(tblteacherrole.getRoleID());
			allRoles.remove(tblteacherrole.getRoleID());
		}
		thModel.addAttribute("roles",teacherRoles);
		thModel.addAttribute("allRoles",allRoles);
		thModel.addAttribute("userVarID",teacherID);
		thModel.addAttribute("userRole",teacherRole);
		return "teacherrolelist";
	}
	
	@PostMapping("/role/add/{teacherID}")
	public String addTeacherRole(@PathVariable("teacherID")long teacherID,@Valid @ModelAttribute("userRole")TeacherRole userRole
			,BindingResult bindingResult,final RedirectAttributes attributes)
	{
		if(bindingResult.hasErrors())
		{
			return "teacherrolelist";
		}
		try
		{
			Tblteacher teacher=teacherService.getTeacher(teacherID);
			Tblteacherrole tbluserrole=new Tblteacherrole();
			tbluserrole.setTeacherID(teacher);
			tbluserrole.setRoleID(userRole.getRoleID());
			teacherRoleService.addTeacherRole(tbluserrole);
		}
		catch (Exception e) {
			System.out.println(e);
		}
		attributes.addFlashAttribute("addrole","تم اضافه الدور للمدرس");
		return "redirect:/teacher/role/"+teacherID;
	}
	
	@GetMapping("/role/delete/{roleID}/{teacherID}")
	public String deleteTeacherRole(@PathVariable("teacherID") long teacherID,@PathVariable("roleID") long roleID
			,final RedirectAttributes attributes)
	{
		Tblteacherrole tblteacherrole= teacherRoleService.findTeacherRoleByTeacherIDAndRoleID(teacherID, roleID);
		teacherRoleService.deleteTeacherRole(tblteacherrole.getTeacherRoleID());
		attributes.addFlashAttribute("delete","تم مسح الدور من المدرس");
		return "redirect:/teacher/role/"+teacherID;
	}

}
