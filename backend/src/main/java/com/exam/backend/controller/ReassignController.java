//package com.exam.backend.controller;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//import com.exam.common.model.Tblgroupseries;
//import com.exam.common.service.GroupSeriesService;
//import com.exam.common.service.StudentAnswerService;
//
//@Controller
//public class ReassignController
//{
//	@Autowired
//	GroupSeriesService groupSeriesService;
//	@Autowired
//	StudentAnswerService studentAnswerService;
//
//	@GetMapping("/reassign")
//	public String login()
//	{
//		return "reassign";
//	}
//
//	@GetMapping("/reassign/do")
//	public String reassign(final RedirectAttributes redirectAttributes)
//	{
//
//		List<Tblgroupseries> tblgroupseries=groupSeriesService.getGroubByModelMarkAndModelFinished();
//		for(Tblgroupseries tblgroupser:tblgroupseries)
//		{
//
//			tblgroupser.setIsModelCorrected(0);
//			tblgroupser.setIsModelFinished(0);
//			tblgroupser.setStartDate(null);
//			tblgroupser.setFinishedDate(null);
//			groupSeriesService.updateGroupSeries(tblgroupser);
//			//remove all answers
//			int removed=studentAnswerService.deleteStudAnswerByStudIDAndModelID(tblgroupser.getStudentID().getStudentID() , tblgroupser.getModelID().getModelID());
//			System.out.println("student answer deleted "+removed);
//		}
//		redirectAttributes.addFlashAttribute("reassignmsg","تم اعاده التعيين بنجاح");
//		return "redirect:/reassign";
//	}
//
//}
