package com.exam.backend.controller;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.exam.backend.object.BaseBackEnd;
import com.exam.common.model.Tblchoice;
import com.exam.common.model.Tblquestion;
import com.exam.common.service.ChoiceService;
import com.exam.common.service.QuestionService;

@Controller
@RequestMapping("/choice")
public class ChoiceController extends BaseBackEnd
{
	@Autowired
	private ChoiceService choiceService;
	@Autowired
	private QuestionService questionService;
	
	@ModelAttribute("choiceAnswer")
	public Map<Integer, String> getModelPublished()  {

		Map<Integer,String> types = new LinkedHashMap<Integer,String>();
		types.put(0, "لا");
		types.put(1, "نعم");
		return types;
	}
	
	
	@GetMapping("/list/{questID}")
	public String gelAllchoiceByQuestionID(@PathVariable("questID")long questionID,Model thModel)
	{
		Tblquestion tblquestion=questionService.getQuestion(questionID);
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("اجزاء النموذج "+tblquestion.getSectionID().getModelID().getModelName(), "/section/list/"+tblquestion.getSectionID().getModelID().getModelID());
		breadcrumbs.put("اسئله "+tblquestion.getSectionID().getSectionName(), "/question/list/"+tblquestion.getSectionID().getSectionID());
		breadcrumbs.put("اختيارات "+tblquestion.getSectionID().getSectionName(), "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		List<Tblchoice>choices= choiceService.findAllChoicesByQuestionID(questionID);
		thModel.addAttribute("choices",choices);
		return "choicelist";
	}
	@GetMapping("/add/{questID}")
	public String addChoice(@PathVariable("questID")long questionID,Model thModel)
	{
		Tblquestion tblquestion=questionService.getQuestion(questionID);
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("اجزاء النموذج "+tblquestion.getSectionID().getModelID().getModelName(), "/section/list/"+tblquestion.getSectionID().getModelID().getModelID());
		breadcrumbs.put("اسئله "+tblquestion.getSectionID().getSectionName(), "/question/list/"+tblquestion.getSectionID().getSectionID());
		breadcrumbs.put("اختيارات "+tblquestion.getSectionID().getSectionName(), "/choice/list/"+questionID);
		breadcrumbs.put("اضافه او تعديل اختيار", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		Tblchoice choice=new Tblchoice();
		thModel.addAttribute("choice",choice);
		thModel.addAttribute("questionID",questionID);
		return "choiceform";
	}
	@GetMapping("/update/{questID}/{choiceID}")
	public String updateChoice(@PathVariable("questID")long questionID,@PathVariable("choiceID")long choiceID
			,Model thModel)
	{
		Tblquestion tblquestion=questionService.getQuestion(questionID);
		//breadcrumb
		breadcrumbs.clear();
		breadcrumbs.put("الرئيسيه", "/");
		breadcrumbs.put("النماذج", "/model/list");
		breadcrumbs.put("اجزاء النموذج "+tblquestion.getSectionID().getModelID().getModelName(), "/section/list/"+tblquestion.getSectionID().getModelID().getModelID());
		breadcrumbs.put("اسئله "+tblquestion.getSectionID().getSectionName(), "/question/list/"+tblquestion.getSectionID().getSectionID());
		breadcrumbs.put("اختيارات "+tblquestion.getSectionID().getSectionName(), "/choice/list/"+questionID);
		breadcrumbs.put("اضافه او تعديل اختيار", "#");
		thModel.addAttribute("breadcrumbs",breadcrumbs);
		////////////////////////////////////////////////
		Tblchoice choice=choiceService.getChoice(choiceID);
		thModel.addAttribute("choice",choice);
		thModel.addAttribute("questionID",questionID);
		return "choiceform";
	}
	
	@PostMapping("/save/{questID}")
	public String saveChoices(@PathVariable("questID")long questionID,@Valid @ModelAttribute("choice") Tblchoice choice
			,BindingResult bindingResult,final RedirectAttributes redirectAttributes)
	{
		if(bindingResult.hasErrors())
		{
			return "choiceform";
		}
		if(choice.getChoiceID()==null)
		{
			Tblquestion tblquestion=questionService.getQuestion(questionID);
			choice.setQuestionID(tblquestion);
			choiceService.addChoice(choice);
		}
		else
		{
			choiceService.updateChoice(choice);
		}
		redirectAttributes.addFlashAttribute("saveChoice","تم تخزين البيانات بنجاح");
		return "redirect:/choice/list/"+questionID;
	}
	@GetMapping("/delete/{questID}/{choiceID}")
	public String deleteChoice(@PathVariable("questID")long questID,@PathVariable("choiceID")long choiceID
			,final RedirectAttributes redirectAttributes)
	{
		
		choiceService.deleteChoice(choiceID);
		redirectAttributes.addFlashAttribute("deleteChoice","تم مسح الاختيار بنجاح");
		return "redirect:/choice/list/"+questID;
	
	}

}
