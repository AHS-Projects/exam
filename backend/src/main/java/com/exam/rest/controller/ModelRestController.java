package com.exam.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.exam.backend.object.Model;
import com.exam.common.model.Tblmodel;
import com.exam.common.service.ModelService;

@RestController
@RequestMapping("/model/rest")
public class ModelRestController 
{
	@Autowired
	private ModelService modelService;
	
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public List<Model> productList()
	{
	
		List<Model> models=new ArrayList<>();
		List<Tblmodel>tblmodels= modelService.findAllModels();
		for(Tblmodel tblmodel:tblmodels)
		{
			Model model=new Model();
		    model.setCreatedByUser(tblmodel.getCreatedByUserID().getTeacherName());
			model.setModelDesc(tblmodel.getModelDesc());
			model.setModelID(tblmodel.getModelID());
			model.setModelName(tblmodel.getModelName());
			model.setStageName(tblmodel.getStageID().getStageName());
			model.setModelType(tblmodel.getModelType());
			model.setStageID(tblmodel.getStageID().getStageID());
			model.setIsPublished(tblmodel.getIsPublished());
			if(tblmodel.getModelTime()==null)
			{
				model.setModelTime("لم بتم تحديد الوقت");
			}
			else
			{
				model.setModelTime(tblmodel.getModelTime().toString());
			}
			models.add(model);
		}
		
		return models;
	}

}
