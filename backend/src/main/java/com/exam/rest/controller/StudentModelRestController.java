package com.exam.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.exam.backend.object.GroupSeries;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.service.GroupSeriesService;


@RestController
@RequestMapping("/student/model/rest")
public class StudentModelRestController 
{

	@Autowired
	private GroupSeriesService groupSeriesService;
	
	@GetMapping("/notcorrect/list/{stageID}")
	public List<GroupSeries>getAllNotCorrectedModel(@PathVariable("stageID")long stageID)
	{
		List<Tblgroupseries>tblgroupseries=groupSeriesService.findAllNotCorrectModel();
		List<GroupSeries>groupSeries=new ArrayList<GroupSeries>();
		for(Tblgroupseries tblgroupser:tblgroupseries)
		{
			if(tblgroupser.getGroupID().getStageID().getStageID()==stageID)
			{
				GroupSeries groupSer=new GroupSeries();
				groupSer.setAssignedByTecher(tblgroupser.getAssignedByTecherID().getTeacherName());
				groupSer.setAssignedDate(tblgroupser.getAssignedDate().toString());
				groupSer.setFinishedDate(tblgroupser.getFinishedDate().toString());
				groupSer.setGroupID(tblgroupser.getGroupID().getGroupID());
				groupSer.setGroupName(tblgroupser.getGroupID().getGroupName());
				groupSer.setGroupSeriesID(tblgroupser.getGroupSeriesID());
				groupSer.setIsModelFinished(tblgroupser.getIsModelFinished());
				groupSer.setModelID(tblgroupser.getModelID().getModelID());
				groupSer.setModelMark(tblgroupser.getModelID().getModelMark());
				groupSer.setModelName(tblgroupser.getModelID().getModelName());
				groupSer.setModelType(tblgroupser.getModelID().getModelType());
				groupSer.setStageName(tblgroupser.getGroupID().getStageID().getStageName());
				groupSer.setStageID(tblgroupser.getGroupID().getStageID().getStageID());
				groupSer.setStartDate(tblgroupser.getStartDate().toString());
				groupSer.setStudentDadPhone(tblgroupser.getStudentID().getStudentDadPhone());
				groupSer.setStudentID(tblgroupser.getStudentID().getStudentID());
				groupSer.setStudentName(tblgroupser.getStudentID().getStudentName());
				groupSer.setStudentPhone(tblgroupser.getStudentID().getStudentPhone());
				
				groupSeries.add(groupSer);
			}
		}
		return groupSeries;
	}
	@GetMapping("/corrected/list/{stageID}")
	public List<GroupSeries>getAllSeries(@PathVariable("stageID")long stageID)
	{
		List<Tblgroupseries>tblgroupseries=groupSeriesService.findAllCorrectedModel();
		List<GroupSeries>groupSeries=new ArrayList<GroupSeries>();
		for(Tblgroupseries tblgroupser:tblgroupseries)
		{
			if(tblgroupser.getGroupID().getStageID().getStageID()==stageID)
			{
				GroupSeries groupSer=new GroupSeries();
				groupSer.setAssignedByTecher(tblgroupser.getAssignedByTecherID().getTeacherName());
				groupSer.setAssignedDate(tblgroupser.getAssignedDate().toString());
				groupSer.setFinishedDate(tblgroupser.getFinishedDate().toString());
				groupSer.setGroupID(tblgroupser.getGroupID().getGroupID());
				groupSer.setGroupName(tblgroupser.getGroupID().getGroupName());
				groupSer.setGroupSeriesID(tblgroupser.getGroupSeriesID());
				groupSer.setIsModelFinished(tblgroupser.getIsModelFinished());
				groupSer.setModelID(tblgroupser.getModelID().getModelID());
				groupSer.setModelMark(tblgroupser.getModelID().getModelMark());
				groupSer.setModelName(tblgroupser.getModelID().getModelName());
				groupSer.setModelType(tblgroupser.getModelID().getModelType());
				groupSer.setStageName(tblgroupser.getGroupID().getStageID().getStageName());
				groupSer.setStageID(tblgroupser.getGroupID().getStageID().getStageID());
				groupSer.setStartDate(tblgroupser.getStartDate().toString());
				groupSer.setStudentDadPhone(tblgroupser.getStudentID().getStudentDadPhone());
				groupSer.setStudentID(tblgroupser.getStudentID().getStudentID());
				groupSer.setStudentName(tblgroupser.getStudentID().getStudentName());
				groupSer.setStudentPhone(tblgroupser.getStudentID().getStudentPhone());
				groupSer.setStudentMark(tblgroupser.getStudentMark());
				groupSeries.add(groupSer);
			}
		}
		return groupSeries;
	}
}
