package com.exam.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exam.backend.object.Section;
import com.exam.common.model.Tblsection;
import com.exam.common.service.SectionService;

@RestController
@RequestMapping("/section/rest")
public class SectionRestController 
{
	@Autowired
	private SectionService sectionService;
	
	@GetMapping("/list/{modID}")
	public List<Section> getAllSections(@PathVariable("modID")long modID)
	{
		List<Tblsection>tblsections=sectionService.findSectionByModelID(modID);
		List<Section>sections=new ArrayList<>();
		for(Tblsection tblsection:tblsections)
		{
			Section section=new Section();
			section.setCreatedByTeacher(tblsection.getCreatedByUserID().getTeacherName());
			section.setMark(tblsection.getMark());
			section.setModelID(tblsection.getModelID().getModelID());
			section.setSectionID(tblsection.getSectionID());
			section.setSectionName(tblsection.getSectionName());
			sections.add(section);
		}
		
		return sections;
	}

}
