package com.exam.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exam.common.model.Tblvideo;
import com.exam.common.service.VideoService;

@RestController
@RequestMapping("/video/rest")
public class VideoRestController 
{
	@Autowired
	private VideoService videoService;
	
	@GetMapping("/list")
	public List<Tblvideo> getAllSections()
	{
		List<Tblvideo> videos = videoService.findAllVideos();	

		
		return videos;
	}

}
