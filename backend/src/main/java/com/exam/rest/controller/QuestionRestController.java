package com.exam.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.exam.backend.object.Question;
import com.exam.common.model.Tblquestion;
import com.exam.common.service.QuestionService;

@RestController
@RequestMapping("/question/rest")
public class QuestionRestController 
{
	@Autowired
	private QuestionService quesService;
	
	@GetMapping("/list/{secID}")
	public List<Question> getAllQuestionsBySecID(@PathVariable("secID")long secID)
	{
		List<Tblquestion>tblquestions=quesService.findQuestionsBySecID(secID);
		List<Question>questions=new ArrayList<>();
		
		for(Tblquestion tblquestion : tblquestions)
		{
			Question question=new Question();
			question.setCreatedByTeacher(tblquestion.getCreatedByUserID().getTeacherName());
			question.setTrueFalseAnswer(tblquestion.getTrueFalseAnswer());
			question.setAnswer(tblquestion.getAnwser());
			
			if(tblquestion.getMark()==null)
			{
				question.setMark(0);
			}
			else
			{
				question.setMark(tblquestion.getMark());
			}
			question.setQuestion(tblquestion.getQuestion());
			question.setQuestionID(tblquestion.getQuestionID());
			question.setQuestionType(tblquestion.getQuestionType());
			question.setSectionID(tblquestion.getSectionID().getSectionID());
			questions.add(question);
		}
		
		return questions;
	}
	@GetMapping("/list")
	public List<Question> getAllQuestionsBank()
	{
		List<Tblquestion>tblquestions=quesService.findAllQuestions();
		List<Question>questions=new ArrayList<>();
		
		for(Tblquestion tblquestion : tblquestions)
		{
			Question question=new Question();
			question.setCreatedByTeacher(tblquestion.getCreatedByUserID().getTeacherName());
			question.setTrueFalseAnswer(tblquestion.getTrueFalseAnswer());
			question.setAnswer(tblquestion.getAnwser());
			if(tblquestion.getMark()==null)
			{
				question.setMark(0);
			}
			else
			{
				question.setMark(tblquestion.getMark());
			}
			question.setQuestion(tblquestion.getQuestion());
			question.setQuestionID(tblquestion.getQuestionID());
			question.setQuestionType(tblquestion.getQuestionType());
			question.setSectionID(tblquestion.getSectionID().getSectionID());
			questions.add(question);
		}
		
		return questions;
	}
	

}
