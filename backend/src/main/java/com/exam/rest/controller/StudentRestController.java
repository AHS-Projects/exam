package com.exam.rest.controller;

import com.exam.backend.object.StudentExam;
import com.exam.common.dto.Student;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblstudent;
import com.exam.common.service.GroupSeriesService;
import com.exam.common.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/student/rest")
public class StudentRestController {
    @Autowired
    private StudentService studentService;
    @Autowired
    private GroupSeriesService groupSeriesService;


    @GetMapping("/list")
    public List<Student> getAllStudents() {
        List<Tblstudent> tblstudents = studentService.findAllStudentsWithCurresession();
        List<Student> students = new ArrayList<Student>();
        for (Tblstudent tblstudent : tblstudents) {
            Student student = new Student();
            if (tblstudent.getBlocked() == 0) {
                student.setBlocked("نشط");
            } else {
                student.setBlocked("غير نشط");
            }
            student.setStudentDadPhone(tblstudent.getStudentDadPhone());
            student.setStudentEmail(tblstudent.getStudentEmail());
            student.setStudentID(tblstudent.getStudentID());
            student.setStudentName(tblstudent.getStudentName());
            student.setStudentPhone(tblstudent.getStudentPhone());
            student.setStageName(tblstudent.getStageID().getStageName());
            student.setGroupName(tblstudent.getGroupID().getGroupName());
            students.add(student);
        }

        return students;
    }

    @GetMapping("/list/group/{groupID}/{stageID}/{modelID}")
    public List<Student> getAllStudentByGroup(@PathVariable("groupID") long groupID
            , @PathVariable("stageID") long stageID, @PathVariable("modelID") long modelID) {
        List<Tblstudent> tblstudents = studentService.findStudenstByGroupIDAndStageID(groupID, stageID);
        List<Student> students = new ArrayList<Student>();
        for (Tblstudent tblstudent : tblstudents) {
            Student student = new Student();
            //Tblgroupseries checkGrupSeries = groupSeriesService.findGroupSeriesByStudentAndModelAndGroup(groupID, modelID, tblstudent.getStudentID());
//            if (checkGrupSeries != null) {
//                student.setIsAssigned(1);
//            } else {
//                student.setIsAssigned(0);
//            }
            student.setStudentDadPhone(tblstudent.getStudentDadPhone());
            student.setStudentEmail(tblstudent.getStudentEmail());
            student.setStudentID(tblstudent.getStudentID());
            student.setStudentName(tblstudent.getStudentName());
            student.setStudentPhone(tblstudent.getStudentPhone());
            student.setStageName(tblstudent.getStageID().getStageName());
            student.setGroupName(tblstudent.getGroupID().getGroupName());
            students.add(student);
        }

        return students;
    }


    @GetMapping("/exam/list/{studentID}")
    public List<StudentExam> getAllStudentExaams(@PathVariable("studentID") long studentID) {
        Tblstudent student = studentService.getStudentWithGroupSerios(studentID);
        List<StudentExam> exams = new ArrayList<StudentExam>();
        if (student != null) {
            for (Tblgroupseries tblgroupseries : student.getTblgroupseriesList()) {
                StudentExam studentExam = new StudentExam();
                studentExam.setModelName(tblgroupseries.getModelID().getModelName());
                if (tblgroupseries.getIsModelFinished() == 1) {
                    studentExam.setModelStatus("تم الانتهاء");
                } else {
                    studentExam.setModelStatus("لم يتم الاختبار حتى الان");
                }
                if (tblgroupseries.getIsModelCorrected() == null || tblgroupseries.getIsModelCorrected() == 0) {
                    studentExam.setModelCorrected("لم يتم التصحيح");
                } else {
                    studentExam.setModelCorrected("تم التصحيح");

                }

                studentExam.setTotalModelMarks(tblgroupseries.getModelID().getModelMark());
                studentExam.setModelType(tblgroupseries.getModelID().getModelType());
                studentExam.setStudentName(tblgroupseries.getStudentID().getStudentName());
                if (tblgroupseries.getStudentMark() != null) {
                    studentExam.setStudentMarks(tblgroupseries.getStudentMark());
                }
                //			if(tblgroupseries.getStudentMark()==null)
                //			{
                //				List<Tblstudentanswer>tblstudentanswers=studAnswerService.findAllAnsersByStudentAndModel(tblgroupseries.getStudentID().getStudentID(), tblgroupseries.getModelID().getModelID());
                //				double studentMark=0;
                //				for(Tblstudentanswer tblstudentanswer:tblstudentanswers)
                //				{
                //					studentMark=studentMark+tblstudentanswer.getMark();
                //				}
                //				studentExam.setStudentMarks(studentMark);
                //				tblgroupseries.setStudentMark(studentMark);
                //				//we need fix it
                //				//groupSeriesService.updateGroupSeries(tblgroupseries);
                //			}
                //			else
                //			{
                //studentExam.setStudentMarks(tblgroupseries.getStudentMark());
                //}

                exams.add(studentExam);
            }
        }
        return exams;
    }

}
