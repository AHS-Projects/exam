package com.exam.editor;

import java.beans.PropertyEditorSupport;
import com.exam.common.model.Tblgroup;


public class GroupEditor extends PropertyEditorSupport 
{
	 @Override
	 public void setAsText(String id)
	 {
		 if(id.equals(""))
		    {
		    	this.setValue(null);
		    }
		    else
		    {
		        Tblgroup g=new Tblgroup();
		        g.setGroupID(Long.parseLong(id));
		        this.setValue(g);
		    }
	  }
}
