package com.exam.editor;

import java.beans.PropertyEditorSupport;
import com.exam.common.model.Tblstage;


public class StageEditor extends PropertyEditorSupport 
{	
	 @Override
	 public void setAsText(String id)
	 {
		    if(id.equals(""))
		    {
		    	this.setValue(null);
		    }
		    else
		    {
		        Tblstage s=new Tblstage();
		        s.setStageID(Long.parseLong(id));
		        this.setValue(s);
		    }
	  }
}
