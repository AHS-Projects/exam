<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>اضافه او تعديل البيانات</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
</head>

<body>
 
 <!-- Sidenav -->
	  <jsp:directive.include file = "templete/rightbar.html" />
	  <!-- Main content -->
	  <div class="main-content">
	  <!-- Top navbar -->
	  <jsp:directive.include file = "templete/topbar.jsp" />
	  
	  <!-- Page content -->
	  
	   <div class="container-fluid mt--7">
	    <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">اضافه او تعديل البيانات</h3>
            </div>
         <form:form  modelAttribute="teacher" action="/teacher/save" method="post" >
				      <form:hidden path="teacherID" />
				      <form:hidden path="createdDate" />
				      <form:hidden path="updatedDate" />
				       
					  <div class="row">
					    <div class="col-md-8">
					      <label >اسم المدرس</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="teacherName"  placeholder="اسم المدرس" />
         				    <form:errors path="teacherName"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >كلمه السر</label>
					      <div class="form-group">
					        <form:password  class="form-control" path="teacherPassword"  placeholder="كلمه السر" />
					        <form:errors path="teacherPassword"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >وظيفه المدرس</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="teacherType"  placeholder="وظيفه المدرس" />
					        <form:errors path="teacherType"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  					 
		             <button style="float: right;" type="submit" class="btn btn-primary" >حفظ</button>
			     </form:form>	
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <jsp:directive.include file = "templete/footer.html" />
  <!-- Argon Scripts -->
  <!-- Core -->
  <jsp:directive.include file = "templete/js.html" />
</body>

</html>