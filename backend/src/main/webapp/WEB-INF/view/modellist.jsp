<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>النماذج</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
    
          <c:if test="${not empty saveModel}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${saveModel}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			 <c:if test="${not empty deleteModel}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${deleteModel}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			  <!-- Modal Approval -->
			 <div class="modal fade" id="deletemodelid"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="approveLabel"> مسح النموذج</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      
				      <div class="modal-body">
				        هل انت متاكد؟
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">لا</button>
				        <a role="button" id="deleteLink" class="btn btn-primary" href="#">نعم</a>
				      </div>
				    </div>
				  </div>
			 </div>   
			 
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">النماذج</h3>
            </div>
           
            <div class="table-responsive">
     
              <table id="tblmodelid" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">اسم النموذج</th>
                    <th scope="col">نوع النموذج</th>
                    <th scope="col">وصف النموذج</th>
                    <th scope="col">المرحله</th>
                    <th scope="col">وقت النموذج بالدقيقه</th>
                    <th scope="col">تم نشره؟</th>
                    <th scope="col">تم بواسطه</th>
                    <th scope="col">اجزاء النموذج</th>
                    <th scope="col" >المجموعات و الطلبه</th>
                    <th scope="col">تعديل</th>
                    <th scope="col">مسح</th>
                  </tr>
                </thead>
                <tbody>
              
	                  <tr>
	                   
	                    <td>
	                      modelName
	                    </td>
	                    <td>
	                      modelType
	                    </td>
	                    <td>
	                      modelDesc
	                    </td>
	                    <td>
	                      stageName
	                    </td>
	                    <td>
	                      modelTime
	                    </td>
	                    <td>
	                      isPublished
	                    </td>
	                    <td>
	                      teacherName
	                    </td>
	                    <td>
	                      displaySections
	                    </td>
	                     <td>
	                      groups
	                    </td>
	                    <td>
	                      edit
	                    </td>
	                    <td>
	                     delete
	                    </td>
	                   
	                  </tr>
                 
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
      <spring:url value="/model/add" var="addurl" />
	  <a class="btn btn-primary" style="float: right;" href="${addurl}" role="button" >اضافه نموذج</a>
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
 
  <script type="text/javascript">
	  $(document).ready( function () {
			 var table = $('#tblmodelid').DataTable({
				    "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			        "iDisplayLength": 5, 
					"sAjaxSource": "/model/rest/list",
					"sAjaxDataProp": "",
					"order": [[ 0, "asc" ]],
					"aoColumns": [
						
				      { "mData": "modelName" },
				      { "mData": "modelType",render: getModelType },
				      { "mData": "modelDesc" },
				      { "mData": "stageName" }, 
				      { "mData": "modelTime" },
				      { "mData": "isPublished",render: isPublishedModel },
				      { "mData": "createdByUser" },
				      { "mData": "displaySections", render: showSections },
				      { "mData": "groups", render: displayGroups },
				      { "mData": "edit", render: edit },
				      { "mData": "delete", render: deletemodel }
					]
			 })
		});
	  
	  function displayGroups(data, type, row, meta) {
		  
		   return '<a class="btn btn-primary" href="/group/list/'+row.stageID+'/'+row.modelID+'" role="button" >عرض</a>';
	       
	    }
	  function isPublishedModel(data, type, row, meta) {
		  
		   if(row.isPublished==0)
	         return 'لا';
		   else
			   return 'نعم'; 
	    }
	  function getModelType(data, type, row, meta) {
		  
		   if(row.modelType=='daily')
	         return 'يومى';
		   else if(row.modelType=='monthly')
			   return 'شهرى'; 
	    }
	  
	  function showSections(data, type, row, meta) {
		  
		   return '<a class="btn btn-primary" href="/section/list/'+row.modelID+'" role="button" >عرض</a>';
	       
	    }
	  function edit(data, type, row, meta) {
		  
		   return '<a class="btn btn-primary" href="/model/update/'+row.modelID+'" role="button" >تعديل</a>';
	       
	    }
	  function deletemodel(data, type, row, meta) {

		   return '<a class="btn btn-primary" href="#" role="button" data-toggle="modal" onclick="deleteModel('+row.modelID+');" >مسح</a>';
	       
	    }
	  
	 
		function deleteModel(modelID)
		{
			 var deleteUrl="/model/delete/"+modelID;
			 $("#deleteLink").attr("href", deleteUrl);
			 $("#deletemodelid").modal();
		}
	    
  </script>
 
  
</body>

</html>