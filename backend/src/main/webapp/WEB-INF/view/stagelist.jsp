<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>المراحل</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
    
          <c:if test="${not empty saveStage}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${saveStage}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			 <c:if test="${not empty deleteStage}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${deleteStage}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			  <!-- Modal Approval -->
			 <div class="modal fade" id="deletestage"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="approveLabel">مسح المرحله</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        هل انت متاكد؟
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">لا</button>
				        <a role="button" id="deleteLink" class="btn btn-primary" href="#">نعم</a>
				      </div>
				    </div>
				  </div>
			 </div>   
			 
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">المراحل</h3>
            </div>
            <div class="table-responsive">
             <h4 class="text-danger" >${error}</h4>
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">اسم المرحله</th>
                    <th scope="col">وصف المرحله</th>
                    <th scope="col">تم بواسطه</th>
                    <th scope="col">عرض مجموعات المرحله</th>
                    <th scope="col">تعديل</th>
                    <th scope="col">مسح</th>
                  </tr>
                </thead>
                <tbody>
                 <c:forEach items="${stages}" var="stage" >
	                  <tr>
	                   
	                    <td>
	                      ${stage.stageName}
	                    </td>
	                    <td>
	                      ${stage.stageDesc}
	                    </td>
	                     <td>
	                      ${stage.createdByUserID.teacherName}
	                    </td>
	                    <td>
	                      <spring:url value="/group/list/${stage.stageID}" var="disrole" />
	                      <a class="btn btn-primary" href="${disrole}" role="button" >عرض</a>
	                    </td>
	                    <td>
	                      <spring:url value="/stage/update/${stage.stageID}" var="updateurl" />
	                      <a class="btn btn-primary" href="${updateurl}" role="button" >تعديل</a>
	                    </td>
	                    <td>
	                      <spring:url value="/stage/delete/${stage.stageID}" var="deleteurl" />
	                      <a class="btn btn-primary" href="#" role="button"  onclick="deleteStage(${stage.stageID});" >مسح</a>
	                    </td>
	                   
	                  </tr>
                 </c:forEach>
                 
                </tbody>
              </table>
             
            </div>

          </div>
        </div>
      </div>
      <spring:url value="/stage/add" var="addurl" />
	  <a class="btn btn-primary" style="float: right;" href="${addurl}" role="button" >اضافه مرحله</a>
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
 <script type="text/javascript">
 function deleteStage(stageID)
 {
	 var deleteUrl="/stage/delete/"+stageID;
	 $("#deleteLink").attr("href", deleteUrl);
	 $("#deletestage").modal();
 }
 </script>
  
</body>

</html>