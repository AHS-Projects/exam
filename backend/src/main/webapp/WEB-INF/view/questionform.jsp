<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>اضافه او تعديل سؤال</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
</head>

<body>
 
 <!-- Sidenav -->
	  <jsp:directive.include file = "templete/rightbar.html" />
	  <!-- Main content -->
	  <div class="main-content">
	  <!-- Top navbar -->
	 <jsp:directive.include file = "templete/topbar.jsp" />
	  
	  <!-- Page content -->
	  
	   <div class="container-fluid mt--7">
	    <div class="col">
	    
	      <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">بنك الاسئله</h3>
                <div class="table-responsive">
     
	              <table id="tblquesid" class="table align-items-center table-flush">
	                <thead class="thead-light">
	                  <tr>
	                    <th scope="col">السؤال</th>
	                    <th scope="col">الاجابه</th>
	                    <th scope="col">درجه السؤال</th>
	                    <th scope="col">نوع السؤال</th>
	                    <th scope="col">اختيارات السؤال</th>
	                    <th scope="col">تم بواسطه</th>
	                   
	                  </tr>
	                </thead>
	                <tbody>
		                  <tr>
		                   
		                    <td>
		                      question
		                    </td>
		                    <td>
		                      anwser
		                    </td>
		                    <td>
		                      mark
		                    </td>
		                     <td>
		                      questionType
		                    </td>
		                    <td>
		                      questionChoice
		                    </td>
		                    <td>
		                      teacherName
		                    </td>
		              
		                  </tr>
	                 
	                </tbody>
	              </table>
              </div>
            </div>
           </div>
	     
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">اضافه او تعديل سؤال</h3>
            </div>
         <form:form  modelAttribute="question" action="/question/save/${secID}" method="post" >
				      <form:hidden path="questionID" />
				      <form:hidden path="sectionID.sectionID" />
				      <form:hidden path="createdDate" />
				      <form:hidden path="updatedDate" />
				      <form:hidden path="createdByUserID.teacherID" />
				      <form:hidden path="updatedByUserID.teacherID" />
					  <div class="row">
					    <div class="col-md-8">
					      <label >السؤال</label>
					      <div class="form-group">
					        <form:textarea id="questextareaid" class="form-control" path="question"  placeholder="السؤال" />
         				    <form:errors path="question"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					
					  <c:choose>
						    <c:when test="${not empty question.questionType and question.questionType eq 'rightorwrong'}">
						        
						    </c:when>    
						    <c:otherwise>
						       
						    </c:otherwise>
					  </c:choose>
					  
					  <div class="row">
					    <div class="col-md-8">
					      <label >نوع السؤال</label>
					      <div class="form-group">
					      <form:select id="typeselectid" class="form-control" path="questionType">
    						<form:options items="${questiontypes}" />
						  </form:select>
					       <form:errors path="questionType" cssClass="text-danger" />
					      </div>
					    </div>
					  </div>
					  
					    <c:choose>
						    <c:when test="${not empty question.questionType and question.questionType eq 'rightorwrong'}">
						    
						          <div id="answerid" class="row" style="display: none;">
								    <div class="col-md-8">
								      <label >الاجابه</label>
								      <div class="form-group">
								        <form:textarea  class="form-control" path="anwser"  placeholder="الاجابه" />
								        <form:errors path="anwser"  class="text-danger"  />
								      </div>
								    </div>
								  </div>
								  
								  <div id="tfanswerid" class="row" >
								    <div class="col-md-8">
								      <label >الاجابه</label>
								      <div class="form-group">
									      <form:select class="form-control" path="trueFalseAnswer">
				    						<form:options items="${truefalsetypes}" />
										  </form:select>
									       <form:errors path="trueFalseAnswer" cssClass="text-danger" />
								      </div>
								    </div>
								  </div>
						    </c:when>    
						    <c:otherwise>
						         <div id="answerid" class="row">
								    <div class="col-md-8">
								      <label >الاجابه</label>
								      <div class="form-group">
								        <form:textarea  class="form-control" path="anwser"  placeholder="الاجابه" />
								        <form:errors path="anwser"  class="text-danger"  />
								      </div>
								    </div>
								  </div>
								  
								  <div id="tfanswerid" class="row" style="display: none;">
								    <div class="col-md-8">
								      <label >الاجابه</label>
								      <div class="form-group">
									      <form:select class="form-control" path="trueFalseAnswer">
				    						<form:options items="${truefalsetypes}" />
										  </form:select>
									       <form:errors path="trueFalseAnswer" cssClass="text-danger" />
								      </div>
								    </div>
								  </div>
					  
						    </c:otherwise>
					  </c:choose>		
					    
					
					  <div class="row">
					    <div class="col-md-8">
					      <label >درجه السؤال</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="mark"  placeholder="درجه السؤال" />
					        <form:errors path="mark"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  					 
		             <button style="float: right;" type="submit" class="btn btn-primary" >حفظ</button>
			     </form:form>	
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <jsp:directive.include file = "templete/footer.html" />
  <!-- Argon Scripts -->
  <!-- Core -->
  <jsp:directive.include file = "templete/js.html" />
  <script src="${pageContext.request.contextPath}/resources/js/tinymce.min.js"></script>
  <script>tinymce.init({selector:'#questextareaid'});</script>
   <script type="text/javascript">
	  $(document).ready( function () {
			 var table = $('#tblquesid').DataTable({
				    "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			        "iDisplayLength": 5, 
					"sAjaxSource": "/question/rest/list/",
					"sAjaxDataProp": "",
					"order": [[ 0, "asc" ]],
					"aoColumns": [
						
				      { "mData": "question" },
				      { "mData": "answer" },
				      { "mData": "mark" },
				      { "mData": "questionType",render: getQuestionType },
				      { "mData": "questionChoice", render: getQuestionChoice },
				      { "mData": "createdByTeacher" }
				      
					]
			 })
		});
	  
	
	 
	  function getQuestionChoice(data, type, row, meta) {
		   if(row.questionType=='choice')
		       return '<a class="btn btn-primary" href="/choice/list/'+row.questionID+'" role="button" >عرض</a>' ;
		   else
			   return 'لا يوجد اختيارات للسؤال'; 	   
	       
	    }
	   
	  function getQuestionType(data, type, row, meta) {
		  
		   if(row.questionType=='choice')
	         return 'اختيارى';
		   else if(row.questionType=='meaning')
			   return 'مرادفات'; 
		   else if(row.questionType=='rightorwrong')
			   return 'صح/خطأ';
		   else if(row.questionType=='part')
			   return 'قطعه';
		   else if(row.questionType=='explain')
			   return 'شرح';
	    }
	  
	  
	  $('#typeselectid').on('change', function() {
		 
		   if(this.value =='rightorwrong')
		   {
			  $("#tfanswerid").show();
		      $("#answerid").empty(); 
		   }
		   
		   else
		   {
			   $("#answerid").show();
			   $("#tfanswerid").hide(); 
		   }
		});
	    
  </script>
</body>

</html>