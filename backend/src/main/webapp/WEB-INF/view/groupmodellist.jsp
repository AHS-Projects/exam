<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>مجموعات ${stageName}</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
    
          
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">مجموعات ${stageName}</h3>
            </div>
            <spring:url value="/teacher/update" var="upurl" /> 
            <div class="table-responsive">
             <h4 class="text-danger" >${error}</h4>
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">اسم المجموعه</th>
                    <th scope="col">المرحله</th>
                    <th scope="col">تم بواسطه</th>
                    <th scope="col">طلبه المجموعه</th>

                  </tr>
                </thead>
                <tbody>
                 <c:forEach items="${groups}" var="group" >
	                  <tr>
	                   
	                    <td>
	                      ${group.groupName}
	                    </td>
	                    <td>
	                      ${group.stageID.stageName}
	                    </td>
	                    <td>
	                      ${group.createdByUserID.teacherName}
	                    </td>
	                    <td>
	                      <spring:url value="/student/list/group/${group.groupID}/${group.stageID.stageID}/${modelID}" var="stgurl" />
	                      <a class="btn btn-primary" href="${stgurl}" role="button" >عرض</a>
	                    </td>
	                   
	                  </tr>
                 </c:forEach>
                 
                </tbody>
              </table>
             
            </div>

          </div>
        </div>
      </div>
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
 
  
</body>

</html>