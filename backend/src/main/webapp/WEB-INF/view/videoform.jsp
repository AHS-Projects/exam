<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>اضافه او تعديل رابط فيديو</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
</head>

<body>
 
 <!-- Sidenav -->
	  <jsp:directive.include file = "templete/rightbar.html" />
	  <!-- Main content -->
	  <div class="main-content">
	  <!-- Top navbar -->
	  <jsp:directive.include file = "templete/topbar.jsp" />
	  
	  <!-- Page content -->
	  
	   <div class="container-fluid mt--7">
	    <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">اضافه او تعديل رابط فيديو</h3>
            </div>
         <form:form  modelAttribute="video" action="/video/add" method="post" >
				      <form:hidden path="videoID" />
					  <div class="row">
					    <div class="col-md-8">
					      <label >اسم الفيديو</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="videoName"  placeholder="اسم الفيديو" />
         				    <form:errors path="videoName"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >رابط الفيديو</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="link"  placeholder="رابط الفيديو" />
					        <form:errors path="link"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >وصف الفيديو</label>
					      <div class="form-group">
					        <form:textarea type="text" class="form-control" path="videoDesc"  placeholder="وصف الفيديو" />
					        <form:errors path="videoDesc"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  					 
		             <button style="float: right;" type="submit" class="btn btn-primary" >حفظ</button>
			     </form:form>	
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <jsp:directive.include file = "templete/footer.html" />
  <!-- Argon Scripts -->
  <!-- Core -->
  <jsp:directive.include file = "templete/js.html" />
</body>

</html>