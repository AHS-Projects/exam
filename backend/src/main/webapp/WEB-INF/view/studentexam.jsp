<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>نماذج الطالب</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
    
          
			 
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">نماذج الطالب</h3>
            </div>
           
            <div class="table-responsive">
     
              <table id="tblstudid" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">اسم الطالب</th>
                    <th scope="col">اسم النموذج</th>
                    <th scope="col">نوع النموذج</th>
                    <th scope="col">حاله النموذج</th>
                    <th scope="col">التصحيح</th>
                    <th scope="col">درجه النموذج</th>
                    <th scope="col">درجه الطالب</th>
                    
                  </tr>
                </thead>
                <tbody>
              
	                  <tr>
	                   
	                    <td>
	                      studentName
	                    </td>
	                    <td>
	                      modelName
	                    </td>
	                    <td>
	                      modelType
	                    </td>
	                    <td>
	                      modelStatus
	                    </td>
	                    <td>
	                      corrected
	                    </td>
	                    <td>
	                      totalModelMarks
	                    </td>
	                    <td>
	                      studentMarks
	                    </td>
	                
	                   
	                  </tr>
                 
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
      
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
 
  <script type="text/javascript">
	  $(document).ready( function () {
			 var table = $('#tblstudid').DataTable({
				    "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			        "iDisplayLength": 5, 
					"sAjaxSource": "/student/rest/exam/list/"+${studentID},
					"sAjaxDataProp": "",
					"order": [[ 0, "asc" ]],
					"aoColumns": [
						
				      { "mData": "studentName" },
				      { "mData": "modelName" },
				      { "mData": "modelType",render: getModelType },
				      { "mData": "modelStatus" },
				      { "mData": "modelCorrected" },
				      { "mData": "totalModelMarks" },
				      { "mData": "studentMarks" }
				      
					]
			 })
		});
	  
	  
	  function getModelType(data, type, row, meta) {
		  
		   if(row.modelType=='daily')
	         return 'يومى';
		   else if(row.modelType=='monthly')
			   return 'شهرى'; 
	    }
	  
  </script>
 
  
</body>

</html>