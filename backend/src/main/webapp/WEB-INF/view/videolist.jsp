<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>اجزاء النموذج</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
    
          <c:if test="${not empty saveVideo}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${saveVideo}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			 <c:if test="${not empty deleteVideo}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${deleteVideo}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			  <!-- Modal Approval -->
			 <div class="modal fade" id="deleteVideoModalID"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="approveLabel"> مسح الرابط</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      
				      <div class="modal-body">
				        هل انت متاكد؟
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">لا</button>
				        <a role="button" id="deleteLink" class="btn btn-primary" href="#">نعم</a>
				      </div>
				    </div>
				  </div>
			 </div>   
			 
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0"> روابط الفيديو </h3>
            </div>
           
            <div class="table-responsive">
     
              <table id="tblvideolid" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">اسم الفيديو</th>
                    <th scope="col">رابط الفيديو</th>
                     <th scope="col">وصف الفيديو</th>
                    <th scope="col">تعديل</th>
                    <th scope="col">مسح</th>
                  </tr>
                </thead>
                <tbody>
                 
	                  <tr>
	                  
	                    <td>
	                      videoName
	                    </td>
	                    <td>
	                      link
	                    </td>
	                     <td>
	                      videoDesc
	                    </td>
	                    <td>
	                     edit
	                    </td>
	                    <td>
	                      delete
	                    </td>
	                   
	                  </tr>
                 
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
      <spring:url value="/video/redirectToAddVideo" var="addurl" />
	  <a class="btn btn-primary" style="float: right;" href="${addurl}" role="button" >اضافه رابط فيديو</a>
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
 
 <script type="text/javascript">
	  $(document).ready( function () {
			 var table = $('#tblvideolid').DataTable({
				    "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			        "iDisplayLength": 5, 
					"sAjaxSource": "/video/rest/list/",
					"sAjaxDataProp": "",
					"order": [[ 0, "asc" ]],
					"aoColumns": [
					
					
				      { "mData": "videoName" },
				      { "mData": "link" },
				      { "mData": "videoDesc" },
				      { "mData": "edit", render: edit },
				      { "mData": "delete", render: deleteVideoBtn }
					]
			 })
		});
	  
// 	  function showQues(data, type, row, meta) {
		  
// 		   return '<a class="btn btn-primary" href="/question/list/'+row.sectionID+'" role="button" >عرض</a>';
	       
// 	    }
	  function edit(data, type, row, meta) {
		  
		   return '<a class="btn btn-primary" href="/video/redirectToUpdateVideo/'+row.videoID+'" role="button" >تعديل</a>';
	       
	    }
	  function deleteVideoBtn(data, type, row, meta) {
console.log("video id in delete btn: "+ row.videoID);
		   return '<a class="btn btn-primary" href="#" role="button" data-toggle="modal" onclick="deleteVideoModal('+row.videoID+');"  >مسح</a>';
	       
	    }
	  
	  function deleteVideoModal(videoID)
		{
			 var deleteUrl="/video/delete/"+videoID;
			 $("#deleteLink").attr("href", deleteUrl);
			 $("#deleteVideoModalID").modal();
		}
	    
  </script>
 
 
  
</body>

</html>