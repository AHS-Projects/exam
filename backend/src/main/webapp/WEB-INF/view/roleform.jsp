<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>   
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>   
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>الدور</title>
   <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
</head>
<body>
	  <!-- Sidenav -->
	  <jsp:directive.include file = "templete/rightbar.html" />
	  <!-- Main content -->
	  <div class="main-content">
	  <!-- Top navbar -->
	  <jsp:directive.include file = "templete/topbar.jsp" />
	  
	  <!-- Page content -->
	  
	   <div class="container-fluid mt--7">
	    <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0"></h3>
            </div>
			    <h2 style="text-align: center;">اضافه او تعديل دور</h2>
		        <spring:url value="/role/save" var="saveurl" />
				<form:form  modelAttribute="role" action="${saveurl}" method="post" >
				      <form:hidden path="roleID" />
				      <form:hidden path="createdDate" />
				      <form:hidden path="updatedDate" />
				      <form:hidden path="createdByUserID.teacherID" />
				      <form:hidden path="updatedByUserID.teacherID" />
					  <div class="row">
					    <div class="col-md-8">
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="role"  placeholder="اسم الدور" />
         				    <form:errors path="role"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="roleDesc"  placeholder="وصف الدور" />
					      </div>
					    </div>
					  </div>
		             <button style="float: right;" type="submit" class="btn btn-primary" >حفظ</button>
			</form:form>
		</div>
		</div>
       </div>
	  </div>
	  <!-- Footer -->
	  <jsp:directive.include file = "templete/footer.html" />
	   <!-- js -->
	  <jsp:directive.include file = "templete/js.html" />
</body>

</html>