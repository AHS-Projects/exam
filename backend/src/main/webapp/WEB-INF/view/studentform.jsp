<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>اضافه او تعديل طالب</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
</head>

<body>
 
 <!-- Sidenav -->
	  <jsp:directive.include file = "templete/rightbar.html" />
	  <!-- Main content -->
	  <div class="main-content">
	  <!-- Top navbar -->
	  <jsp:directive.include file = "templete/topbar.jsp" />
	  
	  <!-- Page content -->
	  
	   <div class="container-fluid mt--7">
	    <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">اضافه او تعديل طالب</h3>
            </div>
         <form:form  modelAttribute="student" action="/student/save" method="post" >
				      <form:hidden path="studentID" />
				      <form:hidden path="createdDate" />
				      <form:hidden path="updatedDate" />
				      <form:hidden path="createdByUserID.teacherID" />
				      <form:hidden path="updatedByUserID.teacherID" />
				      
					  <div class="row">
					    <div class="col-md-8">
					      <label >اسم الطالب</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="studentName"  placeholder="اسم الطالب" />
         				    <form:errors path="studentName"  class="text-danger"  />
         				     <c:if test="${not empty uniqueUser}">
					             <div class="alert alert-success fade show" role="alert">
								  <strong>ملحوظه!</strong> ${uniqueUser}.
								  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
								    <span aria-hidden="true">&times;</span>
								  </button>
								 </div>
							 </c:if>
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >البريد الالكترونى</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="studentEmail"  placeholder="البريد الالكترونى" />
         				    <form:errors path="studentEmail"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >كلمه السر</label>
					      <div class="form-group">
					        <c:if  test = "${not empty student.studentID}">
					          <input  readonly="readonly" class="form-control"   placeholder="كلمه السر" />
					          <form:hidden path="studentPassword" />
					        </c:if>
					        <c:if  test = "${empty student.studentID}">
					          <form:password id="password"  onkeyup='check();' class="form-control" path="studentPassword"  placeholder="كلمه السر" />
					        </c:if>
         				    <form:errors path="studentPassword"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					   <div class="row">
					    <div class="col-md-8">
					      <label >تاكيد كلمه السر</label>
					      <div class="form-group">
					        <c:if  test = "${not empty student.studentID}">
				        		<input  readonly="readonly"  class="form-control"   placeholder="تاكيد كلمه السر" />
				        		<form:hidden path="confirmationStudentPassword" />
					        </c:if>
					         <c:if  test = "${empty student.studentID}">
				        		<form:password id="confirm_password"  onkeyup='check();' class="form-control" path="confirmationStudentPassword"  placeholder="تاكيد كلمه السر" />
					        </c:if>
         				    <form:errors path="confirmationStudentPassword"  class="text-danger"  />
         				    <span id='message'></span>
					      </div>
					    </div>
					  </div>
					   <div class="row">
					    <div class="col-md-8">
					      <label >رقم تليفون الطالب</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="studentPhone"  placeholder="رقم تليفون الطالب" />
         				    <form:errors path="studentPhone"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >رقم تليفون ولى امر الطالب</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="studentDadPhone"  placeholder="رقم تليفون ولى امر الطالب" />
         				    <form:errors path="studentDadPhone"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					   <div class="row">
					    <div class="col-md-8">
					      <label >سن الطالب</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="studentAge"  placeholder="سن الطالب" />
         				    <form:errors path="studentAge"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					   
					  <div class="row">
					    <div class="col-md-8">
					      <label >الحاله</label>
					      <div class="form-group">
					      <form:select class="form-control" path="blocked">
    						<form:options items="${blocktypes}" />
						  </form:select>
					       <form:errors path="blocked" cssClass="text-danger" />
					      </div>
					    </div>
					  </div>
					   <!--  <div class="row">
					    <div class="col-md-8">
					      <label >المرحله</label>
					      <div class="form-group">
					        <form:select id="slctstageid" path="stageID" class="form-control" items="${allStages}" itemValue="stageID" itemLabel="stageName" />
					        <form:errors path="stageID" cssClass="text-danger" />
					      </div>
					    </div>
					  </div>-->
					  
					  <div class="row">
					    <div class="col-md-8">
					      <label >المرحله</label>
					      <div class="form-group">
					 			 <form:select class="form-control" id="slctstageid" path="stageID">
						          <option value="">-Select-</option>
						          <form:options items="${allStages}" itemValue="stageID" itemLabel="stageName"/>
						        </form:select>
						        <form:errors path="stageID" cssClass="text-danger" />
						    </div>
					    </div>
					  </div>
    
					   <div class="row">
					    <div class="col-md-8">
					      <label >المجموعه</label>
					      <div class="form-group">
					        <form:select class="form-control" id="selectgroup" path="groupID">
						       <option value="-1" label="-Select-"/>
						    </form:select>
					        <form:errors path="groupID" cssClass="text-danger" />
					      </div>
					    </div>
					  </div>
					  
					 
					  					 
		             <button style="float: right;" type="submit" onclick="return check()" class="btn btn-primary" >حفظ</button>
			     </form:form>	
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <jsp:directive.include file = "templete/footer.html" />
  <!-- Argon Scripts -->
  <!-- Core -->
  <jsp:directive.include file = "templete/js.html" />
  
  <script type="text/javascript">
  
  $( document ).ready(function() {
	 
	   //handle group display when edit student
	    stageid=${student.stageID.stageID};
	    if(stageid=='')
	    {
	    	return;
	    }
	    $.ajax({
	        type: 'GET',
	        url: "/student/groupbystage/" + stageid,
	        success: function(data){
	            var slctgrp=$('#selectgroup'), option="";
	            slctgrp.empty();
	            if(data.length==0)
	            {
	            	option = option + "<option value='"+null+ "'>"+"-select-"+ "</option>";
	            	slctgrp.append(option);
	            }
	            else
	            {
		            for(var i=0; i<data.length; i++){
		                option = option + "<option value='"+data[i].groupID + "'>"+data[i].groupName + "</option>";
		            }
		            slctgrp.append(option);
		            $("#selectgroup").val(${student.groupID.groupID});
	            }
	        },
	        error:function(){
	            //alert("error");
	        }

	    });
	});
  
  
  </script>
  
  <script type="text/javascript">
  
  $("#slctstageid").change(function(){
	    var stageid = $(this).val();
	    $.ajax({
	        type: 'GET',
	        url: "/student/groupbystage/" + stageid,
	        success: function(data){
	            var slctgrp=$('#selectgroup'), option="";
	            slctgrp.empty();
	            if(data.length==0)
	            {
	            	option = option + "<option value='"+null+ "'>"+"-select-"+ "</option>";
	            	slctgrp.append(option);
	            }
	            else
	            {
		            for(var i=0; i<data.length; i++){
		                option = option + "<option value='"+data[i].groupID + "'>"+data[i].groupName + "</option>";
		            }
		            slctgrp.append(option);
	            }
	        },
	        error:function(){
	            //alert("error");
	        }

	    });
	});

  
  </script>
  
  <script type="text/javascript">
  
  var check = function() {
	  if (document.getElementById('password').value ==
	    document.getElementById('confirm_password').value) {
	    document.getElementById('message').style.color = 'green';
	    document.getElementById('message').innerHTML = 'كلمه السر مطابقه';
	    return true;
	  } else {
	    document.getElementById('message').style.color = 'red';
	    document.getElementById('message').innerHTML = 'كلمه السر غير مطابقه';
	    return false;
	  }
	}
  
  
  
  </script>
</body>

</html>