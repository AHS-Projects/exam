<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>تصحيح النموذج</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  <style type="text/css">
    .error{
      color: red;
    }
  </style>
</head>

<body>
 
 <!-- Sidenav -->
	  <jsp:directive.include file = "templete/rightbar.html" />
	  <!-- Main content -->
	  <div class="main-content">
	  <!-- Top navbar -->
	  <jsp:directive.include file = "templete/topbar.jsp" />
	  
	  <!-- Page content -->
	  
	   <div class="container-fluid mt--7">
	    <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">تصحيح النموذج</h3>
            </div>
         <form:form modelAttribute="model" id="modelFormID"
		action="${pageContext.request.contextPath}/student/model/correct/save/${groupserID}"
		method="post">
		<form:hidden path="modelID" />
		<form:hidden path="modelName" />
		<form:hidden path="modelType" />
		<form:hidden path="isPublished" />
		<form:hidden path="createdDate" />
		<form:hidden path="updatedDate" />
		<form:hidden path="createdByUserID.teacherID" />
		<form:hidden path="updatedByUserID.teacherID" />
		<form:hidden path="stageID.stageID" />
		<form:hidden path="modelTime" />
		<form:hidden path="modelMark" />
		<form:hidden path="modelDesc" />
<!-- Display the countdown timer in an element -->
<p id="demo" style="text-align: left"></p>
		<div class="card-header border-0">
			<center>
				<h3 class="mb-0">${model.modelName}</h3>
			</center>
		</div>
		<c:forEach items="${model.tblsectionList}" varStatus="secLst"
			var="section">
			<div class="row">
				<div class="col-md-12">
					<div class="card shadow">
						<section style="padding-right:2%">
							<form:hidden path="tblsectionList[${secLst.index}].sectionID" />
							<form:hidden path="tblsectionList[${secLst.index}].mark" />
							    
							<form:label path="tblsectionList[${secLst.index}].sectionName">${section.sectionName}</form:label>
							<br />

							<c:forEach items="${section.tblquestionList}" varStatus="quesLst"
								var="question">
								<form:hidden path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].questionID" />
								<form:hidden path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].questionType" />
								<form:hidden path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].mark" />
								<form:hidden path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].anwser" />
								
								<div> <span>-</span> ${question.question} </div><br>
					
                                <c:forEach items="${question.tblstudentanswerList}" varStatus="studAnsLst" var="studAns">
                                  <c:if test="${not empty studAns.studentAnswerID}">
                                    <form:hidden path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].studentAnswerID" />
	                                <form:hidden path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].modelID.modelID" />
	                                <form:hidden path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].questionID.questionID" />
	                                <form:hidden path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].sectionID.sectionID" />
	                                <form:hidden path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].studentID.studentID" />
	                                <form:hidden path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].createdDate" /> 
									<c:choose>
	
										<c:when test="${(question.questionType == 'explain')}">
										 
  											<span>  اجابه الطالب </span>
  											<form:textarea class="form-control"  readonly="true"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].answer"
												id="${question.questionID}"  />
											
										<span>  الاجابه النموذجيه	</span>
											<form:textarea class="form-control"  readonly="true"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].anwser"
												  />
												
										   <span>	درجه السؤال </span>
											<form:input  class="form-control"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].mark"  readonly="true"
												 />		
												 	  
											 <span>درجه الطالب</span>
										    <form:input class="form-control"  required="true" number="true"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].mark"
												  />	
												
										</c:when>
										<c:when test="${(question.questionType == 'meaning')}">
										 
										
										  <span>  اجابه الطالب </span>
											<form:textarea class="form-control"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].answer"
												id="${question.questionID}" readonly="true" />
											
											<span>الاجابه النموذجيه</span>
											<form:textarea class="form-control"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].anwser"  readonly="true"
												 />
												
										   <span>  درجه السؤال</span>
											<form:input  class="form-control" 
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].mark"  readonly="true"
												 />
													 
										    <span>درجه الطالب</span> 
										    <form:input  class="form-control"  required="true" number="true"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].mark"
												  />
																 
										</c:when>
	
										<c:when test="${(question.questionType == 'rightorwrong')}">
											<span> اجابه الطالب</span> 
											 
											<form:input   class="form-control"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].answer"
												id="${question.questionID}" readonly="true" />
											<span>الاجابه النموذجيه</span> 
											
											<form:input  class="form-control"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].trueFalseAnswer"  readonly="true"
												 />
												<span>  درجه السؤال</span>
											<form:input  class="form-control"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].mark"  readonly="true"
												 />			 
										      <span>درجه الطالب</span>    
										    <form:input class="form-control"   required="true" number="true"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].mark"
												  />
										</c:when>
	
										<c:when test="${(question.questionType == 'choice') }">
	
	                                      <span> اجابه الطالب</span> 							
											<form:input  class="form-control"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].answer"
												id="${question.questionID}" readonly="true" />
											<span>الاجابه النموذجيه</span>
											<c:forEach items="${question.tblchoiceList}"
												varStatus="choiceLst" var="choice">
												
												<c:if test="${choice.ischoosed eq 1 }" >
												   
											      <form:input class="form-control" path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblchoiceList[${choiceLst.index}].choice"  readonly="true" />
												</c:if>
											</c:forEach>
										     <span>  درجه السؤال</span>
											<form:input class="form-control"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].mark"  readonly="true"
												 />	
											 
										      <span>درجه الطالب</span> 
										    <form:input class="form-control"  required="true" number="true"
												path="tblsectionList[${secLst.index}].tblquestionList[${quesLst.index}].tblstudentanswerList[${studAnsLst.index}].mark"
												  />
											
										</c:when>
									</c:choose>
								  </c:if>	
								</c:forEach>	

								<br />
								<br />
							</c:forEach>
						</section>

					</div>
				</div>
			</div>
			<br />
			<br />
		</c:forEach>
		<center>
			<button type="submit" class="btn btn-primary">أنهيت التصحيح</button>
			<!-- make it disabled when model is finished (check finishedDate !=null in tblgroupSeries OR updatedDate != null  in tblmodel) -->
		</center>

	</form:form>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <jsp:directive.include file = "templete/footer.html" />
  <!-- Argon Scripts -->
  <!-- Core -->
  <jsp:directive.include file = "templete/js.html" />
  <script type="text/javascript">
	  jQuery.extend(jQuery.validator.messages, {
		    required: "الرجاء ادخال هذا الحقل",
		    remote: "Please fix this field.",
		    email: "Please enter a valid email address.",
		    url: "Please enter a valid URL.",
		    date: "Please enter a valid date.",
		    dateISO: "Please enter a valid date (ISO).",
		    number: "الرجاء ادخال رقم صحيح",
		    digits: "Please enter only digits.",
		    creditcard: "Please enter a valid credit card number.",
		    equalTo: "Please enter the same value again.",
		    accept: "Please enter a value with a valid extension.",
		    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		    minlength: jQuery.validator.format("Please enter at least {0} characters."),
		    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
		});
	  
	    $("#modelFormID").validate();
  </script>
</body>

</html>