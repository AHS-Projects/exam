<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>الطلاب</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
    
          <c:if test="${not empty saveStudent}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${saveStudent}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			 <c:if test="${not empty deleteStudent}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${deleteStudent}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			  <!-- Modal Approval -->
			 <div class="modal fade" id="deletestudentid"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="approveLabel"> مسح الطالب</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      
				      <div class="modal-body">
				        هل انت متاكد؟
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">لا</button>
				        <a role="button" id="deleteLink" class="btn btn-primary" href="#">نعم</a>
				      </div>
				    </div>
				  </div>
			 </div>   
			 
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">الطلاب</h3>
            </div>
           
            <div class="table-responsive">
     
              <table id="tblstudid" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">اسم الطالب</th>
                    <th scope="col">البريد الالكترونى</th>
                    <th scope="col">رقم تليفون الطالب</th>
                    <th scope="col">رقم ولى الامر الطالب</th>
                    <th scope="col">المرحله</th>
                    <th scope="col">المجموعه</th>
                    <th scope="col">حاله الطالب</th>
                    <th scope="col">عرض نماذج الطالب</th>
                    <th scope="col">تعديل</th>
                    <th scope="col">تغيير كلمه السر</th>
                    <th scope="col">مسح</th>
                  </tr>
                </thead>
                <tbody>
              
	                  <tr>
	                   
	                    <td>
	                      studentName
	                    </td>
	                    <td>
	                      studentEmail
	                    </td>
	                    <td>
	                      studentPhone
	                    </td>
	                    <td>
	                      studentDadPhone
	                    </td>
	                     <td>
	                      stageName
	                    </td>
	                    <td>
	                      groupName
	                    </td>
	                    <td>
	                      block
	                    </td>
	                    <td>
	                      display
	                    </td>
	                    <td>
	                      edit
	                    </td>
	                    <td>
	                      editPass
	                    </td>
	                    <td>
	                     delete
	                    </td>
	                   
	                  </tr>
                 
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
      <spring:url value="/student/add" var="addurl" />
	  <a class="btn btn-primary" style="float: right;" href="${addurl}" role="button" >اضافه طالب</a>
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
 
  <script type="text/javascript">
	  $(document).ready( function () {
			 var table = $('#tblstudid').DataTable({
				    "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			        "iDisplayLength": 5, 
					"sAjaxSource": "/student/rest/list",
					"sAjaxDataProp": "",
					"order": [[ 0, "asc" ]],
					"aoColumns": [
						
				      { "mData": "studentName" },
				      { "mData": "studentEmail" },
				      { "mData": "studentPhone" },
				      { "mData": "studentDadPhone" },
				      { "mData": "stageName" },
				      { "mData": "groupName" },
				      { "mData": "block", render: blockStudent },
				      { "mData": "display", render: displayStudentModel },
				      { "mData": "edit", render: edit },
				      { "mData": "editPass", render: editPass },
				      { "mData": "delete", render: deleteStudent }
					]
			 })
		});
	  
	  function blockStudent(data, type, row, meta) {
		  
		   return '<a class="btn btn-primary" href="/student/block/'+row.studentID+'" role="button" >'+row.blocked+'</a>';
	       
	    }
	  function edit(data, type, row, meta) {
		  
		   return '<a class="btn btn-primary" href="/student/update/'+row.studentID+'" role="button" >تعديل</a>';
	       
	    }
	  function editPass(data, type, row, meta) {
		  
		   return '<a class="btn btn-primary" href="/student/update/password/'+row.studentID+'" role="button" >تغيير</a>';
	       
	    }
	  function displayStudentModel(data, type, row, meta) {
		  
		   return '<a class="btn btn-primary" href="/student/exam/'+row.studentID+'" role="button" >عرض</a>';
	       
	    }
	  function deleteStudent(data, type, row, meta) {

		   return '<a class="btn btn-primary" href="#" role="button" data-toggle="modal" onclick="deletestudent('+row.studentID+');" >مسح</a>';
	       
	    }
	  
	 
		function deletestudent(studentID)
		{
			 var deleteUrl="/student/delete/"+studentID;
			 $("#deleteLink").attr("href", deleteUrl);
			 $("#deletestudentid").modal();
		}
	    
  </script>
 
  
</body>

</html>