<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>المُدَرِّس</title>
    <!-- css -->
    <jsp:directive.include file="templete/style.html"/>
</head>

<body class="bg-default">
<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
        <div class="container px-4">
            <!--   <a class="navbar-brand" href="../index.html">
          <img src="${pageContext.request.contextPath}/resources/img/brand/white.png" />
        </a>-->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-main"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-collapse-main">
                <!-- Collapse header -->
                <div class="navbar-collapse-header d-md-none">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <!--  <a href="../index.html">
                  <img src="${pageContext.request.contextPath}/resources/img/brand/blue.png">
                </a>-->
                        </div>
                        <div class="col-6 collapse-close">
                            <button type="button" class="navbar-toggler" data-toggle="collapse"
                                    data-target="#navbar-collapse-main" aria-controls="sidenav-main"
                                    aria-expanded="false" aria-label="Toggle sidenav">
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- Navbar items -->
                <ul class="navbar-nav ml-auto">
                    <!--  <li class="nav-item">
                       <a class="nav-link nav-link-icon" href="../examples/register.html">
                         <i class="ni ni-circle-08"></i>
                         <span class="nav-link-inner--text">Register</span>
                       </a>
                     </li>
                     <li class="nav-item">
                       <a class="nav-link nav-link-icon" href="../index.html">
                         <i class="ni ni-planet"></i>
                         <span class="nav-link-inner--text">Dashboard</span>
                       </a>
                     </li>

                     <li class="nav-item">
                       <a class="nav-link nav-link-icon" href="../examples/login.html">
                         <i class="ni ni-key-25"></i>
                         <span class="nav-link-inner--text">Login</span>
                       </a>
                     </li>
                     <li class="nav-item">
                       <a class="nav-link nav-link-icon" href="../examples/profile.html">
                         <i class="ni ni-single-02"></i>
                         <span class="nav-link-inner--text">Profile</span>
                       </a>
                     </li>-->
                </ul>
            </div>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8">
        <div class="container">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-6">
                        <h1 class="text-white">مرحبا</h1>
                        <!--<p class="text-lead text-light">Use these awesome forms to login or create new account in your project for free.</p>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                 xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-7">
                <div class="card bg-secondary shadow border-0">
                    <div class="card-header bg-transparent pb-5">
                        <!--  <div class="text-muted text-center mt-2 mb-3"><small>Sign in with</small></div>
              <div class="btn-wrapper text-center">
              
                <a href="#" class="btn btn-neutral btn-icon">
                  <span class="btn-inner--icon"><img src="${pageContext.request.contextPath}/resources/img/icons/common/github.svg"></span>
                  <span class="btn-inner--text">Github</span>
                </a>
                <a href="#" class="btn btn-neutral btn-icon">
                  <span class="btn-inner--icon"><img src="${pageContext.request.contextPath}/resources/img/icons/common/google.svg"></span>
                  <span class="btn-inner--text">Google</span>
                </a>
                
              </div>
            </div>-->
                        <div class="card-body px-lg-5 py-lg-5">
                            <!--<div class="alert alert-success fade show" role="alert">
                                <h3>الرجاء عدم الدخول نهائيا حتى تختفى هذه الرساله لوجود اعمال الصيانه!</h3>

                            </div>-->
                            <div class="text-center text-muted mb-4">
                                <small>ادخل عن طريق اسم المستخدم </small>
                            </div>
                            <form method="POST" action="${contextPath}/login">
                                <div class="form-group mb-3">
                                    <c:if test="${param.error == 4}">
                                        <p class="text-danger">اسم المستخدم او كلمه السر غير صحيحه</p>
                                    </c:if>
                                    <c:if test="${param.logout == 'true'}">
                                        <p class="text-success">تم الخروج بنجاح</p>
                                    </c:if>
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>

                                        <input name="username" type="text" class="form-control"
                                               placeholder="اسم المستخدم"
                                               autofocus="true"/>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>

                                        <input name="password" type="password" class="form-control"
                                               placeholder="كلمه السر"/>

                                    </div>
                                </div>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                <!--  <div class="custom-control custom-control-alternative custom-checkbox">
                                  <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
                                  <label class="custom-control-label" for=" customCheckLogin">
                                    <span class="text-muted">افتكرنى</span>
                                  </label>
                                </div>-->
                                <div class="text-center">
                                    <input type="submit" name="submit" value="دخول" class="btn btn-primary my-4"/>

                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- <div class="row mt-3">
                          <div class="col-6">
                            <a href="#" class="text-light"><small>Forgot password?</small></a>
                          </div>
                          <div class="col-6 text-right">
                            <a href="#" class="text-light"><small>Create new account</small></a>
                          </div>
                        </div>-->
                </div>
            </div>
        </div>
    </div>

    <!-- js -->
    <jsp:directive.include file="templete/js.html"/>
</body>

</html>