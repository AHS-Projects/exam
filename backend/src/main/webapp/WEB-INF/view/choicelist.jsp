<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>اختيارات السؤال</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
    
          <c:if test="${not empty saveChoice}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${saveChoice}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			 <c:if test="${not empty deleteChoice}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${deleteChoice}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			  <!-- Modal Approval -->
			 <div class="modal fade" id="deletechoice"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="approveLabel"> مسح الاختيار </h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      
				      <div class="modal-body">
				        هل انت متاكد؟
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">لا</button>
				        <a role="button" id="deleteLink" class="btn btn-primary" href="#">نعم</a>
				      </div>
				    </div>
				  </div>
			 </div>   
			 
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">اختيارات السؤال</h3>
            </div>
           
            <div class="table-responsive">
     
              <table id="tblmodelid" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">الاختيار</th>
                    <th scope="col">هل هو الاجابه؟</th>
                    <th scope="col">تعديل</th>
                    <th scope="col">مسح</th>
                  </tr>
                </thead>
                <tbody>
              
                     
                    
                      <c:if test="${empty choices }">
                        <tr>
                        <td>
                     	   لا يوجد اختيارات  	  
					     <td>       
					    </tr>                 
                      </c:if>
                     
                     
	                 <c:forEach items="${choices}" var="choice" >
	                  <tr>
	                   
	                    <td>
	                      ${choice.choice}
	                    </td>
	                    <td>
	                      <c:if test="${choice.ischoosed==0}">
	                        لا
	                      </c:if>
	                      <c:if test="${choice.ischoosed==1}">
	                        نعم
	                      </c:if>
	                    </td>
	                    <td>
	                      <spring:url value="/choice/update/${questID}/${choice.choiceID}" var="updateurl" />
	                      <a class="btn btn-primary" href="${updateurl}" role="button" >تعديل</a>
	                    </td>
	                    <td>
	                      <a class="btn btn-primary" href="#" role="button"  onclick="deleteChoice(${choice.choiceID},${questID});" >مسح</a>
	                    </td>
	                   
	                  </tr>
                 </c:forEach>
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
      <spring:url value="/choice/add/${questID}" var="addurl" />
	  <a class="btn btn-primary" style="float: right;" href="${addurl}" role="button" >اضافه اختيار</a>
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
 <script type="text/javascript">
 function deleteChoice(choiceID,questionID)
 {
	 var deleteUrl="/choice/delete/"+questionID+"/"+choiceID;
	 $("#deleteLink").attr("href", deleteUrl);
	 $("#deletechoice").modal();
 }
 </script>
  
  
</body>

</html>