<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>اضافه او تعديل جزء</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
</head>

<body>
 
 <!-- Sidenav -->
	  <jsp:directive.include file = "templete/rightbar.html" />
	  <!-- Main content -->
	  <div class="main-content">
	  <!-- Top navbar -->
	  <jsp:directive.include file = "templete/topbar.jsp" />
	  
	  <!-- Page content -->
	  
	   <div class="container-fluid mt--7">
	    <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">اضافه او تعديل جزء</h3>
            </div>
         <form:form  modelAttribute="section" action="/section/save/${modID}" method="post" >
				      <form:hidden path="sectionID" />
				      <form:hidden path="modelID.modelID" />
				      <form:hidden path="createdDate" />
				      <form:hidden path="updatedDate" />
				      <form:hidden path="createdByUserID.teacherID" />
				      <form:hidden path="updatedByUserID.teacherID" />
					  <div class="row">
					    <div class="col-md-8">
					      <label >اسم الجزء</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="sectionName"  placeholder="اسم الجزء" />
         				    <form:errors path="sectionName"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >درجه الجزء</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="mark"  placeholder="درجه الجزء" />
					        <form:errors path="mark"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  					 
		             <button style="float: right;" type="submit" class="btn btn-primary" >حفظ</button>
			     </form:form>	
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <jsp:directive.include file = "templete/footer.html" />
  <!-- Argon Scripts -->
  <!-- Core -->
  <jsp:directive.include file = "templete/js.html" />
</body>

</html>