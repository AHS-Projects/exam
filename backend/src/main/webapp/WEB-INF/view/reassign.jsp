<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>الرئيسيه</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
     <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
       
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="${pageContext.request.contextPath}/resources/img/theme/team-4-800x800.jpg">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">خصائص</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">مرحبا</h6>
              </div>
      
              <div class="dropdown-divider"></div>
              <form action="${contextPath}/logout" method="POST">
	            <input type="submit" class="dropdown-item" value="خروج"/>
	          </form>
              <!--  <a href="#!" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>-->
            </div>
          </li>
        </ul>
      </div>
    </nav>
  	  <!-- Header -->
    <div class="header bg-gradient-primary pb-7 pt-5 pt-md-6">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
           <nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				   <c:forEach var="entry" items="${breadcrumbs}"   >
				    
				    <c:if test="${entry.value eq '#' }">
				        <li class="breadcrumb-item active" aria-current="page">${entry.key}</li>
				    </c:if>
				    <c:if test="${entry.value ne '#'}">
				    	<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}${entry.value}">${entry.key}</a></li>
				    </c:if>
				    
				   </c:forEach>
				  </ol>
	         </nav>
		
		 
	          <c:if test="${not empty reassignmsg}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${reassignmsg}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			 
        </div>
      </div>
 
    </div>
     <br>
    <!-- Page content -->
    <!--  <div class="container-fluid mt--7">
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">الرئيسيه</h3>
            </div>
            
            
             
            </div>

          </div>
        </div>
      </div>-->
      <a class="btn btn-primary" href="/reassign/do/" role="button" >execute</a>
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
  
</body>

</html>