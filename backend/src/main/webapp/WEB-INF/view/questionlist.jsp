<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>اسئله الجزء</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
    
          <c:if test="${not empty saveQues}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${saveQues}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			 <c:if test="${not empty deleteQues}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${deleteQues}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			  <!-- Modal Approval -->
			 <div class="modal fade" id="deleteques"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="approveLabel"> مسح السؤال</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      
				      <div class="modal-body">
				        هل انت متاكد؟
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">لا</button>
				        <a role="button" id="deleteLink" class="btn btn-primary" href="#">نعم</a>
				      </div>
				    </div>
				  </div>
			 </div>   
			 
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0"> اسئله ${secName} </h3>
            </div>
           
            <div class="table-responsive">
     
              <table id="tblquesid" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">المسلسل</th>
                    <th scope="col">السؤال</th>
                    <th scope="col">الاجابه</th>
                    <th scope="col">درجه السؤال</th>
                    <th scope="col">نوع السؤال</th>
                    <th scope="col">اختيارات السؤال</th>
                    <th scope="col">تم بواسطه</th>
                    <th scope="col">تعديل</th>
                    <th scope="col">مسح</th>
                  </tr>
                </thead>
                <tbody>
	                  <tr>
	                   
	                   <td>
	                      questionID
	                    </td>
	                    <td>
	                      question
	                    </td>
	                    <td>
	                      anwser
	                    </td>
	                    <td>
	                      mark
	                    </td>
	                     <td>
	                      questionType
	                    </td>
	                    <td>
	                      questionChoice
	                    </td>
	                    <td>
	                      teacherName
	                    </td>
	              
	                    <td>
	                      edit
	                    </td>
	                    <td>
	                      delete
	                    </td>
	                   
	                  </tr>
                 
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
      <spring:url value="/question/add/${secID}" var="addurl" />
	  <a class="btn btn-primary" style="float: right;" href="${addurl}" role="button" >اضافه سؤال</a>
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
 <script type="text/javascript">
	  $(document).ready( function () {
			 var table = $('#tblquesid').DataTable({
				    "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			        "iDisplayLength": 5, 
					"sAjaxSource": "/question/rest/list/"+${secID},
					"sAjaxDataProp": "",
					"order": [[ 0, "asc" ]],
					"aoColumns": [
				      
					  { "mData": "questionID" },	
				      { "mData": "question" },
				      { "mData": "answer",render: getAnswer },
				      { "mData": "mark" },
				      { "mData": "questionType",render: getQuestionType },
				      { "mData": "questionChoice", render: getQuestionChoice },
				      { "mData": "createdByTeacher" },
				      { "mData": "edit", render: edit },
				      { "mData": "delete", render: deletesec }
					]
			 })
		});
	  
	
	  function edit(data, type, row, meta) {
		   return '<a class="btn btn-primary" href="/question/update/'+row.questionID+'/'+row.sectionID+'" role="button" >تعديل</a>';
	       
	    }
	  function getQuestionChoice(data, type, row, meta) {
		   if(row.questionType=='choice')
		       return '<a class="btn btn-primary" href="/choice/list/'+row.questionID+'" role="button" >عرض</a>' ;
		   else
			   return 'لا يوجد اختيارات للسؤال'; 	   
	       
	    }
	  function deletesec(data, type, row, meta) {
		   var deleteUrl=
		   $("#deleteLink").attr("href", deleteUrl);
		   return '<a class="btn btn-primary" href="#" role="button" data-toggle="modal" onclick="deleteQues('+row.questionID+','+row.sectionID+');"  >مسح</a>';
	       
	    }
	  
	  function deleteQues(questionID,sectionID)
		{
			 var deleteUrl="/question/delete/"+questionID+"/"+sectionID;
			 $("#deleteLink").attr("href", deleteUrl);
			 $("#deleteques").modal();
		}
	  
	  function getQuestionType(data, type, row, meta) {
		  
		   if(row.questionType=='choice')
	         return 'اختيارى';
		   else if(row.questionType=='meaning')
			   return 'مرادفات'; 
		   else if(row.questionType=='rightorwrong')
			   return 'صح/خطأ';
		   else if(row.questionType=='part')
			   return 'قطعه';
		   else if(row.questionType=='explain')
			   return 'شرح';
	    }
	  function getAnswer(data, type, row, meta) {
		  
		   if(row.questionType=='rightorwrong')
	         return row.trueFalseAnswer;
		   else 
			   return row.answer; 
		   
	    }
	    
  </script>
 

  
</body>

</html>