<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>الطلاب</title>
    <!-- css -->
    <jsp:directive.include file="templete/style.html"/>

</head>

<body>
<!-- Sidenav -->
<jsp:directive.include file="templete/rightbar.html"/>
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <jsp:directive.include file="templete/topbar.jsp"/>

    <!-- Page content -->
    <div class="container-fluid mt--7">

        <c:if test="${not empty assignStudent}">
            <div class="alert alert-success fade show" role="alert">
                <strong>ملحوظه!</strong> ${assignStudent}.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </c:if>


        <!-- Table -->
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">تعيين الطلاب المجموعه ${groupName} للنموذج ${modelName} ${stageName} </h3>
                    </div>

                    <div class="table-responsive">
                        <form method="post" action="/student/assign/model/${groupID}/${stageID}/${modelID}">
                            <input type="submit" class="btn btn-primary" value="اضافه الطلبه المحدده للنموذج">
                            <br/> <br/>
                            <table id="tblstudid" class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>

                                    <th scope="col"><input type="checkbox" id="checkBoxAll"/></th>
                                    <th scope="col">اسم الطالب</th>
                                    <th scope="col">البريد الالكترونى</th>
                                    <th scope="col">رقم تليفون الطالب</th>
                                    <th scope="col">رقم ولى الامر الطالب</th>
                                    <th scope="col">المرحله</th>
                                    <th scope="col">المجموعه</th>


                                </tr>
                                </thead>
                                <tbody>

                                <tr>

                                    <td>
                                        checkbox
                                    </td>
                                    <td>
                                        studentName
                                    </td>
                                    <td>
                                        studentEmail
                                    </td>
                                    <td>
                                        studentPhone
                                    </td>
                                    <td>
                                        studentDadPhone
                                    </td>
                                    <td>
                                        stage
                                    </td>
                                    <td>
                                        group
                                    </td>


                                </tr>

                                </tbody>
                            </table>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <!-- Footer -->
        <jsp:directive.include file="templete/footer.html"/>
    </div>
</div>
<!-- Argon Scripts -->
<!-- Core -->
<!-- js -->
<jsp:directive.include file="templete/js.html"/>

<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#tblstudid').DataTable({

            "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
            "iDisplayLength": 5,
            "sAjaxSource": "/student/rest/list/group/"+${groupID}+"/"+${stageID}+"/" +${modelID},
            "sAjaxDataProp": "",
            "order": [[0, "asc"]],
            "aoColumns": [

                {"mData": "checkbox", render: checkBoxAssign},
                {"mData": "studentName"},
                {"mData": "studentEmail"},
                {"mData": "studentPhone"},
                {"mData": "studentDadPhone"},
                {"mData": "stageName"},
                {"mData": "groupName"}
            ]
        })

    });

    //,{ "mData": "isAssigned", render: isAssignedStudentToModel }
    function isAssignedStudentToModel(data, type, row, meta) {
        if (row.isAssigned == 1)
            return 'نعم';
        else
            return 'لا';

    }

    function checkBoxAssign(data, type, row, meta) {

        return '<input type="checkbox" class="chkCheckBoxId" value="' + row.studentID + '" name="studentName" />';

    }

    $(document).ready(function () {

        $('#checkBoxAll').click(function () {

            if ($(this).is(":checked"))
                $('.chkCheckBoxId').prop('checked', true);
            else
                $('.chkCheckBoxId').prop('checked', false);
        });

    });


</script>


</body>

</html>