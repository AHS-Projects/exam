<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>ادوار المستخدم</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">ادوار المستخدم</h3>
            </div>
            <c:if test="${not empty delete}">
			    <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${delete}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			</c:if>	
			 <c:if test="${not empty addrole}">
			    <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${addrole}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			</c:if>	
			 <!-- Modal Approval -->
			 <div class="modal fade" id="deleterole"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="approveLabel"> مسح الدور من المدرس</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        هل انت متاكد؟
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">لا</button>
				        <a role="button" id="deleteLink" class="btn btn-primary" href="#">نعم</a>
				      </div>
				    </div>
				  </div>
			 </div>    
			
              <!-- Modal Approval -->
			 <div class="modal fade" id="rolemodal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="approveLabel"> اضافه دور للمدرس</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				       <c:if test="${empty allRoles}">
				         <div class="alert alert-success" role="alert">
                            هذا المستخدم يمتلك كل الادوار			
					     </div>
				       </c:if>
				       <c:if test="${not empty allRoles}">
					       <form:form modelAttribute="userRole" method="post" action="/teacher/role/add/${userVarID}"  >
					              
					       		  <div class="input-group mb-3">
						             	<form:select class="form-control" path="roleID.roleID">
						             	    <!-- <form:option value="NONE" label="--- Select ---"/>-->
						             	    <c:forEach var="urole" items="${allRoles}">
	    										<form:option value="${urole.roleID.toString()}" label="${urole.role}"/>
	    								    </c:forEach>		
										</form:select>
						          </div>
						          <button style="float: right;" type="submit" class="btn btn-primary" >حفظ</button>
						    </form:form>   
					   </c:if>  	
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
				      </div>
				    </div>
				  </div>
			 </div>    
            <div class="table-responsive">
             <h4 class="text-danger" >${error}</h4>
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">اسم الدور</th>
                    <th scope="col">وصف الدور</th>
                    <th scope="col">مسح الدور</th>
                  </tr>
                </thead>
                <tbody>
                 <c:if test="${empty roles}">
                   لا يوجد ادوار لهذا المستخدم
                 </c:if>
                 <c:forEach items="${roles}" var="role" >
	                  <tr>
	                   
	                    <td>
	                      ${role.role}
	                    </td>
	                    <td>
	                      ${role.roleDesc}
	                    </td>
	          
	                    <td>
	                      <spring:url value="/teacher/role/delete/${role.roleID}/${userVarID}" var="deleteurl" />
	                      <a class="btn btn-primary" href="#" role="button"  onclick="deleteRole(${role.roleID},${userVarID});" >مسح</a>
	                    </td>
	                   
	                  </tr>
                 </c:forEach>
                 
                </tbody>
              </table>
             
            </div>

          </div>
        </div>
      </div>
     
	  <a class="btn btn-primary" style="float: right;" href="#" role="button"  data-toggle="modal" data-target="#rolemodal" >اضافه دور للمستخدم</a>
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
  <script type="text/javascript">
	 function deleteRole(roleID,teacherID)
	 {
		 var deleteUrl="/teacher/role/delete/"+roleID+"/"+teacherID;
		 $("#deleteLink").attr("href", deleteUrl);
		 $("#deleterole").modal();
	 }
 </script>
</body>

</html>