<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title> تغيير كلمه السر</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
</head>

<body>
 
 <!-- Sidenav -->
	  <jsp:directive.include file = "templete/rightbar.html" />
	  <!-- Main content -->
	  <div class="main-content">
	  <!-- Top navbar -->
	  <jsp:directive.include file = "templete/topbar.jsp" />
	  
	  <!-- Page content -->
	  
	   <div class="container-fluid mt--7">
	    <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">تغيير كلمه السر</h3>
            </div>
         <form:form  modelAttribute="student" action="/student/save" method="post" >
				      <form:hidden path="studentID" />
				      <form:hidden path="createdDate" />
				      <form:hidden path="updatedDate" />
				      <form:hidden path="createdByUserID.teacherID" />
				      <form:hidden path="updatedByUserID.teacherID" />
				      <form:hidden path="studentName" />
				      <form:hidden path="studentEmail" />
				      <form:hidden path="studentPhone" />
				      <form:hidden path="studentDadPhone" />
				      <form:hidden path="studentAge" />
				      <form:hidden path="blocked" />
				      <form:hidden path="stageID.stageID" />
				      <form:hidden path="groupID.groupID" />
					  
					
					  <div class="row">
					    <div class="col-md-8">
					      <label >كلمه السر الجديده</label>
					      <div class="form-group">
					        <form:password id="password"  onkeyup='check();' class="form-control" path="studentPassword"  placeholder="كلمه السر" />
         				    <form:errors path="studentPassword"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  
					   <div class="row">
					    <div class="col-md-8">
					      <label >تاكيد كلمه السر</label>
					      <div class="form-group">
				        	<form:password id="confirm_password"  onkeyup='check();' class="form-control" path="confirmationStudentPassword"  placeholder="تاكيد كلمه السر" />
         				    <form:errors path="confirmationStudentPassword"  class="text-danger"  />
         				    <span id='message'></span>
					      </div>
					    </div>
					  </div>
										 
					  					 
		             <button style="float: right;" type="submit" onclick="return check()" class="btn btn-primary" >حفظ</button>
			     </form:form>	
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <jsp:directive.include file = "templete/footer.html" />
  <!-- Argon Scripts -->
  <!-- Core -->
  <jsp:directive.include file = "templete/js.html" />
  

  
  <script type="text/javascript">
 
	  var check = function() {
		  if (document.getElementById('password').value ==
		    document.getElementById('confirm_password').value) {
		    document.getElementById('message').style.color = 'green';
		    document.getElementById('message').innerHTML = 'كلمه السر مطابقه';
		    return true;
		  } else {
		    document.getElementById('message').style.color = 'red';
		    document.getElementById('message').innerHTML = 'كلمه السر غير مطابقه';
		    return false;
		  }
		}
  
  </script>
</body>

</html>