<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>اجزاء النموذج</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
    
          <c:if test="${not empty saveSection}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${saveSection}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			 <c:if test="${not empty deleteSection}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${deleteSection}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			  <!-- Modal Approval -->
			 <div class="modal fade" id="deletesection"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="approveLabel"> مسح الجزء</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      
				      <div class="modal-body">
				        هل انت متاكد؟
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">لا</button>
				        <a role="button" id="deleteLink" class="btn btn-primary" href="#">نعم</a>
				      </div>
				    </div>
				  </div>
			 </div>   
			 
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0"> اجزاء النموذج ${modelName} </h3>
            </div>
           
            <div class="table-responsive">
     
              <table id="tblseclid" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">المسلسل</th>
                    <th scope="col">اسم الجزء</th>
                    <th scope="col">درجه الجزء</th>
                    <th scope="col">تم بواسطه</th>
                    <th scope="col">اسئله الجزء</th>
                    <th scope="col">تعديل</th>
                    <th scope="col">مسح</th>
                  </tr>
                </thead>
                <tbody>
                 
	                  <tr>
	                   
	                   <td>
	                      sectionID
	                    </td>
	                    <td>
	                      sectionName
	                    </td>
	                    <td>
	                      mark
	                    </td>
	                    <td>
	                      teacherName
	                    </td>
	                    <td>
	                      displayQues
	                    </td>
	                    <td>
	                     edit
	                    </td>
	                    <td>
	                      delete
	                    </td>
	                   
	                  </tr>
                 
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
      <spring:url value="/section/add/${modelID}" var="addurl" />
	  <a class="btn btn-primary" style="float: right;" href="${addurl}" role="button" >اضافه جزء</a>
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
 
 <script type="text/javascript">
	  $(document).ready( function () {
			 var table = $('#tblseclid').DataTable({
				    "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			        "iDisplayLength": 5, 
					"sAjaxSource": "/section/rest/list/"+${modelID},
					"sAjaxDataProp": "",
					"order": [[ 0, "asc" ]],
					"aoColumns": [
					
					  { "mData": "sectionID" },	
				      { "mData": "sectionName" },
				      { "mData": "mark" },
				      { "mData": "createdByTeacher" },
				      { "mData": "displayQues", render: showQues },
				      { "mData": "edit", render: edit },
				      { "mData": "delete", render: deletesec }
					]
			 })
		});
	  
	  function showQues(data, type, row, meta) {
		  
		   return '<a class="btn btn-primary" href="/question/list/'+row.sectionID+'" role="button" >عرض</a>';
	       
	    }
	  function edit(data, type, row, meta) {
		  
		   return '<a class="btn btn-primary" href="/section/update/'+row.sectionID+'/'+row.modelID+'" role="button" >تعديل</a>';
	       
	    }
	  function deletesec(data, type, row, meta) {

		   return '<a class="btn btn-primary" href="#" role="button" data-toggle="modal" onclick="deleteSection('+row.sectionID+','+row.modelID+');"  >مسح</a>';
	       
	    }
	  
	  function deleteSection(sectionID,modelID)
		{
			 var deleteUrl="/section/delete/"+sectionID+"/"+modelID;
			 $("#deleteLink").attr("href", deleteUrl);
			 $("#deletesection").modal();
		}
	    
  </script>
 
 
  
</body>

</html>