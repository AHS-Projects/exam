<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>اضافه او تعديل نموذج</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
</head>

<body>
 
 <!-- Sidenav -->
	  <jsp:directive.include file = "templete/rightbar.html" />
	  <!-- Main content -->
	  <div class="main-content">
	  <!-- Top navbar -->
	  <jsp:directive.include file = "templete/topbar.jsp" />
	  
	  <!-- Page content -->
	  
	   <div class="container-fluid mt--7">
	    <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">اضافه او تعديل نموذج</h3>
            </div>
         <form:form  modelAttribute="model" action="/model/save" method="post" >
				      <form:hidden path="modelID" />
				      <form:hidden path="createdDate" />
				      <form:hidden path="updatedDate" />
				      <form:hidden path="createdByUserID.teacherID" />
				      <form:hidden path="updatedByUserID.teacherID" />
				      
					  <div class="row">
					    <div class="col-md-8">
					      <label >اسم النموذج</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="modelName"  placeholder="اسم النموذج" />
         				    <form:errors path="modelName"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >نوع النموذج</label>
					      <div class="form-group">
					      <form:select class="form-control" path="modelType">
    						<form:options items="${modeltypes}" />
						  </form:select>
					       <form:errors path="modelType" cssClass="text-danger" />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >وصف النموذج</label>
					      <div class="form-group">
					        <form:input type="textarea" class="form-control" path="modelDesc"  placeholder="وصف النموذج" />
					        <form:errors path="modelDesc"  class="text-danger"  />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >درجه النموذج</label>
					      <div class="form-group">
					        <form:input type="text" class="form-control" path="modelMark"  placeholder="درجه النموذج" />
					        <form:errors path="modelMark" cssClass="text-danger" />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >المرحله</label>
					      <div class="form-group">
					        <form:select path="stageID" class="form-control" items="${allStages}" itemValue="stageID" itemLabel="stageName" />
					        <form:errors path="stageID" cssClass="text-danger" />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >وقت النموذج بالدقيقه</label>
					      <div class="form-group">
					         <form:select class="form-control" path="modelTime">
    							<form:options items="${modelTimes}" />
					        </form:select>
					       <form:errors path="modelTime" cssClass="text-danger" />
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-8">
					      <label >نشر النموذج</label>
					      <div class="form-group">
					         <form:select class="form-control" path="isPublished">
    							<form:options items="${modelPublished}" />
					        </form:select>
					       <form:errors path="isPublished" cssClass="text-danger" />
					      </div>
					    </div>
					  </div>
					  					 
		             <button style="float: right;" type="submit" class="btn btn-primary" >حفظ</button>
			     </form:form>	
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <jsp:directive.include file = "templete/footer.html" />
  <!-- Argon Scripts -->
  <!-- Core -->
  <jsp:directive.include file = "templete/js.html" />
</body>

</html>