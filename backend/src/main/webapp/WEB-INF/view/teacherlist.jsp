<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>اداره المُدَرِّسين</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
    
          <c:if test="${not empty saveTeacher}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${saveTeacher}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			 <c:if test="${not empty deleteTeacher}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${deleteTeacher}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			  <!-- Modal Approval -->
			 <div class="modal fade" id="deleteteacher"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="approveLabel"> مسح مدرس</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        هل انت متاكد؟
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">لا</button>
				        <a role="button" id="deleteLink" class="btn btn-primary" href="#">نعم</a>
				      </div>
				    </div>
				  </div>
			 </div>   
			 
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">اداره المُدَرِّسين</h3>
            </div>
            <spring:url value="/teacher/update" var="upurl" />
	        <a class="btn btn-primary" style="float: right;" href="${upurl}" role="button" >تعديل حسابك</a>  
            <div class="table-responsive">
             <h4 class="text-danger" >${error}</h4>
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">اسم المُدَرِّس</th>
                    <th scope="col">وظيفه المُدَرِّس</th>
                    <th scope="col">ادوار المُدَرِّس</th>
                    <th scope="col">تعديل</th>
                    <th scope="col">مسح</th>
                  </tr>
                </thead>
                <tbody>
                 <c:forEach items="${teachers}" var="teacher" >
	                  <tr>
	                   
	                    <td>
	                      ${teacher.teacherName}
	                    </td>
	                    <td>
	                      ${teacher.teacherType}
	                    </td>
	                    <td>
	                      <spring:url value="/teacher/role/${teacher.teacherID}" var="disrole" />
	                      <a class="btn btn-primary" href="${disrole}" role="button" >عرض</a>
	                    </td>
	                    <td>
	                      <spring:url value="/teacher/update/${teacher.teacherID}" var="updateurl" />
	                      <a class="btn btn-primary" href="${updateurl}" role="button" >تعديل</a>
	                    </td>
	                    <td>
	                      <spring:url value="/teacher/delete/${teacher.teacherID}" var="deleteurl" />
	                      <a class="btn btn-primary" href="#" role="button"  onclick="deleteTeacher(${teacher.teacherID});" >مسح</a>
	                    </td>
	                   
	                  </tr>
                 </c:forEach>
                 
                </tbody>
              </table>
             
            </div>

          </div>
        </div>
      </div>
      <spring:url value="/teacher/add" var="addurl" />
	  <a class="btn btn-primary" style="float: right;" href="${addurl}" role="button" >اضافه مُدَرِّس</a>
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
 <script type="text/javascript">
 function deleteTeacher(teacherID)
 {
	 var deleteUrl="/teacher/delete/"+teacherID;
	 $("#deleteLink").attr("href", deleteUrl);
	 $("#deleteteacher").modal();
 }
 </script>
  
</body>

</html>