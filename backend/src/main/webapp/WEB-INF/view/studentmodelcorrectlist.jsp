<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>  

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>النماذج المصححه</title>
  <!-- css -->
  <jsp:directive.include file = "templete/style.html" />
  
</head>

<body>
  <!-- Sidenav -->
   <jsp:directive.include file = "templete/rightbar.html" />
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
  <jsp:directive.include file = "templete/topbar.jsp" />
  
    <!-- Page content -->
    <div class="container-fluid mt--7">
    
          <c:if test="${not empty correctedModel}">
	             <div class="alert alert-success fade show" role="alert">
				  <strong>ملحوظه!</strong> ${correctedModel}.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				 </div>
			 </c:if>
			 
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0"> النماذج المصححه </h3>
            </div>
           
            <div class="table-responsive">
     
              <table id="tblseclid" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">اسم الطالب</th>
                    <th scope="col">تليفون الطالب</th>
                    <th scope="col"> تليفون ولى امر الطالب</th>
                    <th scope="col">المرحله</th>
                    <th scope="col">المجموعه</th>
                    <th scope="col">اسم النموذج</th>
                    <th scope="col">نوع النموذج</th>
                    <th scope="col">درجه النموذج</th>
                    <th scope="col">درجه الطالب</th>
                    <th scope="col">بدايه الامتحان</th>
                    <th scope="col">نهايه الامتحان</th>
                    <th scope="col">تعديل التصحيح</th>
                 
                  </tr>
                </thead>
                <tbody>
                 
	                  <tr>
	                   
	                    <td>
	                      studentName
	                    </td>
	                    <td>
	                      studentPhone
	                    </td>
	                    <td>
	                      studentDadPhone
	                    </td>
	                    <td>
	                      stageName
	                    </td>
	                    <td>
	                     groupName
	                    </td>
	                    <td>
	                      modelName
	                    </td>
	                     <td>
	                      modelType
	                    </td>
	                     <td>
	                      modelMark
	                    </td>
	                    <td>
	                      studMark
	                    </td>
	                     <td>
	                      startDate
	                    </td>
	                     <td>
	                      finishedDate
	                    </td>
	                    <td>
	                      edit
	                    </td>
	                   
	                   
	                   
	                  </tr>
                 
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
    
      <!-- Footer -->
       <jsp:directive.include file = "templete/footer.html" />
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <!-- js -->
 <jsp:directive.include file = "templete/js.html" />
 
 <script type="text/javascript">
	  $(document).ready( function () {
			 var table = $('#tblseclid').DataTable({
				    "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			        "iDisplayLength": 5, 
					"sAjaxSource": "/student/model/rest/corrected/list/"+${stageID},
					"sAjaxDataProp": "",
					"order": [[ 0, "asc" ]],
					"aoColumns": [
						
				      { "mData": "studentName" },
				      { "mData": "studentPhone" },
				      { "mData": "studentDadPhone" },
				      { "mData": "stageName" },
				      { "mData": "groupName" },
				      { "mData": "modelName" },
				      { "mData": "modelType",render: getModelType },
				      { "mData": "modelMark" },
				      { "mData": "studentMark"},
				      { "mData": "startDate" },
				      { "mData": "finishedDate" },
				      { "mData": "edit", render: correctModel}
				      
				      
					]
			 })
		});
	  
	  
	  function correctModel(data, type, row, meta) {
		  
		   return '<a class="btn btn-primary" href="/student/model/correct/'+row.groupSeriesID+'" role="button" >تعديل</a>';
	       
	    }
	  
	  function getModelType(data, type, row, meta) {
		  
		   if(row.modelType=='daily')
	         return 'يومى';
		   else if(row.modelType=='monthly')
			   return 'شهرى'; 
	    }
	    
  </script>
 
 
  
</body>

</html>