package com.exam.common.dto;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Model {
    private Long modelID;
    private String modelName;
    private String modelType;
    private Double studentMark;
    private Double modelMark;
    private String startDate;
    private String finishDate;
    private String modelDesc;
    private Integer isPublished;
    //private String stageName;
    private String createdDate;

    public Model() {

    }

    public Model(Long modelID, String modelName, String modelType, Double studentMark, Double modelMark, Date startDate, Date finishDate, String modelDesc, Date createdDate, Integer isPublished) {
        this.modelID = modelID;
        this.modelName = modelName;
        this.modelType = modelType;
        this.studentMark = studentMark;
        this.modelMark = modelMark;
        if (startDate != null) {
            this.startDate = startDate.toString();
        }
        if (finishDate != null) {
            this.finishDate = finishDate.toString();
        }
        this.modelDesc = modelDesc;
        this.isPublished = isPublished;
        // this.stageName = stageName;
        if (createdDate != null) {
            DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String inputText = createdDate.toString();
            Date date = null;
            try {
                date = inputFormat.parse(inputText);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String outputText = outputFormat.format(date);
            this.createdDate = outputText;

        }
    }

    public Integer getIsPublished() {
        return isPublished;
    }

    public void setIsPublished(Integer isPublished) {
        this.isPublished = isPublished;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }


    public Long getModelID() {
        return modelID;
    }

    public void setModelID(Long modelID) {
        this.modelID = modelID;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelDesc() {
        return modelDesc;
    }

    public void setModelDesc(String modelDesc) {
        this.modelDesc = modelDesc;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Double getStudentMark() {
        return studentMark;
    }

    public void setStudentMark(Double studentMark) {
        this.studentMark = studentMark;
    }

    public Double getModelMark() {
        return modelMark;
    }

    public void setModelMark(Double modelMark) {
        this.modelMark = modelMark;
    }


}
