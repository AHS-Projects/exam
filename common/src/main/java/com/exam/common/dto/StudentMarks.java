package com.exam.common.dto;

public class StudentMarks {

    private long studentID;
    private double studentMark;
    private double modelMark;
    private String startDate;
    private String finishDate;
    private String modelType;
    private String modelName;

    public StudentMarks() {
    }


    public StudentMarks(double studentMark, double modelMark, String startDate, String finishDate, String modelType) {
        this.studentMark = studentMark;
        this.modelMark = modelMark;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.modelType = modelType;
    }


    public long getStudentID() {
        return studentID;
    }

    public void setStudentID(long studentID) {
        this.studentID = studentID;
    }


    public double getStudentMark() {
        return studentMark;
    }


    public void setStudentMark(double studentMark) {
        this.studentMark = studentMark;
    }


    public double getModelMark() {
        return modelMark;
    }


    public void setModelMark(double modelMark) {
        this.modelMark = modelMark;
    }


    public String getModelType() {
        return modelType;
    }


    public void setModelType(String modelType) {
        this.modelType = modelType;
    }


    public String getStartDate() {
        return startDate;
    }


    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }


    public String getFinishDate() {
        return finishDate;
    }


    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }


    public String getModelName() {
        return modelName;
    }


    public void setModelName(String modelName) {
        this.modelName = modelName;
    }


}
