package com.exam.common.dto;


public class Student {
    private Long studentID;
    private String studentName;
    private String studentEmail;
    private String studentPhone;
    private String studentDadPhone;
    private String blocked;
    private String stageName;
    private String groupName;
    private Integer isAssigned;

    public Student() {

    }

    public Student(Long studentID, String studentName, String studentEmail, String studentPhone, String studentDadPhone, Integer blocked, String stageName, String groupName, Integer isAssigned) {
        this.studentID = studentID;
        this.studentName = studentName;
        this.studentEmail = studentEmail;
        this.studentPhone = studentPhone;
        this.studentDadPhone = studentDadPhone;
        this.blocked = blocked.toString();
        this.stageName = stageName;
        this.groupName = groupName;
        this.isAssigned = isAssigned;
    }

    public Long getStudentID() {
        return studentID;
    }

    public void setStudentID(Long studentID) {
        this.studentID = studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public void setStudentPhone(String studentPhone) {
        this.studentPhone = studentPhone;
    }

    public String getStudentDadPhone() {
        return studentDadPhone;
    }

    public void setStudentDadPhone(String studentDadPhone) {
        this.studentDadPhone = studentDadPhone;
    }

    public String getBlocked() {
        return blocked;
    }

    public void setBlocked(String blocked) {
        this.blocked = blocked;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getIsAssigned() {
        return isAssigned;
    }

    public void setIsAssigned(Integer isAssigned) {
        this.isAssigned = isAssigned;
    }


}
