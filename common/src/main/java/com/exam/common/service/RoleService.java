package com.exam.common.service;

import java.util.List;

import com.exam.common.model.Tblrole;


public interface RoleService 
{
	
	public List<Tblrole> findAllRoles();
	public void addRole(Tblrole r);
	public void updateRole(Tblrole r);
	public void deleteRole(long rID);
	public Tblrole getRole(long rID);

}
