package com.exam.common.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.exam.common.dao.StageDao;
import com.exam.common.model.Tblstage;

@Service
@Transactional
public class StageServiceImp implements StageService {

	@Autowired
	private StageDao stageDao;
	
	@Override
	public List<Tblstage> findAllStagesWithStudents() {
		// TODO Auto-generated method stub
		return stageDao.findAllStagesWithStudents();
	}
	@Override
	public List<Tblstage> findAllStages() {
		// TODO Auto-generated method stub
		return stageDao.findAllStages();
	}

	@Override
	public void addStage(Tblstage s) {
		// TODO Auto-generated method stub
		stageDao.addStage(s);
	}

	@Override
	public void updateStage(Tblstage s) {
		// TODO Auto-generated method stub
		stageDao.updateStage(s);
	}

	@Override
	public void deleteStage(long sID) {
		// TODO Auto-generated method stub
		stageDao.deleteStage(sID);
	}

	@Override
	public Tblstage getStage(long sID) {
		// TODO Auto-generated method stub
		return stageDao.getStage(sID);
	}

}
