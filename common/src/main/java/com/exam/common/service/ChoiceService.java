package com.exam.common.service;

import com.exam.common.model.Tblchoice;

import java.util.List;

public interface ChoiceService {
	

    public List<Tblchoice> findAllChoices();

    public List<Tblchoice> findAllChoicesByQuestionID(long questionID);

    public void addChoice(Tblchoice c);

    public void updateChoice(Tblchoice c);

    public void deleteChoice(long cID);

    public Tblchoice getChoice(long cID);

}
