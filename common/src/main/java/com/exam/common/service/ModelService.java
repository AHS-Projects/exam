package com.exam.common.service;

import com.exam.common.dto.Model;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblmodel;

import java.util.List;

public interface ModelService {

    public List<Tblmodel> findAllModels();

    public void addModel(Tblmodel m);

    public void updateModel(Tblmodel m);

    public void deleteModel(long mID);

    public Tblmodel getModelWithSections(long mID);

    public Tblmodel getModelByID(long mID);

    public List<Tblgroupseries> getstudentPrevModels(long studID);
    public List<Model> findPrevModelsByStudName(String studName);
    public List<Tblgroupseries> getstudentNextModels(long studID);

    public long countAllModelsByModelType(String modelType);

    public List<Model> findNextModelsByStudName(String studName);

    public Tblgroupseries getstudentNextModelsWithStuiDAndModelID(long studentID, long modelID);


}
