package com.exam.common.service;


import com.exam.common.dao.StudentDao;
import com.exam.common.dto.Model;
import com.exam.common.dto.Student;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblstudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class StudentServiceImp implements StudentService {

    @Autowired
    private StudentDao studentDao;

    @Override
    public List<Tblstudent> findAllStudents() {

        return studentDao.findAllStudents();
    }

    @Override
    public void addStudent(Tblstudent s) {

        studentDao.addStudent(s);
    }

    @Override
    public List<Student> findStudentsAndGroupSeriesByGrIDANDStIDAndModID(long groupID, long stageID, long modelID) {
        return studentDao.findStudentsAndGroupSeriesByGrIDANDStIDAndModID(groupID, stageID, modelID);
    }

    @Override
    public void updateStudent(Tblstudent s) {
        studentDao.updateStudent(s);
    }

    @Override
    public void deleteStudent(long sID) {
        studentDao.deleteStudent(sID);
    }

    @Override
    public Tblstudent getStudentWithGroupSerios(long sID) {
        return studentDao.getStudentWithGroupSerios(sID);
    }

    @Override
    public Tblstudent findStudentByName(String studentName) {
        return studentDao.findStudentByName(studentName);
    }

    @Override
    public List<Tblgroupseries> getStudentMarks(long studentID) {

        return studentDao.getStudentMarks(studentID);
    }

    @Override
    public Tblstudent getStudentWithCurrentSession(long sID) {
        // TODO Auto-generated method stub
        return studentDao.getStudentWithCurrentSession(sID);
    }

    @Override
    public List<Tblstudent> findStudenstByGroupID(long groupID) {
        // TODO Auto-generated method stub
        return studentDao.findStudenstByGroupID(groupID);
    }

    @Override
    public List<Tblstudent> findStudenstByGroupIDAndStageID(long groupID, long stageID) {
        // TODO Auto-generated method stub
        return studentDao.findStudenstByGroupIDAndStageID(groupID, stageID);
    }

    @Override
    public List<Tblstudent> findAllStudentsWithCurresession() {
        // TODO Auto-generated method stub
        return studentDao.findAllStudentsWithCurresession();
    }

    @Override
    public long countAllStudentBuStatsus(Integer blocked) {
        // TODO Auto-generated method stub
        return studentDao.countAllStudentBuStatsus(blocked);
    }

    @Override
    public List<Tblgroupseries> getAllStudentModels(long studentID) {

        return studentDao.getAllStudentModels(studentID);
    }

    @Override
    public List<Model> getStudentMarksByName(String studentName) {
        return studentDao.getStudentMarksByName(studentName);
    }
}
