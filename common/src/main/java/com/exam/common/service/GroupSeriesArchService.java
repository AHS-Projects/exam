package com.exam.common.service;

import com.exam.common.model.TblgroupseriesArchive;

import java.util.List;


public interface GroupSeriesArchService {

    public List<TblgroupseriesArchive> findAllGroupSeries();

    public void addGroupSeries(TblgroupseriesArchive g);

    public void updateGroupSeries(TblgroupseriesArchive g);

    public void deleteGroupSeries(long gID);

    public TblgroupseriesArchive getGroupSeries(long gID);


}
