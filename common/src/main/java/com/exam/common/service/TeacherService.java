package com.exam.common.service;

import com.exam.common.model.Tblteacher;

import java.util.List;

public interface TeacherService {
	

    public List<Tblteacher> findAllTeachers();

    public void addTeacher(Tblteacher t);

    public void updateTeacher(Tblteacher t);

    public void deleteTeacher(long tID);

    public Tblteacher getTeacher(long tID);

    public Tblteacher findTeacherByName(String teacherName);

}
