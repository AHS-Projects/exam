package com.exam.common.service;

import com.exam.common.dao.GroupSeriesArchDao;
import com.exam.common.model.TblgroupseriesArchive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class GroupSeriesServiceArchImp implements GroupSeriesArchService {

    @Autowired
    private GroupSeriesArchDao groupSeriesDao;

    @Override
    public List<TblgroupseriesArchive> findAllGroupSeries() {
        // TODO Auto-generated method stub
        return groupSeriesDao.findAllGroupSeries();
    }

    @Override
    public void addGroupSeries(TblgroupseriesArchive g) {
        // TODO Auto-generated method stub
        groupSeriesDao.addGroupSeries(g);
    }

    @Override
    public void updateGroupSeries(TblgroupseriesArchive g) {
        // TODO Auto-generated method stub
        groupSeriesDao.updateGroupSeries(g);
    }

    @Override
    public void deleteGroupSeries(long gID) {
        // TODO Auto-generated method stub
        groupSeriesDao.deleteGroupSeries(gID);
    }

    @Override
    public TblgroupseriesArchive getGroupSeries(long gID) {
        return groupSeriesDao.getGroupSeries(gID);
    }


}
