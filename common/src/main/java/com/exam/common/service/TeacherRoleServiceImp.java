package com.exam.common.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.common.dao.TeacherRoleDao;
import com.exam.common.model.Tblteacherrole;


@Service
@Transactional
public class TeacherRoleServiceImp implements TeacherRoleService {

	@Autowired
	private TeacherRoleDao teacherRole;

	@Override
	public List<Tblteacherrole> findAllTeacherRoles() {
		// TODO Auto-generated method stub
		return teacherRole.findAllTeacherRoles();
	}

	@Override
	public void addTeacherRole(Tblteacherrole r) {
		// TODO Auto-generated method stub
		teacherRole.addTeacherRole(r);
	}

	@Override
	public void updateTeacherRole(Tblteacherrole r) {
		// TODO Auto-generated method stub
		teacherRole.updateTeacherRole(r);
	}

	@Override
	public void deleteTeacherRole(long rID) {
		// TODO Auto-generated method stub
		teacherRole.deleteTeacherRole(rID);
	}

	@Override
	public Tblteacherrole getTeacherRole(long rID) {
		// TODO Auto-generated method stub
		return teacherRole.getTeacherRole(rID);
	}

	@Override
	public Tblteacherrole findTeacherRoleByTeacherIDAndRoleID(long teacherID, long roleID) {
		// TODO Auto-generated method stub
		return teacherRole.findTeacherRoleByTeacherIDAndRoleID(teacherID, roleID);
	}
	
	

}
