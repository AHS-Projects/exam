package com.exam.common.service;

import com.exam.common.dao.GroupSeriesDao;
import com.exam.common.model.Tblgroupseries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class GroupSeriesServiceImp implements GroupSeriesService {

    @Autowired
    private GroupSeriesDao groupSeriesDao;

    @Override
    public List<Tblgroupseries> findAllGroupSeries() {
        // TODO Auto-generated method stub
        return groupSeriesDao.findAllGroupSeries();
    }

    @Override
    public void addGroupSeries(Tblgroupseries g) {
        // TODO Auto-generated method stub
        groupSeriesDao.addGroupSeries(g);
    }

    @Override
    public void updateGroupSeries(Tblgroupseries g) {
        // TODO Auto-generated method stub
        groupSeriesDao.updateGroupSeries(g);
    }

    @Override
    public void deleteGroupSeries(long gID) {
        // TODO Auto-generated method stub
        groupSeriesDao.deleteGroupSeries(gID);
    }

    @Override
    public Tblgroupseries getGroupSeriesWithModelsAndSections(long gID) {
        // TODO Auto-generated method stub
        return groupSeriesDao.getGroupSeriesWithModelsAndSections(gID);
    }

    @Override
    public Tblgroupseries findGroupSeriesNotModelFinishByStudentAndModelAndGroup(long groupID, long modelID,
                                                                                 long studentID) {
        // TODO Auto-generated method stub
        return groupSeriesDao.findGroupSeriesNotModelFinishByStudentAndModelAndGroup(groupID, modelID, studentID);
    }

    @Override
    public List<Tblgroupseries> findAllCorrectedModelForArchive() {
        return groupSeriesDao.findAllCorrectedModelForArchive();
    }

    @Override
    public List<Tblgroupseries> findAllNotCorrectModel() {
        // TODO Auto-generated method stub
        return groupSeriesDao.findAllNotCorrectModel();
    }

//	@Override
//	public Tblgroupseries findGroupSeriesNotModelFinishByStudentAndModelAndGroupWithCurrentSession(long groupID, long modelID,
//			long studentID) {
//		// TODO Auto-generated method stub
//		return groupSeriesDao.findGroupSeriesNotModelFinishByStudentAndModelAndGroupWithCurrentSession(groupID, modelID, studentID);
//	}

    @Override
    public Tblgroupseries findGroupSeriesByStudentAndModelAndGroupWithCurrentSession(long groupID, long modelID,
                                                                                     long studentID) {
        // TODO Auto-generated method stub
        return groupSeriesDao.findGroupSeriesByStudentAndModelAndGroupWithCurrentSession(groupID, modelID, studentID);
    }

    public long countAllModelsNotCorrected() {
        return groupSeriesDao.countAllModelsNotCorrected();
    }

    @Override
    public long countAllModelsNotCorrectedByModelType(String modelType) {
        // TODO Auto-generated method stub
        return groupSeriesDao.countAllModelsNotCorrectedByModelType(modelType);
    }

    @Override
    public List<Tblgroupseries> findAllCorrectedModel() {
        // TODO Auto-generated method stub
        return groupSeriesDao.findAllCorrectedModel();
    }

    @Override
    public Tblgroupseries getGroupSeries(long gID) {
        // TODO Auto-generated method stub
        return groupSeriesDao.getGroupSeries(gID);
    }

    @Override
    public Tblgroupseries findGroupSeriesByStudentAndModelAndGroup(long groupID, long modelID, long studentID) {
        // TODO Auto-generated method stub
        return groupSeriesDao.findGroupSeriesByStudentAndModelAndGroup(groupID, modelID, studentID);
    }

    @Override
    public List<Tblgroupseries> getGroubByModelMarkAndModelFinished() {
        // TODO Auto-generated method stub
        return groupSeriesDao.getGroubByModelMarkAndModelFinished();
    }

    @Override
    public List<Tblgroupseries> findAllNotFinishedBeforeLast2Months() {
        return groupSeriesDao.findAllNotFinishedBeforeLast2Months();
    }

}
