package com.exam.common.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.common.dao.VideoDao;
import com.exam.common.model.Tblvideo;

@Service
@Transactional
public class VideoServiceImp implements VideoService {

	@Autowired
	VideoDao videoDao;

	@Override
	public List<Tblvideo> findAllVideos() {
		// TODO Auto-generated method stub
		return videoDao.findAllVideos();
	}

	@Override
	public void addVideo(Tblvideo v) {
		videoDao.addVideo(v);
	}

	@Override
	public void updateVideo(Tblvideo v) {
		videoDao.updateVideo(v);
	}

	@Override
	public void deleteVideo(long vID) {
		videoDao.deleteVideo(vID);
	}

	@Override
	public Tblvideo getVideo(long vID) {
		// TODO Auto-generated method stub
		return videoDao.getVideo(vID);
	}

}
