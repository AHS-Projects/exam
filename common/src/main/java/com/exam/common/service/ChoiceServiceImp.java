package com.exam.common.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.common.dao.ChoiceDao;
import com.exam.common.model.Tblchoice;

@Service
@Transactional
public class ChoiceServiceImp implements  ChoiceService
{

	@Autowired
	private ChoiceDao choiceDao;
	
	@Override
	public List<Tblchoice> findAllChoices() {
		// TODO Auto-generated method stub
		return choiceDao.findAllChoices();
	}

	@Override
	public List<Tblchoice> findAllChoicesByQuestionID(long questionID) {
		// TODO Auto-generated method stub
		return choiceDao.findAllChoicesByQuestionID(questionID);
	}

	@Override
	public void addChoice(Tblchoice c) {
		// TODO Auto-generated method stub
		choiceDao.addChoice(c);
	}

	@Override
	public void updateChoice(Tblchoice c) {
		// TODO Auto-generated method stub
		choiceDao.updateChoice(c);
	}

	@Override
	public void deleteChoice(long cID) {
		// TODO Auto-generated method stub
		choiceDao.deleteChoice(cID);
	}

	@Override
	public Tblchoice getChoice(long cID) {
		// TODO Auto-generated method stub
		return choiceDao.getChoice(cID);
	}

}
