package com.exam.common.service;

import java.util.List;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import com.exam.common.dao.TeacherDao;
import com.exam.common.model.Tblteacher;

import org.springframework.stereotype.Service;

@Service
@Transactional
public class TeacherServiceImp implements TeacherService {

	@Autowired
	private TeacherDao teacherDao;
	
	@Override
	public List<Tblteacher> findAllTeachers() {
		// TODO Auto-generated method stub
		return teacherDao.findAllTeachers();
	}

	@Override
	public void addTeacher(Tblteacher t) {
		// TODO Auto-generated method stub
		teacherDao.addUser(t);
	}

	@Override
	public void updateTeacher(Tblteacher t) {
		// TODO Auto-generated method stub
		teacherDao.updateUser(t);
	}

	@Override
	public void deleteTeacher(long tID) {
		// TODO Auto-generated method stub
		teacherDao.deleteUser(tID);
	}

	@Override
	public Tblteacher getTeacher(long tID) {
		// TODO Auto-generated method stub
		return teacherDao.getUser(tID);
	}

	@Override
	public Tblteacher findTeacherByName(String teacherName) {
		// TODO Auto-generated method stub
		return teacherDao.findTeacherByName(teacherName);
	}

}
