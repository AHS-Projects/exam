package com.exam.common.service;

import java.util.List;

import com.exam.common.model.Tblsection;
import com.exam.common.model.Tblvideo;

public interface VideoService 
{
	
	public List<Tblvideo> findAllVideos();
	public void addVideo(Tblvideo v);
	public void updateVideo(Tblvideo v);
	public void deleteVideo(long vID);
	public Tblvideo getVideo(long vID);

}
