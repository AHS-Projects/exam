package com.exam.common.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.common.dao.GroupDao;
import com.exam.common.model.Tblgroup;

@Transactional
@Service
public class GroupServiceImp implements GroupService {

	@Autowired
	private GroupDao groupDao;
	
	@Override
	public List<Tblgroup> findAllGroup() {
		// TODO Auto-generated method stub
		return groupDao.findAllGroup();
	}

	@Override
	public void addGroup(Tblgroup g) {
		// TODO Auto-generated method stub
		groupDao.addGroup(g);
	}

	@Override
	public void updateGroup(Tblgroup g) {
		// TODO Auto-generated method stub
		groupDao.updateGroup(g);
	}

	@Override
	public void deleteGroup(long gID) {
		// TODO Auto-generated method stub
		groupDao.deleteGroup(gID);
	}

	@Override
	public Tblgroup getGroup(long gID) {
		// TODO Auto-generated method stub
		return groupDao.getGroup(gID);
	}

	@Override
	public List<Tblgroup> findGroupsByStageID(long StageID) {
		// TODO Auto-generated method stub
		return groupDao.findGroupsByStageID(StageID);
	}

}
