package com.exam.common.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.exam.common.dao.QuestionDao;
import com.exam.common.model.Tblquestion;

@Service
@Transactional
public class QuestionServiceImp implements QuestionService {

	@Autowired
	private QuestionDao questionDao;
	
	@Override
	public List<Tblquestion> findAllQuestions() {
		// TODO Auto-generated method stub
		return questionDao.findAllQuestions();
	}

	@Override
	public void addQuestion(Tblquestion q) {
		// TODO Auto-generated method stub
		questionDao.addQuestion(q);
	}

	@Override
	public void updateQuestion(Tblquestion q) {
		// TODO Auto-generated method stub
		questionDao.updateQuestion(q);
	}

	@Override
	public void deleteQuestion(long qID) {
		// TODO Auto-generated method stub
		questionDao.deleteQuestion(qID);
	}

	@Override
	public Tblquestion getQuestion(long qID) {
		// TODO Auto-generated method stub
		return questionDao.getQuestion(qID);
	}

	@Override
	public List<Tblquestion> findQuestionsBySecID(long secID) {
		// TODO Auto-generated method stub
		return questionDao.findQuestionsBySecID(secID);
	}


}
