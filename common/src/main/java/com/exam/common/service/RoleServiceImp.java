package com.exam.common.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.common.dao.RoleDao;
import com.exam.common.model.Tblrole;


@Service
@Transactional
public class RoleServiceImp implements RoleService {

	@Autowired
	private RoleDao roleDao; 
	
	@Override
	public List<Tblrole> findAllRoles() {
		// TODO Auto-generated method stub
		return roleDao.findAllRoles();
	}

	@Override
	public void addRole(Tblrole r) {
		// TODO Auto-generated method stub
		roleDao.addRole(r);  
	}

	@Override
	public void updateRole(Tblrole r) {
		// TODO Auto-generated method stub
		roleDao.updateRole(r);
	}

	@Override
	public void deleteRole(long rID) {
		// TODO Auto-generated method stub
		roleDao.deleteRole(rID); 
	}

	@Override
	public Tblrole getRole(long rID) {
		// TODO Auto-generated method stub
		return roleDao.getRole(rID);
	}



}
