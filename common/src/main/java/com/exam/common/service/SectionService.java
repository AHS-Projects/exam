package com.exam.common.service;

import java.util.List;

import com.exam.common.model.Tblsection;

public interface SectionService 
{
	
	public List<Tblsection> findAllSection();
	public List<Tblsection> findSectionByModelID(long modelID);
	public void addSection(Tblsection s);
	public void updateSection(Tblsection s);
	public void deleteSection(long sID);
	public Tblsection getSection(long sID);

}
