package com.exam.common.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.common.dao.StudentAnswerDao;
import com.exam.common.model.Tblstudentanswer;

@Service
@Transactional
public class StudentAnswerServiceImp implements StudentAnswerService {

	@Autowired
	private StudentAnswerDao studAnswerDao;
	
	@Override
	public List<Tblstudentanswer> findAllStudAnswer() {
		// TODO Auto-generated method stub
		return studAnswerDao.findAllStudAnswer();
	}

	@Override
	public void addStudAnswer(Tblstudentanswer s) {
		// TODO Auto-generated method stub
		studAnswerDao.addStudAnswer(s);
	}

	@Override
	public void updateStudAnswer(Tblstudentanswer s) {
		// TODO Auto-generated method stub
		studAnswerDao.updateStudAnswer(s);
	}

	@Override
	public void deleteStudAnswer(long sID) {
		// TODO Auto-generated method stub
		studAnswerDao.deleteStudAnswer(sID);
	}

	@Override
	public Tblstudentanswer getStudAnswer(long sID) {
		// TODO Auto-generated method stub
		return studAnswerDao.getStudAnswer(sID);
	}

	@Override
	public List<Tblstudentanswer> findAllAnsersByStudentAndModel(long studentID, long modelID) {
		// TODO Auto-generated method stub
		return studAnswerDao.findAllAnsersByStudentAndModel(studentID, modelID);
	}

	@Override
	public List<Tblstudentanswer> findAllAnwsersByStudentAndQuestions(long studentID, long questionID) {
		// TODO Auto-generated method stub
		return studAnswerDao.findAllAnwsersByStudentAndQuestions(studentID, questionID);
	}

	@Override
	public int deleteStudAnswerByStudIDAndModelID(long studID, long modelID) {
		// TODO Auto-generated method stub
		return studAnswerDao.deleteStudAnswerByStudIDAndModelID(studID, modelID);
	}

}
