package com.exam.common.service;

import com.exam.common.dao.ModelDao;
import com.exam.common.dao.QuestionDao;
import com.exam.common.dao.SectionDao;
import com.exam.common.dto.Model;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblmodel;
import com.exam.common.model.Tblquestion;
import com.exam.common.model.Tblsection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class ModelServiceImp implements ModelService {

    @Autowired
    private ModelDao modelDao;
    @Autowired
    private SectionDao sectionDao;
    @Autowired
    private QuestionDao questionDao;

    @Override
    public List<Tblmodel> findAllModels() {
        // TODO Auto-generated method stub
        return modelDao.findAllModels();
    }

    @Override
    public void addModel(Tblmodel m) {
        // TODO Auto-generated method stub
        modelDao.addModel(m);
    }

    @Override
    public void updateModel(Tblmodel m) {
        // TODO Auto-generated method stub
        modelDao.updateModel(m);
    }

    @Override
    public void deleteModel(long mID) {
        // TODO Auto-generated method stub
        modelDao.deleteModel(mID);
    }

    @Override
    public Tblmodel getModelWithSections(long mID) {
        // TODO Auto-generated method stub
        Tblmodel model = modelDao.getModelWithSections(mID);

        List<Tblsection> tblsectionList = model.getTblsectionList();
        for (Tblsection tblsection : tblsectionList) {

            Tblsection section = sectionDao.getSection(tblsection.getSectionID());
            List<Tblquestion> tblquestionList = section.getTblquestionList();
            for (Tblquestion tblquestion : tblquestionList) {

                Tblquestion questionAndStudentAnswer = questionDao.getQuestionAndStudentAnswer(tblquestion.getQuestionID());
                if (questionAndStudentAnswer == null) {
                    tblquestion.setTblstudentanswerList(new ArrayList<>());
                } else {

                }

                if (tblquestion.getQuestionType().equals("choice")) {
                    tblquestion.setTblchoiceList(questionDao.getQuestion(tblquestion.getQuestionID()).getTblchoiceList());
                } else {
                    tblquestion.setTblchoiceList(new ArrayList<>());
                }
            }
            tblsection.setTblquestionList(section.getTblquestionList());
        }

        return modelDao.getModelWithSections(mID);
    }

    @Override
    public List<Tblgroupseries> getstudentPrevModels(long studID) {
        return modelDao.getstudentPrevModels(studID);
    }

    @Override
    public List<Tblgroupseries> getstudentNextModels(long studID) {
        return modelDao.getstudentNextModels(studID);
    }

    @Override
    public long countAllModelsByModelType(String modelType) {
        // TODO Auto-generated method stub
        return modelDao.countAllModelsByModelType(modelType);
    }

    @Override
    public List<Model> findNextModelsByStudName(String studName) {
        return modelDao.findNextModelsByStudName(studName);
    }

    @Override
    public Tblmodel getModelByID(long mID) {
        // TODO Auto-generated method stub
        return modelDao.getModelByID(mID);
    }

    @Override
    public Tblgroupseries getstudentNextModelsWithStuiDAndModelID(long studentID, long modelID) {
        // TODO Auto-generated method stub
        return modelDao.getstudentNextModelsWithStuiDAndModelID(studentID, modelID);
    }

	@Override
	public List<Model> findPrevModelsByStudName(String studName) {
		// TODO Auto-generated method stub
		return modelDao.findPrevModelsByStudName(studName);
	}

}
