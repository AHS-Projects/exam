package com.exam.common.service;

import com.exam.common.model.Tblstage;

import java.util.List;

public interface StageService {
	

    public List<Tblstage> findAllStagesWithStudents();

    public List<Tblstage> findAllStages();

    public void addStage(Tblstage s);

    public void updateStage(Tblstage s);

    public void deleteStage(long sID);

    public Tblstage getStage(long sID);

}
