package com.exam.common.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.common.dao.SectionDao;
import com.exam.common.model.Tblsection;

@Service
@Transactional
public class SectionServiceImp implements SectionService {

	@Autowired
	private SectionDao sectionDao;
	
	@Override
	public List<Tblsection> findAllSection() {
		// TODO Auto-generated method stub
		return sectionDao.findAllSection();
	}

	@Override
	public void addSection(Tblsection s) {
		// TODO Auto-generated method stub
		sectionDao.addSection(s);
	}

	@Override
	public void updateSection(Tblsection s) {
		// TODO Auto-generated method stub
		sectionDao.updateSection(s);
	}

	@Override
	public void deleteSection(long sID) {
		// TODO Auto-generated method stub
		sectionDao.deleteSection(sID);
	}

	@Override
	public Tblsection getSection(long sID) {
		// TODO Auto-generated method stub
		return sectionDao.getSection(sID);
	}

	@Override
	public List<Tblsection> findSectionByModelID(long modelID) {
		// TODO Auto-generated method stub
		return sectionDao.findSectionByModelID(modelID);
	}

}
