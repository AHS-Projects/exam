package com.exam.common.dao;

import java.util.List;
import com.exam.common.model.Tblchoice;



public interface ChoiceDao {
	
	public List<Tblchoice> findAllChoices();
	public List<Tblchoice> findAllChoicesByQuestionID(long questionID);
	public void addChoice(Tblchoice c);
	public void updateChoice(Tblchoice c);
	public void deleteChoice(long cID);
	public Tblchoice getChoice(long cID);


}
