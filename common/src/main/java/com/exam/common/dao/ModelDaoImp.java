package com.exam.common.dao;

import com.exam.common.dto.Model;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblmodel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ModelDaoImp implements ModelDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Tblmodel> findAllModels() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		return session
				.createQuery("SELECT m FROM Tblmodel m join fetch m.createdByUserID cid join fetch m.stageID sid ",
						Tblmodel.class)
				.getResultList();
	}

	@Override
	public void addModel(Tblmodel m) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(m);
	}

	@Override
	public void updateModel(Tblmodel m) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(m);
	}

	@Override
	public void deleteModel(long mID) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Tblmodel m = (Tblmodel) session.get(Tblmodel.class, mID);
		session.delete(m);
	}

	@Override
	public Tblmodel getModelWithSections(long mID) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		// join fetch m.tblsectionList s join fetch s.tblquestionList q
		Query<Tblmodel> query = session.createQuery(
				"SELECT m FROM Tblmodel m left join fetch m.stageID stg left join fetch m.createdByUserID c left join fetch m.updatedByUserID up left join fetch m.tblsectionList sec where m.modelID=:mID",
				Tblmodel.class);
		query.setParameter("mID", mID);

		if (query.getResultList().isEmpty()) {
			return null;
		}

		return query.getSingleResult();
	}

	@Override
	public Tblmodel getModelByID(long mID) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		// join fetch m.tblsectionList s join fetch s.tblquestionList q
		Query<Tblmodel> query = session.createQuery(
				"SELECT m FROM Tblmodel m join fetch m.stageID stg join fetch m.createdByUserID c  where m.modelID=:mID",
				Tblmodel.class);
		query.setParameter("mID", mID);

		if (query.getResultList().isEmpty()) {
			return null;
		}

		return query.getSingleResult();
	}

	@Override
	public List<Tblgroupseries> getstudentPrevModels(long studentID) {
		Session session = sessionFactory.getCurrentSession();

		Query<Tblgroupseries> query = session.createQuery(
				"SELECT s FROM Tblgroupseries s join fetch s.modelID where s.studentID.studentID=:studentID and s.isModelFinished=:isModelFinished and s.isModelCorrected=:isModelCorrected",
				Tblgroupseries.class);

		query.setParameter("studentID", studentID);
		query.setParameter("isModelFinished", 1);
		query.setParameter("isModelCorrected", 1);
		if (query.getResultList().isEmpty()) {
			System.out.println("no prev models list is emptyyyyyy!!!!!!!!");
			return null;
		}

		return query.getResultList();
	}

	@Override
	public List<Tblgroupseries> getstudentNextModels(long studentID) {
		Session session = sessionFactory.getCurrentSession();

		Query<Tblgroupseries> query = session.createQuery(
				"SELECT s FROM Tblgroupseries s join fetch s.modelID where s.studentID.studentID=:studentID and s.isModelFinished=:isModelFinished",
				Tblgroupseries.class);

		query.setParameter("studentID", studentID);
		query.setParameter("isModelFinished", 0);

		if (query.getResultList().isEmpty()) {
			return null;
		}

		return query.getResultList();
	}

	@Override
	public Tblgroupseries getstudentNextModelsWithStuiDAndModelID(long studentID, long modelID) {
		Session session = sessionFactory.getCurrentSession();

		Query<Tblgroupseries> query = session.createQuery(
				"SELECT s FROM Tblgroupseries s join fetch s.modelID where s.studentID.studentID=:studentID and s.modelID.modelID=:modelID and s.isModelFinished=:isModelFinished",
				Tblgroupseries.class);

		query.setParameter("studentID", studentID);
		query.setParameter("isModelFinished", 0);
		query.setParameter("modelID", modelID);
		if (query.getResultList().isEmpty()) {
			return null;
		}

		return query.getSingleResult();
	}

	public long countAllModelsByModelType(String modelType) {
		Session session = sessionFactory.getCurrentSession();
		Query<Long> query = session.createQuery("select count(*) from Tblmodel m where m.modelType=:modelType",
				Long.class);
		query.setParameter("modelType", modelType);
		Long count = (Long) query.uniqueResult();
		return count;
	}

	@Override
	public List<Model> findNextModelsByStudName(String studName) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Query<Model> query = session.createQuery(
				"select new com.exam.common.dto.Model(m.modelID,m.modelName,m.modelType,g.studentMark,m.modelMark,g.startDate,g.finishedDate,m.modelDesc,m.createdDate,m.isPublished) "
						+ "from Tblstudent st,Tblmodel m,Tblgroupseries g where st.studentID=g.studentID.studentID and m.modelID=g.modelID.modelID and st.studentName=:studName and g.isModelFinished=:isModelFinished and m.isPublished=:published",
				Model.class);
		query.setParameter("isModelFinished", 0);
		query.setParameter("published", 1);
		query.setParameter("studName", studName);
		if (query.getResultList().isEmpty()) {
			return new ArrayList<>();
		}

		return query.getResultList();
	}

//	@Override
//	public List<Model> findPrevModelsByStudName(String studName) {
//	    Session session = sessionFactory.getCurrentSession();
//
//	      //  Query<Tblgroupseries> query = session.createQuery("SELECT s FROM Tblgroupseries s join fetch s.modelID where s.studentID.studentID=:studentID and s.isModelFinished=:isModelFinished and s.isModelCorrected=:isModelCorrected", Tblgroupseries.class);
//	        Query<Model> query = session.createQuery(
//	                "select new com.exam.common.dto.Model(m.modelID,m.modelName,m.modelType,g.studentMark,m.modelMark,g.startDate,g.finishedDate,m.modelDesc,m.createdDate,m.isPublished) "
//	                        + "from Tblstudent st,Tblmodel m,Tblgroupseries g where st.studentID=g.studentID.studentID and m.modelID=g.modelID.modelID and st.studentName=:studName and g.isModelFinished=:isModelFinished and m.isPublished=:published",
//	                Model.class);
//	        query.setParameter("isModelFinished", 1);
//	        query.setParameter("published", 1);
//	        query.setParameter("studName", studName);
//
//	        if (query.getResultList().isEmpty()) {
//	            return new ArrayList<>();
//	        }
//
//	        return query.getResultList();
//	}
	@Override
	public List<Model> findPrevModelsByStudName(String studName) {
		Session session = sessionFactory.getCurrentSession();

		// Query<Tblgroupseries> query = session.createQuery("SELECT s FROM
		// Tblgroupseries s join fetch s.modelID where s.studentID.studentID=:studentID
		// and s.isModelFinished=:isModelFinished and
		// s.isModelCorrected=:isModelCorrected", Tblgroupseries.class);
		Query<Model> query = session.createQuery(
				"select new com.exam.common.dto.Model(m.modelID,m.modelName,m.modelType,g.studentMark,m.modelMark,g.startDate,g.finishedDate,m.modelDesc,m.createdDate,m.isPublished) "
						+ "from Tblstudent st,Tblmodel m,Tblgroupseries g where st.studentID=g.studentID.studentID and m.modelID=g.modelID.modelID and st.studentName=:studName and g.isModelFinished=:isModelFinished and m.isPublished=:published and DATEDIFF( CURDATE(), g.startDate) > 15",
				Model.class);
		query.setParameter("isModelFinished", 1);
		query.setParameter("published", 1);
		query.setParameter("studName", studName);

		if (query.getResultList().isEmpty()) {
			return new ArrayList<>();
		}

		return query.getResultList();
	}

}
