package com.exam.common.dao;

import com.exam.common.dto.Model;
import com.exam.common.dto.Student;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblstudent;

import java.util.List;


public interface StudentDao {


    public List<Tblstudent> findAllStudents();

    public List<Tblstudent> findAllStudentsWithCurresession();

    public void addStudent(Tblstudent s);

    public void updateStudent(Tblstudent s);

    public void deleteStudent(long sID);

    public List<Student> findStudentsAndGroupSeriesByGrIDANDStIDAndModID(long groupID, long stageID, long modelID);

    public Tblstudent getStudentWithGroupSerios(long sID);

    public Tblstudent getStudentWithCurrentSession(long sID);

    public Tblstudent findStudentByName(String studentName);

    public List<Tblstudent> findStudenstByGroupID(long groupID);

    public List<Tblgroupseries> getStudentMarks(long studentID);

    public List<Model> getStudentMarksByName(String studentName);

    public List<Tblstudent> findStudenstByGroupIDAndStageID(long groupID, long stageID);

    public long countAllStudentBuStatsus(Integer blocked);

    public List<Tblgroupseries> getAllStudentModels(long studentID);


}
