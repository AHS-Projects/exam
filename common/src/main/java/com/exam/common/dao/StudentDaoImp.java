package com.exam.common.dao;


import com.exam.common.dto.Model;
import com.exam.common.dto.Student;
import com.exam.common.model.Tblgroupseries;
import com.exam.common.model.Tblstudent;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentDaoImp implements StudentDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Tblstudent> findAllStudents() {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("SELECT s FROM Tblstudent s", Tblstudent.class).getResultList();
    }

    @Override
    public void addStudent(Tblstudent s) {

        Session session = sessionFactory.getCurrentSession();
        session.save(s);
    }

    @Override
    public void updateStudent(Tblstudent s) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(s);
    }

    @Override
    public void deleteStudent(long sID) {
        Session session = sessionFactory.getCurrentSession();
        Tblstudent s = (Tblstudent) session.get(Tblstudent.class, sID);
        session.delete(s);
    }


    @Override
    public Tblstudent getStudentWithGroupSerios(long sID) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        Query<Tblstudent> query = session.createQuery("SELECT s FROM Tblstudent s join fetch s.groupID gid join fetch s.stageID sid join fetch s.tblgroupseriesList grpSer join fetch grpSer.modelID mid where s.studentID=:studentID",
                Tblstudent.class);
        query.setParameter("studentID", sID);
        if (query.getResultList().isEmpty()) {
            return null;
        }
        return query.getSingleResult();
    }

    @Override
    public Tblstudent getStudentWithCurrentSession(long sID) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();

        Query<Tblstudent> query = session.createQuery("SELECT s FROM Tblstudent s join fetch s.groupID gid join fetch s.stageID where s.studentID=:studentID",
                Tblstudent.class);
        query.setParameter("studentID", sID);

        if (query.getResultList().isEmpty())
            return null;

        return query.getSingleResult();
    }

    @Override
    public Tblstudent findStudentByName(String studentName) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        Query<Tblstudent> query = session.createQuery("SELECT  s FROM Tblstudent s join fetch s.groupID gid join fetch s.stageID sid where s.studentName=:studentName",
                Tblstudent.class);
        query.setParameter("studentName", studentName);

        if (query.getResultList().isEmpty()) {
            return null;
        }
        return query.getSingleResult();

    }

    @Override
    public List<Tblgroupseries> getStudentMarks(long studentID) {
        Session session = sessionFactory.getCurrentSession();
        System.out.println("student IDDDD: " + studentID);
        Query<Tblgroupseries> query = session.createQuery(
                "SELECT s FROM Tblgroupseries s join fetch s.modelID where s.studentID.studentID=:studentID and s.isModelFinished=:isModelFinished and s.isModelCorrected=:isModelCorrected",
                Tblgroupseries.class);

        query.setParameter("studentID", studentID);
        query.setParameter("isModelFinished", 1);
        query.setParameter("isModelCorrected", 1);
        if (query.getResultList().isEmpty()) {
            return null;
        }

        return query.getResultList();
    }


    @Override
    public List<Tblgroupseries> getAllStudentModels(long studentID) {
        Session session = sessionFactory.getCurrentSession();
        Query<Tblgroupseries> query = session.createQuery(
                "SELECT s FROM Tblgroupseries s join fetch s.modelID where s.studentID.studentID=:studentID",
                Tblgroupseries.class);

        query.setParameter("studentID", studentID);
        if (query.getResultList().isEmpty()) {
            return null;
        }

        return query.getResultList();
    }


    @Override
    public List<Tblstudent> findStudenstByGroupID(long groupID) {
        // TODO Auto-generated method stub

        Session session = sessionFactory.getCurrentSession();
        Query<Tblstudent> query = session.createQuery("SELECT s FROM Tblstudent s where s.groupID.groupID=:groupID",
                Tblstudent.class);
        query.setParameter("groupID", groupID);

        return query.getResultList();

    }

    @Override
    public List<Tblstudent> findStudenstByGroupIDAndStageID(long groupID, long stageID) {
        // TODO Auto-generated method stub


        Session session = sessionFactory.getCurrentSession();
        Query<Tblstudent> query = session.createQuery("SELECT s FROM Tblstudent s join fetch s.stageID sid join fetch s.groupID gid where s.groupID.groupID=:groupID and s.stageID.stageID=:stageID and s.blocked=:blocked ", Tblstudent.class);
        query.setParameter("groupID", groupID);
        query.setParameter("stageID", stageID);
        query.setParameter("blocked", 0);

        return query.getResultList();

    }

    @Override
    public List<Student> findStudentsAndGroupSeriesByGrIDANDStIDAndModID(long groupID, long stageID, long modelID) {
//        Session session = sessionFactory.getCurrentSession();
//        Query<Student> query = session.createQuery(
//                "select new com.exam.common.dto.Student(st.studentID,st.studentName,st.studentEmail,st.studentPhone,st.studentDadPhone,st.blocked,stag.stageName,gr.groupName,m.createdDate,m.isPublished) "
//                        + "from Tblstudent st,Tblmodel m,Tblgroupseries g,Tblgroup gr,Tblstage stag where g.modelID.modelID=:modelID and g.groupID.groupID=:groupID and g.studentID.studentID=st.studentID and st.groupID.groupID=:groupID and st.stageID.stageID=:stageID ",
//                Student.class);
//        query.setParameter("groupID", groupID);
//        query.setParameter("stageID", stageID);
//        query.setParameter("modelID", modelID);
//
//        if (query.getResultList().isEmpty()) {
//            return new ArrayList<>();
//        }
        return new ArrayList<>();
        // return query.getResultList();
    }


    @Override
    public List<Tblstudent> findAllStudentsWithCurresession() {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();

        Query<Tblstudent> query = session.createQuery(
                "SELECT s FROM Tblstudent s join fetch s.stageID join fetch s.groupID  ", Tblstudent.class);

        return query.getResultList();
    }

    public long countAllStudentBuStatsus(Integer blocked) {
        Session session = sessionFactory.getCurrentSession();
        Query<Long> query = session.createQuery("select count(*) from Tblstudent s where s.blocked=:blocked",
                Long.class);
        query.setParameter("blocked", blocked);
        Long count = (Long) query.uniqueResult();
        return count;
    }

    @Override
    public List<Model> getStudentMarksByName(String studentName) {
        Session session = sessionFactory.getCurrentSession();

        //  Query<Tblgroupseries> query = session.createQuery("SELECT s FROM Tblgroupseries s join fetch s.modelID where s.studentID.studentID=:studentID and s.isModelFinished=:isModelFinished and s.isModelCorrected=:isModelCorrected", Tblgroupseries.class);
        Query<Model> query = session.createQuery(
                "select new com.exam.common.dto.Model(m.modelID,m.modelName,m.modelType,g.studentMark,m.modelMark,g.startDate,g.finishedDate,m.modelDesc,m.createdDate,m.isPublished) "
                        + "from Tblstudent st,Tblmodel m,Tblgroupseries g where st.studentID=g.studentID.studentID and m.modelID=g.modelID.modelID and st.studentName=:studName and g.isModelFinished=:isModelFinished and m.isPublished=:published",
                Model.class);
        query.setParameter("isModelFinished", 1);
        query.setParameter("published", 1);
        query.setParameter("studName", studentName);

        if (query.getResultList().isEmpty()) {
            return new ArrayList<>();
        }

        return query.getResultList();
    }

}
