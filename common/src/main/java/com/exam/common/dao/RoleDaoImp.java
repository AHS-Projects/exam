package com.exam.common.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.exam.common.model.Tblrole;

@Repository
public class RoleDaoImp implements RoleDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Tblrole> findAllRoles() {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		return session.createQuery("SELECT r FROM Tblrole r", Tblrole.class).getResultList(); 
	}

	@Override
	public void addRole(Tblrole r) {
		
		Session session=sessionFactory.getCurrentSession();
		session.save(r);    
	}

	@Override
	public void updateRole(Tblrole r) {
		Session session=sessionFactory.getCurrentSession();
		session.saveOrUpdate(r);    
	}

	@Override
	public void deleteRole(long rID) {
		
		Session session=sessionFactory.getCurrentSession();
		Tblrole r=(Tblrole) session.get(Tblrole.class, rID);
		session.delete(r);  

	}

	@Override
	public Tblrole getRole(long rID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Tblrole r=  (Tblrole) session.get(Tblrole.class, rID);
		return r;
	}





}
