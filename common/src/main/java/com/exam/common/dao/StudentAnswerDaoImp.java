package com.exam.common.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.exam.common.model.Tblstudentanswer;

@Repository
public class StudentAnswerDaoImp implements StudentAnswerDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Tblstudentanswer> findAllStudAnswer() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("SELECT s FROM Tblstudentanswer s", Tblstudentanswer.class).getResultList();
	}

	@Override
	public void addStudAnswer(Tblstudentanswer s) {
		// TODO Auto-generated method stub
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(s);
		} catch (ConstraintViolationException e) {
			
	        System.out.println("exception index6 duplication in model=  " + s.getModelID().getModelID());
	        System.out.println("questionID =  " + s.getQuestionID().getQuestionID());
	        System.out.println("studID =  " + s.getStudentID().getStudentID());
	        System.out.println("sectionID =  " + s.getSectionID().getSectionID());


		}
	}

	@Override
	public void updateStudAnswer(Tblstudentanswer s) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.merge("Tblstudentanswer", s);
	}

	@Override
	public void deleteStudAnswer(long sID) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Tblstudentanswer s = (Tblstudentanswer) session.get(Tblstudentanswer.class, sID);
		session.delete(s);
	}

	@Override
	public Tblstudentanswer getStudAnswer(long sID) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Tblstudentanswer s = (Tblstudentanswer) session.get(Tblstudentanswer.class, sID);
		return s;
	}

	@Override
	public List<Tblstudentanswer> findAllAnsersByStudentAndModel(long studentID, long modelID) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Query<Tblstudentanswer> query = session.createQuery(
				"SELECT s FROM Tblstudentanswer s where s.studentID.studentID=:studentID and s.modelID.modelID=:modelID order by s.sectionID.sectionID  ",
				Tblstudentanswer.class);
		query.setParameter("studentID", studentID);
		query.setParameter("modelID", modelID);
		return query.getResultList();
	}

	@Override
	public List<Tblstudentanswer> findAllAnwsersByStudentAndQuestions(long studentID, long questionID) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Query<Tblstudentanswer> query = session.createQuery(
				"SELECT s FROM Tblstudentanswer s where s.studentID.studentID=:studentID and s.questionID.questionID=:questionID order by s.sectionID.sectionID ",
				Tblstudentanswer.class);
		query.setParameter("studentID", studentID);
		query.setParameter("questionID", questionID);
		return query.getResultList();
	}

	@Override
	public int deleteStudAnswerByStudIDAndModelID(long studID, long modelID) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Query q = session.createQuery(
				"delete from Tblstudentanswer s where s.modelID.modelID =:modelID and s.studentID.studentID=:studID");
		q.setParameter("studID", studID);
		q.setParameter("modelID", modelID);
		int countRemoved = q.executeUpdate();
		return countRemoved;
	}

}
