package com.exam.common.dao;

import com.exam.common.model.Tblgroup;

import java.util.List;

;


public interface GroupDao {
	

    public List<Tblgroup> findAllGroup();

    public void addGroup(Tblgroup g);

    public void updateGroup(Tblgroup g);

    public void deleteGroup(long gID);

    public Tblgroup getGroup(long gID);

    public List<Tblgroup> findGroupsByStageID(long StageID);

}
