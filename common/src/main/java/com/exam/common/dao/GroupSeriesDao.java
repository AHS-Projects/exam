package com.exam.common.dao;

import com.exam.common.model.Tblgroupseries;

import java.util.List;


public interface GroupSeriesDao {


    public List<Tblgroupseries> findAllGroupSeries();

    public void addGroupSeries(Tblgroupseries g);

    public void updateGroupSeries(Tblgroupseries g);

    public void deleteGroupSeries(long gID);

    public Tblgroupseries getGroupSeriesWithModelsAndSections(long gID);

    public Tblgroupseries getGroupSeries(long gID);

    public Tblgroupseries findGroupSeriesNotModelFinishByStudentAndModelAndGroup(long groupID, long modelID, long studentID);

    public Tblgroupseries findGroupSeriesByStudentAndModelAndGroup(long groupID, long modelID, long studentID);

    public List<Tblgroupseries> findAllCorrectedModelForArchive();

    //public Tblgroupseries findGroupSeriesNotModelFinishByStudentAndModelAndGroupWithCurrentSession(long groupID,long modelID,long studentID);
    public Tblgroupseries findGroupSeriesByStudentAndModelAndGroupWithCurrentSession(long groupID, long modelID, long studentID);

    public List<Tblgroupseries> findAllNotCorrectModel();

    public List<Tblgroupseries> findAllCorrectedModel();

    public long countAllModelsNotCorrected();

    public long countAllModelsNotCorrectedByModelType(String modelType);

    public List<Tblgroupseries> getGroubByModelMarkAndModelFinished();

    public List<Tblgroupseries> findAllNotFinishedBeforeLast2Months();
}
