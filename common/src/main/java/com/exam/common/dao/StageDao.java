package com.exam.common.dao;

import java.util.List;
import com.exam.common.model.Tblstage;



public interface StageDao {
	
	public List<Tblstage> findAllStagesWithStudents();
	public List<Tblstage> findAllStages();
	public void addStage(Tblstage s);
	public void updateStage(Tblstage s);
	public void deleteStage(long sID);
	public Tblstage getStage(long sID);

}
