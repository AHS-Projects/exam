package com.exam.common.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.exam.common.model.Tblteacher;

@Repository
public class TeacherDaoImp implements TeacherDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Tblteacher> findAllTeachers() {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		return session.createQuery("SELECT t FROM Tblteacher t", Tblteacher.class).getResultList(); 
	}

	@Override
	public void addUser(Tblteacher t) {
		
		Session session=sessionFactory.getCurrentSession();
		session.save(t);    
	}

	@Override
	public void updateUser(Tblteacher t) {
		Session session=sessionFactory.getCurrentSession();
		session.saveOrUpdate(t);    
	}

	@Override
	public void deleteUser(long tID) {
		Session session=sessionFactory.getCurrentSession();
		Tblteacher t=(Tblteacher) session.get(Tblteacher.class, tID);
		session.delete(t);  
	}

	@Override
	public Tblteacher getUser(long tID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Query<Tblteacher> query= session.createQuery("SELECT t FROM Tblteacher t LEFT join fetch t.tblteacherroleList r LEFT join fetch r.roleID where t.teacherID=:teacherID", Tblteacher.class);
		query.setParameter("teacherID", tID);
		if(query.getResultList().isEmpty())
		{
			return null;
		}
		return query.getSingleResult();
	}

	@Override
	public Tblteacher findTeacherByName(String teacherName) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Query<Tblteacher> query= session.createQuery("SELECT t FROM Tblteacher t left join fetch t.tblteacherroleList r left join fetch r.roleID where t.teacherName=:teachername", Tblteacher.class);
		query.setParameter("teachername", teacherName);
		if(query.getResultList().isEmpty())
		{
			return null;
		}
		return query.getSingleResult();
	}

}
