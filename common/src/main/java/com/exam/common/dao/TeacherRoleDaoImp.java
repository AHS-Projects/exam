package com.exam.common.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.exam.common.model.Tblteacherrole;

@Repository
public class TeacherRoleDaoImp implements TeacherRoleDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Tblteacherrole> findAllTeacherRoles() {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		return session.createQuery("SELECT r FROM Tblteacherrole r", Tblteacherrole.class).getResultList(); 
	}

	@Override
	public void addTeacherRole(Tblteacherrole r) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		session.save(r);    
	}

	@Override
	public void updateTeacherRole(Tblteacherrole r) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		session.saveOrUpdate(r);    
	}

	@Override
	public void deleteTeacherRole(long rID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Tblteacherrole r=(Tblteacherrole) session.get(Tblteacherrole.class, rID);
		session.delete(r);  
	}

	@Override
	public Tblteacherrole getTeacherRole(long rID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Tblteacherrole r=  (Tblteacherrole) session.get(Tblteacherrole.class, rID);
		return r;
	}

	@Override
	public Tblteacherrole findTeacherRoleByTeacherIDAndRoleID(long teacherID, long roleID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Query<Tblteacherrole> query= session.createQuery("SELECT ur FROM Tblteacherrole ur where ur.teacherID.teacherID=:teacherID and ur.roleID.roleID=:roleID ", Tblteacherrole.class);
		query.setParameter("teacherID", teacherID);
		query.setParameter("roleID", roleID);
		if(query.getResultList().isEmpty())
		{
			return null;
		}
		return query.getSingleResult();
	}
	

}
