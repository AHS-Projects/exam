package com.exam.common.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.exam.common.model.Tblchoice;


@Repository
public class ChoiceDaoImp implements ChoiceDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Tblchoice> findAllChoices() {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		return session.createQuery("SELECT c FROM Tblchoice c", Tblchoice.class).getResultList();
	}

	@Override
	public List<Tblchoice> findAllChoicesByQuestionID(long questionID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Query<Tblchoice> query= session.createQuery("SELECT c FROM Tblchoice c where c.questionID.questionID=:questionID", Tblchoice.class);
		query.setParameter("questionID", questionID);
		
		return query.getResultList();
	}

	@Override
	public void addChoice(Tblchoice c) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		session.save(c);    
	}

	@Override
	public void updateChoice(Tblchoice c) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		session.saveOrUpdate(c);   
	}

	@Override
	public void deleteChoice(long cID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Tblchoice c=(Tblchoice) session.get(Tblchoice.class, cID);
		session.delete(c);  
	}

	@Override
	public Tblchoice getChoice(long cID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Tblchoice c=  (Tblchoice) session.get(Tblchoice.class, cID);
		return c;
	}

}
