package com.exam.common.dao; 
 
import java.util.List; 
import org.hibernate.Session; 
import org.hibernate.SessionFactory; 
import org.hibernate.query.Query; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Repository; 
import com.exam.common.model.Tblquestion; 

@Repository 
public class QuestionDaoImp implements QuestionDao { 
 
    @Autowired 
    private SessionFactory sessionFactory; 
     
    @Override 
    public List<Tblquestion> findAllQuestions() { 
        // TODO Auto-generated method stub 
        Session session=sessionFactory.getCurrentSession(); 
        return session.createQuery("SELECT q FROM Tblquestion q join fetch q.createdByUserID uid where q.question in (select distinct qu.question from Tblquestion qu) order by q.question  ", Tblquestion.class).getResultList();  
    } 
 
    @Override 
    public void addQuestion(Tblquestion q) { 
        // TODO Auto-generated method stub 
        Session session=sessionFactory.getCurrentSession(); 
        session.save(q);   
    } 
 
    @Override 
    public void updateQuestion(Tblquestion q) { 
        // TODO Auto-generated method stub 
        Session session=sessionFactory.getCurrentSession(); 
        session.saveOrUpdate(q);     
    } 
 
    @Override 
    public void deleteQuestion(long qID) { 
        // TODO Auto-generated method stub 
        Session session=sessionFactory.getCurrentSession(); 
        Tblquestion q=(Tblquestion) session.get(Tblquestion.class, qID); 
        session.delete(q);   
    } 

   
    @Override 
    public Tblquestion getQuestion(long qID) { 
        // TODO Auto-generated method stub 
        Session session=sessionFactory.getCurrentSession(); 
        Query<Tblquestion> query= session.createQuery("SELECT q FROM Tblquestion q left join fetch q.sectionID sec left join fetch sec.modelID mod left join fetch q.tblchoiceList ch  where q.questionID=:questionID", Tblquestion.class); 
        query.setParameter("questionID", qID); 
        if(query.getResultList().isEmpty()) 
        { 
            return null; 
        } 
        else 
        { 
            return query.getSingleResult(); 
        } 
         
    } 
     
    @Override 
    public Tblquestion getQuestionAndStudentAnswer(long qID) { 
        // TODO Auto-generated method stub 
        Session session=sessionFactory.getCurrentSession(); 
        Query<Tblquestion> query= session.createQuery("SELECT q FROM Tblquestion q join fetch q.tblstudentanswerList ch where q.questionID=:questionID", Tblquestion.class); 
        query.setParameter("questionID", qID); 
         
        if(query.getResultList().isEmpty()) 
        { 
            return null; 
        } 
        else 
        { 
            return query.getSingleResult(); 
        } 
         
    } 
 
    @Override 
    public List<Tblquestion> findQuestionsBySecID(long secID) { 
        // TODO Auto-generated method stub 
        Session session=sessionFactory.getCurrentSession(); 
        Query<Tblquestion> query= session.createQuery("SELECT q FROM Tblquestion q join fetch q.createdByUserID where q.sectionID.sectionID=:secID", Tblquestion.class); 
        query.setParameter("secID", secID); 
         
        return query.getResultList(); 
    } 

 
 
} 
 