package com.exam.common.dao;

import com.exam.common.model.TblgroupseriesArchive;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class GroupSeriesArchDaoImp implements GroupSeriesArchDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<TblgroupseriesArchive> findAllGroupSeries() {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("SELECT g FROM TblgroupseriesArchive g", TblgroupseriesArchive.class).getResultList();
    }

    @Override
    public void addGroupSeries(TblgroupseriesArchive g) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        session.save(g);
    }

    @Override
    public void updateGroupSeries(TblgroupseriesArchive g) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        session.merge("TblgroupseriesArchive", g);
    }

    @Override
    public void deleteGroupSeries(long gID) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        TblgroupseriesArchive g = (TblgroupseriesArchive) session.get(TblgroupseriesArchive.class, gID);
        session.delete(g);
    }


    @Override
    public TblgroupseriesArchive getGroupSeries(long gID) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        TblgroupseriesArchive g = (TblgroupseriesArchive) session.get(TblgroupseriesArchive.class, gID);
        return g;


    }


}
