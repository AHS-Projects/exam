package com.exam.common.dao;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import com.exam.common.model.Tblstudentanswer;



public interface StudentAnswerDao {
	
	public List<Tblstudentanswer> findAllStudAnswer();
	public void addStudAnswer(Tblstudentanswer s) ;
	public void updateStudAnswer(Tblstudentanswer s);
	public void deleteStudAnswer(long sID);
	public int deleteStudAnswerByStudIDAndModelID(long studID,long modelID);
	public Tblstudentanswer getStudAnswer(long sID);
	public List<Tblstudentanswer> findAllAnsersByStudentAndModel(long studentID,long modelID);
	public List<Tblstudentanswer> findAllAnwsersByStudentAndQuestions(long studentID,long questionID);

}
