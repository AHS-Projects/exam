package com.exam.common.dao;

import java.util.List;

import com.exam.common.model.Tblteacherrole;



public interface TeacherRoleDao 
{
	public List<Tblteacherrole> findAllTeacherRoles();
	public void addTeacherRole(Tblteacherrole r);
	public void updateTeacherRole(Tblteacherrole r);
	public void deleteTeacherRole(long rID);
	public Tblteacherrole getTeacherRole(long rID);
	public Tblteacherrole findTeacherRoleByTeacherIDAndRoleID(long teacherID,long roleID);

}
