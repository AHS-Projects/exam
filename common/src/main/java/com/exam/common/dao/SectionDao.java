package com.exam.common.dao;

import com.exam.common.model.Tblsection;

import java.util.List;


public interface SectionDao {
	
    public List<Tblsection> findAllSection();

    public List<Tblsection> findSectionByModelID(long modelID);

    public void addSection(Tblsection s);

    public void updateSection(Tblsection s);

    public void deleteSection(long sID);

    public Tblsection getSection(long sID);

}
