package com.exam.common.dao;

import com.exam.common.model.Tblgroupseries;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class GroupSeriesDaoImp implements GroupSeriesDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Tblgroupseries> findAllGroupSeries() {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("SELECT g FROM Tblgroupseries g", Tblgroupseries.class).getResultList();
    }

    @Override
    public void addGroupSeries(Tblgroupseries g) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        session.save(g);
    }

    @Override
    public void updateGroupSeries(Tblgroupseries g) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        session.merge("Tblgroupseries", g);
    }

    @Override
    public void deleteGroupSeries(long gID) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        Tblgroupseries g = (Tblgroupseries) session.get(Tblgroupseries.class, gID);
        session.delete(g);
    }

    @Override
    public Tblgroupseries getGroupSeriesWithModelsAndSections(long gID) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();

        Query<Tblgroupseries> query = session.createQuery("SELECT g FROM Tblgroupseries g JOIN FETCH g.groupID JOIN FETCH g.studentID std JOIN FETCH g.modelID m JOIN FETCH m.tblsectionList s  where g.groupSeriesID=:gID", Tblgroupseries.class);
        query.setParameter("gID", gID);
        if (query.getResultList().isEmpty()) {
            return null;
        }

        return query.getSingleResult();

    }

    @Override
    public Tblgroupseries getGroupSeries(long gID) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        Tblgroupseries g = (Tblgroupseries) session.get(Tblgroupseries.class, gID);
        return g;


    }

    @Override
    public Tblgroupseries findGroupSeriesNotModelFinishByStudentAndModelAndGroup(long groupID, long modelID,
                                                                                 long studentID) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        Query<Tblgroupseries> query = session.createQuery("SELECT g FROM Tblgroupseries g where g.modelID.modelID=:modelID and g.groupID.groupID=:groupID and g.studentID.studentID=:studentID and g.isModelFinished=:isModelFinished "
                , Tblgroupseries.class);
        query.setParameter("modelID", modelID);
        query.setParameter("groupID", groupID);
        query.setParameter("studentID", studentID);
        query.setParameter("isModelFinished", 0);
        if (query.getResultList().isEmpty()) {
            return null;
        }
        return query.getSingleResult();
    }

    @Override
    public Tblgroupseries findGroupSeriesByStudentAndModelAndGroup(long groupID, long modelID,
                                                                   long studentID) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        Query<Tblgroupseries> query = session.createQuery("SELECT g FROM Tblgroupseries g where g.modelID.modelID=:modelID and g.groupID.groupID=:groupID and g.studentID.studentID=:studentID  "
                , Tblgroupseries.class);
        query.setParameter("modelID", modelID);
        query.setParameter("groupID", groupID);
        query.setParameter("studentID", studentID);
        if (query.getResultList().isEmpty()) {
            return null;
        }
        return query.getSingleResult();
    }

    @Override
    public List<Tblgroupseries> findAllNotCorrectModel() {
        // TODO Auto-generated method stub
        Session session = null;
        session = sessionFactory.getCurrentSession();
        Query<Tblgroupseries> query = session.createQuery("SELECT g FROM Tblgroupseries g JOIN FETCH g.modelID m JOIN FETCH g.studentID stu JOIN FETCH g.groupID gr JOIN FETCH gr.stageID st JOIN FETCH g.assignedByTecherID ass  where  g.isModelFinished=:isModelFinished and (g.isModelCorrected=:isModelCorrected or g.isModelCorrected is null) "
                , Tblgroupseries.class);

        query.setParameter("isModelFinished", 1);
        query.setParameter("isModelCorrected", 0);

        return query.getResultList();

    }

    @Override
    public List<Tblgroupseries> findAllCorrectedModel() {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        Query<Tblgroupseries> query = session.createQuery("SELECT g FROM Tblgroupseries g JOIN FETCH g.modelID m JOIN FETCH g.studentID stu JOIN FETCH g.groupID gr JOIN FETCH gr.stageID st JOIN FETCH g.assignedByTecherID ass where  g.isModelFinished=:isModelFinished and g.isModelCorrected=:isModelCorrected and  (MONTH(g.finishedDate) = MONTH(CURRENT_DATE()) or MONTH(g.finishedDate) = MONTH(CURRENT_DATE())-1 ) AND YEAR(g.finishedDate) = YEAR(CURRENT_DATE()) "
                , Tblgroupseries.class);

        query.setParameter("isModelFinished", 1);
        query.setParameter("isModelCorrected", 1);

        return query.getResultList();
    }

//	@Override
//	public Tblgroupseries findGroupSeriesNotModelFinishByStudentAndModelAndGroupWithCurrentSession(long groupID,
//			long modelID, long studentID) {
//		// TODO Auto-generated method stub
//		Session session=sessionFactory.getCurrentSession();
//		Query<Tblgroupseries> query= session.createQuery("SELECT g FROM Tblgroupseries g where g.modelID.modelID=:modelID and g.groupID.groupID=:groupID and g.studentID.studentID=:studentID and g.isModelFinished=:isModelFinished "                
//				, Tblgroupseries.class);
//		query.setParameter("modelID", modelID);
//		query.setParameter("groupID", groupID);
//		query.setParameter("studentID", studentID);
//		query.setParameter("isModelFinished", 0);
//		if(query.getResultList().isEmpty())
//		{
//			return null;
//		}
//		return query.getSingleResult();
//	}


    @Override
    public Tblgroupseries findGroupSeriesByStudentAndModelAndGroupWithCurrentSession(long groupID, long modelID,
                                                                                     long studentID) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        Query<Tblgroupseries> query = session.createQuery("SELECT g FROM Tblgroupseries g where g.modelID.modelID=:modelID and g.groupID.groupID=:groupID and g.studentID.studentID=:studentID"
                , Tblgroupseries.class);
        query.setParameter("modelID", modelID);
        query.setParameter("groupID", groupID);
        query.setParameter("studentID", studentID);
        if (query.getResultList().isEmpty()) {

            return null;
        }
        return query.getSingleResult();
    }

    public long countAllModelsNotCorrected() {
        Session session = sessionFactory.getCurrentSession();
        Query<Long> query = session.createQuery("select count(*) from Tblgroupseries g where g.isModelFinished=:isModelFinished and (g.isModelCorrected=:isModelCorrected or g.isModelCorrected is null)", Long.class);
        query.setParameter("isModelFinished", 1);
        query.setParameter("isModelCorrected", 0);
        Long count = (Long) query.uniqueResult();
        return count;
    }

    @Override
    public long countAllModelsNotCorrectedByModelType(String modelType) {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        Query<Long> query = session.createQuery("select count(*) from Tblgroupseries g where g.isModelFinished=:isModelFinished and (g.isModelCorrected=:isModelCorrected or g.isModelCorrected is null) and g.modelID.modelType=:modelType", Long.class);
        query.setParameter("isModelFinished", 1);
        query.setParameter("isModelCorrected", 0);
        query.setParameter("modelType", modelType);
        Long count = (Long) query.uniqueResult();
        return count;
    }

    @Override
    public List<Tblgroupseries> getGroubByModelMarkAndModelFinished() {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        Query<Tblgroupseries> query = session.createQuery("SELECT g FROM Tblgroupseries g JOIN FETCH g.modelID m JOIN FETCH g.studentID stu JOIN FETCH g.groupID gr JOIN FETCH gr.stageID st JOIN FETCH g.assignedByTecherID ass where  g.isModelFinished=:isModelFinished and g.studentMark=:studentMark "
                , Tblgroupseries.class);

        query.setParameter("isModelFinished", 1);
        query.setParameter("studentMark", 0.0);

        return query.getResultList();
    }

    @Override
    public List<Tblgroupseries> findAllNotFinishedBeforeLast2Months() {
        Session session = sessionFactory.getCurrentSession();
        Query<Tblgroupseries> query = session.createQuery("SELECT g FROM Tblgroupseries g  where  MONTH(g.assignedDate) not in (MONTH(CURRENT_DATE()), MONTH(CURRENT_DATE())-1 ,12) "
                , Tblgroupseries.class);


        return query.getResultList();
    }

    @Override
    public List<Tblgroupseries> findAllCorrectedModelForArchive() {
        // TODO Auto-generated method stub
        Session session = sessionFactory.getCurrentSession();
        Query<Tblgroupseries> query = session.createQuery("SELECT g FROM Tblgroupseries g JOIN FETCH g.modelID m JOIN FETCH g.studentID stu JOIN FETCH g.groupID gr  JOIN FETCH g.assignedByTecherID ass where  g.isModelFinished=:isModelFinished and g.isModelCorrected=:isModelCorrected  "
                , Tblgroupseries.class);

        query.setParameter("isModelFinished", 1);
        query.setParameter("isModelCorrected", 1);

        return query.getResultList();
    }


}
