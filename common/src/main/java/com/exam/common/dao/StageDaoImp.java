package com.exam.common.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.exam.common.model.Tblstage;


@Repository
public class StageDaoImp implements StageDao {
   
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Tblstage> findAllStagesWithStudents() {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		return session.createQuery("SELECT s FROM Tblstage s join fetch s.tblstudentList stus join fetch s.createdByUserID cid ", Tblstage.class).getResultList();
	}
	
	@Override
	public List<Tblstage> findAllStages() {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		return session.createQuery("SELECT s FROM Tblstage s join fetch s.createdByUserID cid ", Tblstage.class).getResultList();
	}

	@Override
	public void addStage(Tblstage s) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		session.save(s);    
	}

	@Override
	public void updateStage(Tblstage s) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		session.saveOrUpdate(s);    
	}

	@Override
	public void deleteStage(long sID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Tblstage s=(Tblstage) session.get(Tblstage.class, sID);
		session.delete(s);  
	}

	@Override
	public Tblstage getStage(long sID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Tblstage s=  (Tblstage) session.get(Tblstage.class, sID);
		return s;
	}

}
