package com.exam.common.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.exam.common.model.Tblvideo;

@Repository
public class VideoDaoImp implements VideoDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Tblvideo> findAllVideos() {
		Session session=sessionFactory.getCurrentSession();
		return session.createQuery("SELECT v FROM Tblvideo v", Tblvideo.class).getResultList();
	}

	@Override
	public void addVideo(Tblvideo v) {
		Session session=sessionFactory.getCurrentSession();
		session.save(v);   
	}

	@Override
	public void updateVideo(Tblvideo v) {
		Session session=sessionFactory.getCurrentSession();
		session.saveOrUpdate(v);  
	}

	@Override
	public void deleteVideo(long vID) {
		Session session=sessionFactory.getCurrentSession();
		Tblvideo v=(Tblvideo) session.get(Tblvideo.class, vID);
		session.delete(v); 
	}

	@Override
	public Tblvideo getVideo(long vID) {
		Session session=sessionFactory.getCurrentSession();
		Tblvideo v=(Tblvideo) session.get(Tblvideo.class, vID);
		return v;
	}

}
