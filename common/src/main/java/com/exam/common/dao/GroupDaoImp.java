package com.exam.common.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.exam.common.model.Tblgroup;

@Repository
public class GroupDaoImp implements GroupDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Tblgroup> findAllGroup() {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		return session.createQuery("SELECT g FROM Tblgroup g", Tblgroup.class).getResultList();
	}

	@Override
	public void addGroup(Tblgroup g) {
		Session session=sessionFactory.getCurrentSession();
		session.save(g);    
	}

	@Override
	public void updateGroup(Tblgroup g) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		session.saveOrUpdate(g); 
	}

	@Override
	public void deleteGroup(long gID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Tblgroup g=(Tblgroup) session.get(Tblgroup.class, gID);
		session.delete(g);  
	}

	@Override
	public Tblgroup getGroup(long gID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Tblgroup g=  (Tblgroup) session.get(Tblgroup.class, gID);
		return g;
	}

	@Override
	public List<Tblgroup> findGroupsByStageID(long StageID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Query<Tblgroup> query= session.createQuery("SELECT g FROM Tblgroup g JOIN FETCH g.stageID JOIN FETCH g.createdByUserID cid where g.stageID.stageID=:stageID", Tblgroup.class);
		query.setParameter("stageID", StageID);
		return query.getResultList();
	}

}
