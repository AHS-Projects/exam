package com.exam.common.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.exam.common.model.Tblsection;

@Repository
public class SectionDaoImp implements SectionDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Tblsection> findAllSection() {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		return session.createQuery("SELECT s FROM Tblsection s", Tblsection.class).getResultList();
	}

	@Override
	public void addSection(Tblsection s) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		session.save(s);    
	}

	@Override
	public void updateSection(Tblsection s) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		session.saveOrUpdate(s);
	}

	@Override
	public void deleteSection(long sID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Tblsection s=(Tblsection) session.get(Tblsection.class, sID);
		session.delete(s);  
	}

	@Override
	public Tblsection getSection(long sID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Query<Tblsection> query= session.createQuery("SELECT s FROM Tblsection s left join fetch s.tblquestionList q left join fetch s.modelID mid where s.sectionID=:sectionID", Tblsection.class);
		query.setParameter("sectionID", sID);
		if(query.getResultList().isEmpty())
		{
			return null;
		}
		else
		{
			return query.getSingleResult();
		}
	}
	


	@Override
	public List<Tblsection> findSectionByModelID(long modelID) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Query<Tblsection> query= session.createQuery("SELECT s FROM Tblsection s join fetch s.createdByUserID uid where s.modelID.modelID=:modelID", Tblsection.class);
		query.setParameter("modelID", modelID);
		
		return query.getResultList();
	}

}
