package com.exam.common.dao;

import com.exam.common.model.Tblquestion;

import java.util.List;


public interface QuestionDao {
	

    public List<Tblquestion> findAllQuestions();

    public List<Tblquestion> findQuestionsBySecID(long secID);

    public void addQuestion(Tblquestion q);

    public void updateQuestion(Tblquestion q);

    public void deleteQuestion(long qID);

    public Tblquestion getQuestion(long qID);

    public Tblquestion getQuestionAndStudentAnswer(long qID);


}
