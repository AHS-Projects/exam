package com.exam.common.dao;

import java.util.List;

import com.exam.common.model.Tblvideo;

public interface VideoDao 
{
	
	public List<Tblvideo> findAllVideos();
	public void addVideo(Tblvideo v);
	public void updateVideo(Tblvideo v);
	public void deleteVideo(long vID);
	public Tblvideo getVideo(long vID);

}
