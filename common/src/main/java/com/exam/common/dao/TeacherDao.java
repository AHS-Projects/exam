package com.exam.common.dao;

import com.exam.common.model.Tblteacher;

import java.util.List;


public interface TeacherDao {
	

    public List<Tblteacher> findAllTeachers();

    public void addUser(Tblteacher t);

    public void updateUser(Tblteacher t);

    public void deleteUser(long tID);

    public Tblteacher getUser(long tID);

    public Tblteacher findTeacherByName(String teacherName);

}
