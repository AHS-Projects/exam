/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *
 * @author khaled
 */
@Entity
@Table(name = "tblvideo")
public class Tblvideo implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "VideoID", nullable = false)
    private Long videoID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300,message="من  فضلك ادخل هذا الحقل")
    @Column(name = "Link", nullable = false, length = 300)
    private String link;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300,message="من  فضلك ادخل هذا الحقل")
    @Column(name = "VideoName", nullable = false, length = 300)
    private String videoName;  
    @Column(name = "VideoDesc", length = 300)
    private String videoDesc;   
    public Tblvideo()
    {}
    
	public Tblvideo(String link, String videoName) {
		this.link = link;
		this.videoName = videoName;
	}

	public Long getVideoID() {
		return videoID;
	}

	public void setVideoID(Long videoID) {
		this.videoID = videoID;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getVideoName() {
		return videoName;
	}

	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}
	
	
	public String getVideoDesc() {
		return videoDesc;
	}

	public void setVideoDesc(String videoDesc) {
		this.videoDesc = videoDesc;
	}

	@Override
	public String toString() {
		return "Tblvideo [videoID=" + videoID + ", link=" + link + ", videoName=" + videoName + ", videoDesc="
				+ videoDesc + "]";
	}

}
