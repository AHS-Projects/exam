/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblteachstud")
public class Tblteachstud implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TeacherStudID", nullable = false)
    private Long teacherStudID;
    @JoinColumn(name = "TeacherID", referencedColumnName = "TeacherID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblteacher teacherID;
    @JoinColumn(name = "StudentID", referencedColumnName = "StudentID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblstudent studentID;

    public Tblteachstud()
    {
    }

    public Tblteachstud(Long teacherStudID)
    {
        this.teacherStudID = teacherStudID;
    }

    public Long getTeacherStudID()
    {
        return teacherStudID;
    }

    public void setTeacherStudID(Long teacherStudID)
    {
        this.teacherStudID = teacherStudID;
    }

    public Tblteacher getTeacherID()
    {
        return teacherID;
    }

    public void setTeacherID(Tblteacher teacherID)
    {
        this.teacherID = teacherID;
    }

    public Tblstudent getStudentID()
    {
        return studentID;
    }

    public void setStudentID(Tblstudent studentID)
    {
        this.studentID = studentID;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (teacherStudID != null ? teacherStudID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblteachstud))
        {
            return false;
        }
        Tblteachstud other = (Tblteachstud) object;
        if ((this.teacherStudID == null && other.teacherStudID != null) || (this.teacherStudID != null && !this.teacherStudID.equals(other.teacherStudID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblteachstud[ teacherStudID=" + teacherStudID + " ]";
    }

}
