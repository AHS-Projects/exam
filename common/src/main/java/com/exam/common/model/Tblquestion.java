/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblquestion")
public class Tblquestion implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "QuestionID", nullable = false)
    private Long questionID;
    @Size(max = 4000)
    @Column(name = "Anwser", length = 4000)
    private String anwser;
    @Size(max = 50)
    @Column(name = "TrueFalseAnswer", length = 50)
    private String trueFalseAnswer;
    @Column(name = "Mark", nullable = false)
    private Double mark;
    @Basic(optional = false)
    @NotNull(message="من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 8000,message="من  فضلك ادخل هذا الحقل")
    @Column(name = "Question", nullable = false, length = 8000)
    private String question;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "QuestionType", nullable = false, length = 45)
    private String questionType;
    @Column(name = "UpdatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "questionID", fetch = FetchType.LAZY)
    private List<Tblstudentanswer> tblstudentanswerList;
    @JoinColumn(name = "SectionID", referencedColumnName = "SectionID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblsection sectionID;
    @JoinColumn(name = "CreatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher createdByUserID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "questionID", fetch = FetchType.LAZY)
    private List<Tblchoice> tblchoiceList;
    @JoinColumn(name = "UpdatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher updatedByUserID;

    public Tblquestion()
    {
    }

    public Tblquestion(Long questionID)
    {
        this.questionID = questionID;
    }

    public Tblquestion(Long questionID, double mark, String question, String questionType)
    {
        this.questionID = questionID;
        this.mark = mark;
        this.question = question;
        this.questionType = questionType;
    }

    public Long getQuestionID()
    {
        return questionID;
    }

    public void setQuestionID(Long questionID)
    {
        this.questionID = questionID;
    }

    public String getAnwser()
    {
        return anwser;
    }

    public void setAnwser(String anwser)
    {
        this.anwser = anwser;
    }

    public Double getMark()
    {
        return mark;
    }

    public void setMark(Double mark)
    {
        this.mark = mark;
    }

    


	public List<Tblstudentanswer> getTblstudentanswerList() {
		return tblstudentanswerList;
	}

	public void setTblstudentanswerList(List<Tblstudentanswer> tblstudentanswerList) {
		this.tblstudentanswerList = tblstudentanswerList;
	}

	public String getQuestion()
    {
        return question;
    }

    public void setQuestion(String question)
    {
        this.question = question;
    }

    public String getQuestionType()
    {
        return questionType;
    }

    public void setQuestionType(String questionType)
    {
        this.questionType = questionType;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Tblteacher getCreatedByUserID()
    {
        return createdByUserID;
    }

    public void setCreatedByUserID(Tblteacher createdByUserID)
    {
        this.createdByUserID = createdByUserID;
    }

    public Tblteacher getUpdatedByUserID()
    {
        return updatedByUserID;
    }

    public void setUpdatedByUserID(Tblteacher updatedByUserID)
    {
        this.updatedByUserID = updatedByUserID;
    }

    public Tblsection getSectionID()
    {
        return sectionID;
    }

    public void setSectionID(Tblsection sectionID)
    {
        this.sectionID = sectionID;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (questionID != null ? questionID.hashCode() : 0);
        return hash;
    }

    public List<Tblchoice> getTblchoiceList() {
		return tblchoiceList;
	}

	public void setTblchoiceList(List<Tblchoice> tblchoiceList) {
		this.tblchoiceList = tblchoiceList;
	}
	

	public String getTrueFalseAnswer() {
		return trueFalseAnswer;
	}

	public void setTrueFalseAnswer(String trueFalseAnswer) {
		this.trueFalseAnswer = trueFalseAnswer;
	}

	@Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblquestion))
        {
            return false;
        }
        Tblquestion other = (Tblquestion) object;
        if ((this.questionID == null && other.questionID != null) || (this.questionID != null && !this.questionID.equals(other.questionID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblquestion[ questionID=" + questionID + " ]";
    }

}
