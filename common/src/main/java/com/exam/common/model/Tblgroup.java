/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblgroup")
public class Tblgroup implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GroupID", nullable = false)
    private Long groupID;
    @Basic(optional = false)
    @NotNull(message="من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 200,message="من  فضلك ادخل هذا الحقل")
    @Column(name = "GroupName", nullable = false, length = 200)
    private String groupName;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UpdatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupID", fetch = FetchType.LAZY)
    private List<Tblstudent> tblstudentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupID", fetch = FetchType.LAZY)
    private List<Tblmodelgroup> tblmodelgroupList;
    @JoinColumn(name = "StageID", referencedColumnName = "StageID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblstage stageID;
    @OneToMany(mappedBy = "groupID", fetch = FetchType.LAZY)
    private List<Tblgroupseries> tblgroupseriesList;
    @JoinColumn(name = "CreatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher createdByUserID;
    @JoinColumn(name = "UpdatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher updatedByUserID;

    public Tblgroup()
    {
    }

    public Tblgroup(Long groupID)
    {
        this.groupID = groupID;
    }

    public Tblgroup(Long groupID, String groupName)
    {
        this.groupID = groupID;
        this.groupName = groupName;
    }

    public Long getGroupID()
    {
        return groupID;
    }

    public void setGroupID(Long groupID)
    {
        this.groupID = groupID;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public Tblteacher getCreatedByUserID()
    {
        return createdByUserID;
    }

    public void setCreatedByUserID(Tblteacher createdByUserID)
    {
        this.createdByUserID = createdByUserID;
    }

    public Tblteacher getUpdatedByUserID()
    {
        return updatedByUserID;
    }

    public void setUpdatedByUserID(Tblteacher updatedByUserID)
    {
        this.updatedByUserID = updatedByUserID;
    }

    public List<Tblstudent> getTblstudentList()
    {
        return tblstudentList;
    }

    public void setTblstudentList(List<Tblstudent> tblstudentList)
    {
        this.tblstudentList = tblstudentList;
    }

    public List<Tblmodelgroup> getTblmodelgroupList()
    {
        return tblmodelgroupList;
    }

    public void setTblmodelgroupList(List<Tblmodelgroup> tblmodelgroupList)
    {
        this.tblmodelgroupList = tblmodelgroupList;
    }

    public Tblstage getStageID()
    {
        return stageID;
    }

    public void setStageID(Tblstage stageID)
    {
        this.stageID = stageID;
    }

    public List<Tblgroupseries> getTblgroupseriesList()
    {
        return tblgroupseriesList;
    }

    public void setTblgroupseriesList(List<Tblgroupseries> tblgroupseriesList)
    {
        this.tblgroupseriesList = tblgroupseriesList;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (groupID != null ? groupID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblgroup))
        {
            return false;
        }
        Tblgroup other = (Tblgroup) object;
        if ((this.groupID == null && other.groupID != null) || (this.groupID != null && !this.groupID.equals(other.groupID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblgroup[ groupID=" + groupID + " ]";
    }

}
