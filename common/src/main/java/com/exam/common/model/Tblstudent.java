/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author hossam
 */
@Entity
@Table(name = "tblstudent")
public class Tblstudent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "StudentID", nullable = false)
    private Long studentID;
    @Basic(optional = false)
    @NotNull(message = "من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 100, message = "من  فضلك ادخل هذا الحقل")
    @Column(name = "StudentName", nullable = false, length = 100)
    private String studentName;
    @Size(max = 45)
    @Column(name = "StudentEmail", length = 45)
    private String studentEmail;
    @Basic(optional = false)
    @NotNull(message = "من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 400, message = "من  فضلك ادخل هذا الحقل")
    @Column(name = "StudentPassword", nullable = false, length = 400)
    private String studentPassword;
    @Size(max = 400)
    @Column(name = "ConfirmationStudentPassword", length = 400)
    private String confirmationStudentPassword;
    @Basic(optional = false)
    @NotNull(message = "من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 45, message = "من  فضلك ادخل هذا الحقل")
    @Column(name = "StudentPhone", nullable = false, length = 45)
    private String studentPhone;
    @Size(max = 45)
    @Column(name = "StudentDadPhone", length = 45)
    private String studentDadPhone;
    @Column(name = "StudentAge")
    private Integer studentAge;
    @Column(name = "Blocked")
    private Integer blocked;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UpdatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "studentID", fetch = FetchType.LAZY)
    private List<Tblstudentanswer> tblstudentanswerList;
    @NotNull(message = "من  فضلك ادخل هذا الحقل")
    @JoinColumn(name = "StageID", referencedColumnName = "StageID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblstage stageID;
    @NotNull(message = "من  فضلك ادخل هذا الحقل")
    @JoinColumn(name = "GroupID", referencedColumnName = "GroupID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblgroup groupID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "studentID", fetch = FetchType.LAZY)
    private List<Tblgroupseries> tblgroupseriesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "studentID", fetch = FetchType.LAZY)
    private List<Tblteachstud> tblteachstudList;
    @JoinColumn(name = "CreatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher createdByUserID;
    @JoinColumn(name = "UpdatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher updatedByUserID;

    public Tblstudent() {
    }

    public Tblstudent(Long studentID) {
        this.studentID = studentID;
    }

    public Tblstudent(Long studentID, String studentName, String studentPassword, String studentPhone) {
        this.studentID = studentID;
        this.studentName = studentName;
        this.studentPassword = studentPassword;
        this.studentPhone = studentPhone;
    }

    public Long getStudentID() {
        return studentID;
    }

    public void setStudentID(Long studentID) {
        this.studentID = studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public List<Tblstudentanswer> getTblstudentanswerList() {
        return tblstudentanswerList;
    }

    public void setTblstudentanswerList(List<Tblstudentanswer> tblstudentanswerList) {
        this.tblstudentanswerList = tblstudentanswerList;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentPassword() {
        return studentPassword;
    }

    public void setStudentPassword(String studentPassword) {
        this.studentPassword = studentPassword;
    }

    public String getConfirmationStudentPassword() {
        return confirmationStudentPassword;
    }

    public void setConfirmationStudentPassword(String confirmationStudentPassword) {
        this.confirmationStudentPassword = confirmationStudentPassword;
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public void setStudentPhone(String studentPhone) {
        this.studentPhone = studentPhone;
    }

    public String getStudentDadPhone() {
        return studentDadPhone;
    }

    public void setStudentDadPhone(String studentDadPhone) {
        this.studentDadPhone = studentDadPhone;
    }

    public Integer getStudentAge() {
        return studentAge;
    }

    public void setStudentAge(Integer studentAge) {
        this.studentAge = studentAge;
    }

    public Integer getBlocked() {
        return blocked;
    }

    public void setBlocked(Integer blocked) {
        this.blocked = blocked;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Tblteacher getCreatedByUserID() {
        return createdByUserID;
    }

    public void setCreatedByUserID(Tblteacher createdByUserID) {
        this.createdByUserID = createdByUserID;
    }

    public Tblteacher getUpdatedByUserID() {
        return updatedByUserID;
    }

    public void setUpdatedByUserID(Tblteacher updatedByUserID) {
        this.updatedByUserID = updatedByUserID;
    }

    public Tblstage getStageID() {
        return stageID;
    }

    public void setStageID(Tblstage stageID) {
        this.stageID = stageID;
    }

    public Tblgroup getGroupID() {
        return groupID;
    }

    public void setGroupID(Tblgroup groupID) {
        this.groupID = groupID;
    }

    public List<Tblgroupseries> getTblgroupseriesList() {
        return tblgroupseriesList;
    }

    public void setTblgroupseriesList(List<Tblgroupseries> tblgroupseriesList) {
        this.tblgroupseriesList = tblgroupseriesList;
    }

    public List<Tblteachstud> getTblteachstudList() {
        return tblteachstudList;
    }

    public void setTblteachstudList(List<Tblteachstud> tblteachstudList) {
        this.tblteachstudList = tblteachstudList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studentID != null ? studentID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblstudent)) {
            return false;
        }
        Tblstudent other = (Tblstudent) object;
        if ((this.studentID == null && other.studentID != null) || (this.studentID != null && !this.studentID.equals(other.studentID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.test.Tblstudent[ studentID=" + studentID + " ]";
    }

}
