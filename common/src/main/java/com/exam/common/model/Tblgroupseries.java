/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblgroupseries")
public class Tblgroupseries implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GroupSeriesID", nullable = false)
    private Long groupSeriesID;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "StudentMark", precision = 22)
    private Double studentMark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IsModelFinished", nullable = false)
    private Integer isModelFinished;
    @Column(name = "IsModelCorrected" )
    private Integer isModelCorrected;
    @Column(name = "StartDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "FinishedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishedDate;
    @Column(name = "AssignedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date assignedDate;
    @JoinColumn(name = "ModelID", referencedColumnName = "ModelID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblmodel modelID;
    @JoinColumn(name = "GroupID", referencedColumnName = "GroupID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblgroup groupID;
    @JoinColumn(name = "StudentID", referencedColumnName = "StudentID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblstudent studentID;
    @JoinColumn(name = "AssignedByTecherID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher assignedByTecherID;

    public Tblgroupseries()
    {
    }

    public Tblgroupseries(Long groupSeriesID)
    {
        this.groupSeriesID = groupSeriesID;
    }

    public Tblgroupseries(Long groupSeriesID, Integer isModelFinished, Integer isPublished)
    {
        this.groupSeriesID = groupSeriesID;
        this.isModelFinished = isModelFinished;
        
    }

    public Long getGroupSeriesID()
    {
        return groupSeriesID;
    }

    public void setGroupSeriesID(Long groupSeriesID)
    {
        this.groupSeriesID = groupSeriesID;
    }

    public Double getStudentMark()
    {
        return studentMark;
    }

    public void setStudentMark(Double studentMark)
    {
        this.studentMark = studentMark;
    }

    public Integer getIsModelFinished()
    {
        return isModelFinished;
    }

    public void setIsModelFinished(Integer isModelFinished)
    {
        this.isModelFinished = isModelFinished;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getFinishedDate()
    {
        return finishedDate;
    }

    public void setFinishedDate(Date finishedDate)
    {
        this.finishedDate = finishedDate;
    }

    public Date getAssignedDate()
    {
        return assignedDate;
    }

    public void setAssignedDate(Date assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    public Tblmodel getModelID()
    {
        return modelID;
    }

    public void setModelID(Tblmodel modelID)
    {
        this.modelID = modelID;
    }

    public Tblgroup getGroupID()
    {
        return groupID;
    }

    public void setGroupID(Tblgroup groupID)
    {
        this.groupID = groupID;
    }

    public Tblstudent getStudentID()
    {
        return studentID;
    }

    public void setStudentID(Tblstudent studentID)
    {
        this.studentID = studentID;
    }

    public Tblteacher getAssignedByTecherID()
    {
        return assignedByTecherID;
    }
    

    public Integer getIsModelCorrected() {
		return isModelCorrected;
	}

	public void setIsModelCorrected(Integer isModelCorrected) {
		this.isModelCorrected = isModelCorrected;
	}

	public void setAssignedByTecherID(Tblteacher assignedByTecherID)
    {
        this.assignedByTecherID = assignedByTecherID;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (groupSeriesID != null ? groupSeriesID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblgroupseries))
        {
            return false;
        }
        Tblgroupseries other = (Tblgroupseries) object;
        if ((this.groupSeriesID == null && other.groupSeriesID != null) || (this.groupSeriesID != null && !this.groupSeriesID.equals(other.groupSeriesID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblgroupseries[ groupSeriesID=" + groupSeriesID + " ]";
    }

}
