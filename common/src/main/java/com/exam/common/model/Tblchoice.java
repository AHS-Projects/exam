/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblchoice")
public class Tblchoice implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ChoiceID", nullable = false)
    private Long choiceID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "Choice", nullable = false, length = 300)
    private String choice;
    @Column(name = "Ischoosed")
    private Integer ischoosed;
    @JoinColumn(name = "QuestionID", referencedColumnName = "QuestionID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblquestion questionID;

    public Tblchoice()
    {
    }

    public Tblchoice(Long choiceID)
    {
        this.choiceID = choiceID;
    }

    public Tblchoice(Long choiceID, String choice)
    {
        this.choiceID = choiceID;
        this.choice = choice;
    }

    public Long getChoiceID()
    {
        return choiceID;
    }

    public void setChoiceID(Long choiceID)
    {
        this.choiceID = choiceID;
    }

    public String getChoice()
    {
        return choice;
    }

    public void setChoice(String choice)
    {
        this.choice = choice;
    }

    public Integer getIschoosed()
    {
        return ischoosed;
    }

    public void setIschoosed(Integer ischoosed)
    {
        this.ischoosed = ischoosed;
    }

    public Tblquestion getQuestionID()
    {
        return questionID;
    }

    public void setQuestionID(Tblquestion questionID)
    {
        this.questionID = questionID;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (choiceID != null ? choiceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblchoice))
        {
            return false;
        }
        Tblchoice other = (Tblchoice) object;
        if ((this.choiceID == null && other.choiceID != null) || (this.choiceID != null && !this.choiceID.equals(other.choiceID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblchoice[ choiceID=" + choiceID + " ]";
    }

}
