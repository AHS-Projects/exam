/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblstage")
public class Tblstage implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "StageID", nullable = false)
    private Long stageID;
    @Basic(optional = false)
    @NotNull(message="من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 200,message="من  فضلك ادخل هذا الحقل")
    @Column(name = "StageName", nullable = false, length = 200)
    private String stageName;
    @Size(max = 1000)
    @Column(name = "StageDesc", length = 1000)
    private String stageDesc;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UpdatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "stageID", fetch = FetchType.LAZY)
    private List<Tblmodel> tblmodelList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "stageID", fetch = FetchType.LAZY)
    private List<Tblstudent> tblstudentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "stageID", fetch = FetchType.LAZY)
    private List<Tblgroup> tblgroupList;
    @JoinColumn(name = "CreatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher createdByUserID;
    @JoinColumn(name = "UpdatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher updatedByUserID;

    public Tblstage()
    {
    }

    public Tblstage(Long stageID)
    {
        this.stageID = stageID;
    }

    public Tblstage(Long stageID, String stageName)
    {
        this.stageID = stageID;
        this.stageName = stageName;
    }

    public Long getStageID()
    {
        return stageID;
    }

    public void setStageID(Long stageID)
    {
        this.stageID = stageID;
    }

    public String getStageName()
    {
        return stageName;
    }

    public void setStageName(String stageName)
    {
        this.stageName = stageName;
    }

    public String getStageDesc()
    {
        return stageDesc;
    }

    public void setStageDesc(String stageDesc)
    {
        this.stageDesc = stageDesc;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public Tblteacher getCreatedByUserID()
    {
        return createdByUserID;
    }

    public void setCreatedByUserID(Tblteacher createdByUserID)
    {
        this.createdByUserID = createdByUserID;
    }

    public Tblteacher getUpdatedByUserID()
    {
        return updatedByUserID;
    }

    public void setUpdatedByUserID(Tblteacher updatedByUserID)
    {
        this.updatedByUserID = updatedByUserID;
    }

    public List<Tblmodel> getTblmodelList()
    {
        return tblmodelList;
    }

    public void setTblmodelList(List<Tblmodel> tblmodelList)
    {
        this.tblmodelList = tblmodelList;
    }

    public List<Tblstudent> getTblstudentList()
    {
        return tblstudentList;
    }

    public void setTblstudentList(List<Tblstudent> tblstudentList)
    {
        this.tblstudentList = tblstudentList;
    }

    public List<Tblgroup> getTblgroupList()
    {
        return tblgroupList;
    }

    public void setTblgroupList(List<Tblgroup> tblgroupList)
    {
        this.tblgroupList = tblgroupList;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (stageID != null ? stageID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblstage))
        {
            return false;
        }
        Tblstage other = (Tblstage) object;
        if ((this.stageID == null && other.stageID != null) || (this.stageID != null && !this.stageID.equals(other.stageID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblstage[ stageID=" + stageID + " ]";
    }

}
