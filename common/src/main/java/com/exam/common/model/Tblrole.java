/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblrole")
public class Tblrole implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RoleID", nullable = false)
    private Long roleID;
    @Basic(optional = false)
    @NotNull(message="من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 100,message="من  فضلك ادخل هذا الحقل")
    @Column(name = "Role", nullable = false, length = 100)
    private String role;
    @Size(max = 45)
    @Column(name = "RoleDesc", length = 45)
    private String roleDesc;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UpdatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleID", fetch = FetchType.LAZY)
    private List<Tblteacherrole> tblteacherroleList;
    @JoinColumn(name = "CreatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher createdByUserID;
    @JoinColumn(name = "UpdatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher updatedByUserID;

    public Tblrole()
    {
    }

    public Tblrole(Long roleID)
    {
        this.roleID = roleID;
    }

    public Tblrole(Long roleID, String role)
    {
        this.roleID = roleID;
        this.role = role;
    }

    public Long getRoleID()
    {
        return roleID;
    }

    public void setRoleID(Long roleID)
    {
        this.roleID = roleID;
    }

    public String getRole()
    {
        return role;
    }

    public void setRole(String role)
    {
        this.role = role;
    }

    public String getRoleDesc()
    {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc)
    {
        this.roleDesc = roleDesc;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public Tblteacher getCreatedByUserID()
    {
        return createdByUserID;
    }

    public void setCreatedByUserID(Tblteacher createdByUserID)
    {
        this.createdByUserID = createdByUserID;
    }

    public Tblteacher getUpdatedByUserID()
    {
        return updatedByUserID;
    }

    public void setUpdatedByUserID(Tblteacher updatedByUserID)
    {
        this.updatedByUserID = updatedByUserID;
    }


    public List<Tblteacherrole> getTblteacherroleList()
    {
        return tblteacherroleList;
    }

    public void setTblteacherroleList(List<Tblteacherrole> tblteacherroleList)
    {
        this.tblteacherroleList = tblteacherroleList;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (roleID != null ? roleID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblrole))
        {
            return false;
        }
        Tblrole other = (Tblrole) object;
        if ((this.roleID == null && other.roleID != null) || (this.roleID != null && !this.roleID.equals(other.roleID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblrole[ roleID=" + roleID + " ]";
    }

}
