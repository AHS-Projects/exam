/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblmodelgroup")
public class Tblmodelgroup implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ModelGroupID", nullable = false)
    private Long modelGroupID;
    @JoinColumn(name = "ModelID", referencedColumnName = "ModelID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblmodel modelID;
    @JoinColumn(name = "GroupID", referencedColumnName = "GroupID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblgroup groupID;

    public Tblmodelgroup()
    {
    }

    public Tblmodelgroup(Long modelGroupID)
    {
        this.modelGroupID = modelGroupID;
    }

    public Long getModelGroupID()
    {
        return modelGroupID;
    }

    public void setModelGroupID(Long modelGroupID)
    {
        this.modelGroupID = modelGroupID;
    }

    public Tblmodel getModelID()
    {
        return modelID;
    }

    public void setModelID(Tblmodel modelID)
    {
        this.modelID = modelID;
    }

    public Tblgroup getGroupID()
    {
        return groupID;
    }

    public void setGroupID(Tblgroup groupID)
    {
        this.groupID = groupID;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (modelGroupID != null ? modelGroupID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblmodelgroup))
        {
            return false;
        }
        Tblmodelgroup other = (Tblmodelgroup) object;
        if ((this.modelGroupID == null && other.modelGroupID != null) || (this.modelGroupID != null && !this.modelGroupID.equals(other.modelGroupID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblmodelgroup[ modelGroupID=" + modelGroupID + " ]";
    }

}
