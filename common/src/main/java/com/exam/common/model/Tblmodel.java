/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblmodel")
public class Tblmodel implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ModelID", nullable = false)
    private Long modelID;
    @Basic(optional = false)
    @NotNull(message="من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 500,message="من  فضلك ادخل هذا الحقل")
    @Column(name = "ModelName", nullable = false, length = 500)
    private String modelName;
    @Basic(optional = false)
    @NotNull(message="من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 100,message="من  فضلك ادخل هذا الحقل")
    @Column(name = "ModelType", nullable = false, length = 100)
    private String modelType;
    @Size(max = 1000)
    @Column(name = "ModelDesc", length = 1000)
    private String modelDesc;
    @NotNull(message="من  فضلك ادخل هذا الحقل")
    @Column(name = "ModelMark",nullable = false)
    private Double modelMark;
    @Column(name = "ModelTime")
    private Double modelTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IsPublished", nullable = false)
    private Integer isPublished;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UpdatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "StageID", referencedColumnName = "StageID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblstage stageID;
    @OneToMany(mappedBy = "modelID", fetch = FetchType.LAZY)
    private List<Tblsection> tblsectionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "modelID", fetch = FetchType.LAZY)
    private List<Tblmodelgroup> tblmodelgroupList;
    @OneToMany(mappedBy = "modelID", fetch = FetchType.LAZY)
    private List<Tblgroupseries> tblgroupseriesList;
    @JoinColumn(name = "CreatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher createdByUserID;
    @JoinColumn(name = "UpdatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher updatedByUserID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "modelID", fetch = FetchType.LAZY)
    private List<Tblstudentanswer> tblstudentanswerList;

    public Tblmodel()
    {
    }

    public Tblmodel(Long modelID)
    {
        this.modelID = modelID;
    }

    public Tblmodel(Long modelID, String modelName)
    {
        this.modelID = modelID;
        this.modelName = modelName;
    }

    public Long getModelID()
    {
        return modelID;
    }

    public void setModelID(Long modelID)
    {
        this.modelID = modelID;
    }

    public String getModelName()
    {
        return modelName;
    }

    public void setModelName(String modelName)
    {
        this.modelName = modelName;
    }

    public String getModelDesc()
    {
        return modelDesc;
    }

    public void setModelDesc(String modelDesc)
    {
        this.modelDesc = modelDesc;
    }
    

    public Double getModelTime() {
		return modelTime;
	}

	public void setModelTime(Double modelTime) {
		this.modelTime = modelTime;
	}

	public Double getModelMark() {
		return modelMark;
	}

	public void setModelMark(Double modelMark) {
		this.modelMark = modelMark;
	}

	public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public Tblteacher getCreatedByUserID()
    {
        return createdByUserID;
    }

    public void setCreatedByUserID(Tblteacher createdByUserID)
    {
        this.createdByUserID = createdByUserID;
    }

    public Tblteacher getUpdatedByUserID()
    {
        return updatedByUserID;
    }

    public void setUpdatedByUserID(Tblteacher updatedByUserID)
    {
        this.updatedByUserID = updatedByUserID;
    }

    public Tblstage getStageID()
    {
        return stageID;
    }

    public void setStageID(Tblstage stageID)
    {
        this.stageID = stageID;
    }
    public String getModelType() {
		return modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}

    public List<Tblsection> getTblsectionList()
    {
        return tblsectionList;
    }

    public void setTblsectionList(List<Tblsection> tblsectionList)
    {
        this.tblsectionList = tblsectionList;
    }

    public List<Tblmodelgroup> getTblmodelgroupList()
    {
        return tblmodelgroupList;
    }

    public void setTblmodelgroupList(List<Tblmodelgroup> tblmodelgroupList)
    {
        this.tblmodelgroupList = tblmodelgroupList;
    }

    public List<Tblgroupseries> getTblgroupseriesList()
    {
        return tblgroupseriesList;
    }

    public void setTblgroupseriesList(List<Tblgroupseries> tblgroupseriesList)
    {
        this.tblgroupseriesList = tblgroupseriesList;
    }
    
	@Override
    public int hashCode()
    {
        int hash = 0;
        hash += (modelID != null ? modelID.hashCode() : 0);
        return hash;
    }

    public List<Tblstudentanswer> getTblstudentanswerList() {
		return tblstudentanswerList;
	}

	public void setTblstudentanswerList(List<Tblstudentanswer> tblstudentanswerList) {
		this.tblstudentanswerList = tblstudentanswerList;
	}
	

	public Integer getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(Integer isPublished) {
		this.isPublished = isPublished;
	}

	@Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblmodel))
        {
            return false;
        }
        Tblmodel other = (Tblmodel) object;
        if ((this.modelID == null && other.modelID != null) || (this.modelID != null && !this.modelID.equals(other.modelID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblmodel[ modelID=" + modelID + " ]";
    }

}
