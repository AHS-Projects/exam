/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblteacherrole")
public class Tblteacherrole implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TeacherRoleID", nullable = false)
    private Long teacherRoleID;
    @JoinColumn(name = "TeacherID", referencedColumnName = "TeacherID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblteacher teacherID;
    @JoinColumn(name = "RoleID", referencedColumnName = "RoleID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblrole roleID;

    public Tblteacherrole()
    {
    }

    public Tblteacherrole(Long teacherRoleID)
    {
        this.teacherRoleID = teacherRoleID;
    }

    public Long getTeacherRoleID()
    {
        return teacherRoleID;
    }

    public void setTeacherRoleID(Long teacherRoleID)
    {
        this.teacherRoleID = teacherRoleID;
    }

    public Tblteacher getTeacherID()
    {
        return teacherID;
    }

    public void setTeacherID(Tblteacher teacherID)
    {
        this.teacherID = teacherID;
    }

    public Tblrole getRoleID()
    {
        return roleID;
    }

    public void setRoleID(Tblrole roleID)
    {
        this.roleID = roleID;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (teacherRoleID != null ? teacherRoleID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblteacherrole))
        {
            return false;
        }
        Tblteacherrole other = (Tblteacherrole) object;
        if ((this.teacherRoleID == null && other.teacherRoleID != null) || (this.teacherRoleID != null && !this.teacherRoleID.equals(other.teacherRoleID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblteacherrole[ teacherRoleID=" + teacherRoleID + " ]";
    }

}
