/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblsection")
public class Tblsection implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SectionID", nullable = false)
    private Long sectionID;
    @Basic(optional = false)
    @NotNull(message="من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 45,message="من  فضلك ادخل هذا الحقل")
    @Column(name = "SectionName", nullable = false, length = 45)
    private String sectionName;
    @Basic(optional = false)
    @NotNull(message="من  فضلك ادخل هذا الحقل")
    @Column(name = "Mark", nullable = false)
    private Double mark;
    @Column(name = "UpdatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sectionID", fetch = FetchType.LAZY)
    private List<Tblquestion> tblquestionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sectionID", fetch = FetchType.LAZY)
    private List<Tblstudentanswer> tblstudentanswerList;
    @JoinColumn(name = "ModelID", referencedColumnName = "ModelID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblmodel modelID;
    @JoinColumn(name = "CreatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher createdByUserID;
    @JoinColumn(name = "UpdatedByUserID", referencedColumnName = "TeacherID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tblteacher updatedByUserID;

    public Tblsection()
    {
    }

    public Tblsection(Long sectionID)
    {
        this.sectionID = sectionID;
    }

    public Tblsection(Long sectionID, String sectionName, Double mark)
    {
        this.sectionID = sectionID;
        this.sectionName = sectionName;
        this.mark = mark;
    }

    public Long getSectionID()
    {
        return sectionID;
    }

    public void setSectionID(Long sectionID)
    {
        this.sectionID = sectionID;
    }

    public String getSectionName()
    {
        return sectionName;
    }

    public void setSectionName(String sectionName)
    {
        this.sectionName = sectionName;
    }

    public Double getMark()
    {
        return mark;
    }

    public void setMark(Double mark)
    {
        this.mark = mark;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Tblteacher getCreatedByUserID()
    {
        return createdByUserID;
    }

    public void setCreatedByUserID(Tblteacher createdByUserID)
    {
        this.createdByUserID = createdByUserID;
    }

    public Tblteacher getUpdatedByUserID()
    {
        return updatedByUserID;
    }

    public void setUpdatedByUserID(Tblteacher updatedByUserID)
    {
        this.updatedByUserID = updatedByUserID;
    }

    public List<Tblquestion> getTblquestionList()
    {
        return tblquestionList;
    }

    public void setTblquestionList(List<Tblquestion> tblquestionList)
    {
        this.tblquestionList = tblquestionList;
    }

    public Tblmodel getModelID()
    {
        return modelID;
    }

    public void setModelID(Tblmodel modelID)
    {
        this.modelID = modelID;
    }
    

    public List<Tblstudentanswer> getTblstudentanswerList() {
		return tblstudentanswerList;
	}

	public void setTblstudentanswerList(List<Tblstudentanswer> tblstudentanswerList) {
		this.tblstudentanswerList = tblstudentanswerList;
	}

	@Override
    public int hashCode()
    {
        int hash = 0;
        hash += (sectionID != null ? sectionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblsection))
        {
            return false;
        }
        Tblsection other = (Tblsection) object;
        if ((this.sectionID == null && other.sectionID != null) || (this.sectionID != null && !this.sectionID.equals(other.sectionID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblsection[ sectionID=" + sectionID + " ]";
    }

}
