package com.exam.common.model;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author adhs
 */
@Entity
@Table(name = "tblgroupseriesarchive", catalog = "examdb", schema = "", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"ModelID", "GroupID", "StudentID"})})
@NamedQueries({
        @NamedQuery(name = "Tblgroupseriesarchive.findAll", query = "SELECT t FROM TblgroupseriesArchive t"),
        @NamedQuery(name = "Tblgroupseriesarchive.findByModelID", query = "SELECT t FROM TblgroupseriesArchive t WHERE t.tblgroupseriesarchivePK.modelID = :modelID"),
        @NamedQuery(name = "Tblgroupseriesarchive.findByGroupID", query = "SELECT t FROM TblgroupseriesArchive t WHERE t.tblgroupseriesarchivePK.groupID = :groupID"),
        @NamedQuery(name = "Tblgroupseriesarchive.findByStudentID", query = "SELECT t FROM TblgroupseriesArchive t WHERE t.tblgroupseriesarchivePK.studentID = :studentID"),
        @NamedQuery(name = "Tblgroupseriesarchive.findByStudentMark", query = "SELECT t FROM TblgroupseriesArchive t WHERE t.studentMark = :studentMark"),
        @NamedQuery(name = "Tblgroupseriesarchive.findByIsModelFinished", query = "SELECT t FROM TblgroupseriesArchive t WHERE t.isModelFinished = :isModelFinished"),
        @NamedQuery(name = "Tblgroupseriesarchive.findByIsModelCorrected", query = "SELECT t FROM TblgroupseriesArchive t WHERE t.isModelCorrected = :isModelCorrected"),
        @NamedQuery(name = "Tblgroupseriesarchive.findByStartDate", query = "SELECT t FROM TblgroupseriesArchive t WHERE t.startDate = :startDate"),
        @NamedQuery(name = "Tblgroupseriesarchive.findByFinishedDate", query = "SELECT t FROM TblgroupseriesArchive t WHERE t.finishedDate = :finishedDate"),
        @NamedQuery(name = "Tblgroupseriesarchive.findByAssignedDate", query = "SELECT t FROM TblgroupseriesArchive t WHERE t.assignedDate = :assignedDate"),
        @NamedQuery(name = "Tblgroupseriesarchive.findByAssignedByTecherID", query = "SELECT t FROM TblgroupseriesArchive t WHERE t.assignedByTecherID = :assignedByTecherID")})
public class TblgroupseriesArchive implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TblgroupseriesarchivePK tblgroupseriesarchivePK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "StudentMark", precision = 22)
    private Double studentMark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IsModelFinished", nullable = false)
    private Integer isModelFinished;
    @Column(name = "IsModelCorrected")
    private Integer isModelCorrected;
    @Column(name = "StartDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "FinishedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishedDate;
    @Column(name = "AssignedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date assignedDate;
    @Column(name = "AssignedByTecherID")
    private Long assignedByTecherID;

    public TblgroupseriesArchive() {
    }

    public TblgroupseriesarchivePK getTblgroupseriesarchivePK() {
        return tblgroupseriesarchivePK;
    }

    public void setTblgroupseriesarchivePK(TblgroupseriesarchivePK tblgroupseriesarchivePK) {
        this.tblgroupseriesarchivePK = tblgroupseriesarchivePK;
    }

    public Double getStudentMark() {
        return studentMark;
    }

    public void setStudentMark(Double studentMark) {
        this.studentMark = studentMark;
    }

    public Integer getIsModelFinished() {
        return isModelFinished;
    }

    public void setIsModelFinished(Integer isModelFinished) {
        this.isModelFinished = isModelFinished;
    }

    public Integer getIsModelCorrected() {
        return isModelCorrected;
    }

    public void setIsModelCorrected(Integer isModelCorrected) {
        this.isModelCorrected = isModelCorrected;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishedDate() {
        return finishedDate;
    }

    public void setFinishedDate(Date finishedDate) {
        this.finishedDate = finishedDate;
    }

    public Date getAssignedDate() {
        return assignedDate;
    }

    public void setAssignedDate(Date assignedDate) {
        this.assignedDate = assignedDate;
    }

    public Long getAssignedByTecherID() {
        return assignedByTecherID;
    }

    public void setAssignedByTecherID(Long assignedByTecherID) {
        this.assignedByTecherID = assignedByTecherID;
    }
}
