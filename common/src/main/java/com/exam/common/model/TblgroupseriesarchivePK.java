package com.exam.common.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author adhs
 */
@Embeddable
public class TblgroupseriesarchivePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ModelID", nullable = false)
    private long modelID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GroupID", nullable = false)
    private long groupID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "StudentID", nullable = false)
    private long studentID;

    public TblgroupseriesarchivePK() {
    }

    public TblgroupseriesarchivePK(long modelID, long groupID, long studentID) {
        this.modelID = modelID;
        this.groupID = groupID;
        this.studentID = studentID;
    }

    public long getModelID() {
        return modelID;
    }

    public void setModelID(long modelID) {
        this.modelID = modelID;
    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }

    public long getStudentID() {
        return studentID;
    }

    public void setStudentID(long studentID) {
        this.studentID = studentID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) modelID;
        hash += (int) groupID;
        hash += (int) studentID;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblgroupseriesarchivePK)) {
            return false;
        }
        TblgroupseriesarchivePK other = (TblgroupseriesarchivePK) object;
        if (this.modelID != other.modelID) {
            return false;
        }
        if (this.groupID != other.groupID) {
            return false;
        }
        if (this.studentID != other.studentID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.its.etg.common.datasource.TblgroupseriesarchivePK[ modelID=" + modelID + ", groupID=" + groupID + ", studentID=" + studentID + " ]";
    }

}
