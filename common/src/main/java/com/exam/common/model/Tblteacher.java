/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblteacher")
public class Tblteacher implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TeacherID", nullable = false)
    private Long teacherID;
    @Basic(optional = false)
    @NotNull(message="من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 100,message="من  فضلك ادخل هذا الحقل")
    @Column(name = "TeacherName", nullable = false, length = 100)
    private String teacherName;
    @Basic(optional = false)
    @NotNull(message="من  فضلك ادخل هذا الحقل")
    @Size(min = 1, max = 400,message="من  فضلك ادخل هذا الحقل")
    @Column(name = "TeacherPassword", nullable = false, length = 400)
    private String teacherPassword;
    @Size(max = 400)
    @Column(name = "ConfirmationTeacherPassword", length = 400)
    private String confirmationTeacherPassword;
    @Size(max = 100)
    @Column(name = "TeacherType", length = 100)
    private String teacherType;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UpdatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teacherID", fetch = FetchType.LAZY)
    private List<Tblteachstud> tblteachstudList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teacherID", fetch = FetchType.LAZY)
    private List<Tblteacherrole> tblteacherroleList;
    @OneToMany(mappedBy = "createdByUserID", fetch = FetchType.LAZY)
    private List<Tblmodel> tblmodelList;
    @OneToMany(mappedBy = "updatedByUserID", fetch = FetchType.LAZY)
    private List<Tblmodel> tblmodelList1;
    @OneToMany(mappedBy = "createdByUserID", fetch = FetchType.LAZY)
    private List<Tblstage> tblstageList;
    @OneToMany(mappedBy = "updatedByUserID", fetch = FetchType.LAZY)
    private List<Tblstage> tblstageList1;
    @OneToMany(mappedBy = "createdByUserID", fetch = FetchType.LAZY)
    private List<Tblstudent> tblstudentList;
    @OneToMany(mappedBy = "updatedByUserID", fetch = FetchType.LAZY)
    private List<Tblstudent> tblstudentList1;
    @OneToMany(mappedBy = "createdByUserID", fetch = FetchType.LAZY)
    private List<Tblsection> tblsectionList;
    @OneToMany(mappedBy = "updatedByUserID", fetch = FetchType.LAZY)
    private List<Tblsection> tblsectionList1;
    @OneToMany(mappedBy = "createdByUserID", fetch = FetchType.LAZY)
    private List<Tblgroup> tblgroupList;
    @OneToMany(mappedBy = "updatedByUserID", fetch = FetchType.LAZY)
    private List<Tblgroup> tblgroupList1;
    @OneToMany(mappedBy = "createdByUserID", fetch = FetchType.LAZY)
    private List<Tblquestion> tblquestionList;
    @OneToMany(mappedBy = "updatedByUserID", fetch = FetchType.LAZY)
    private List<Tblquestion> tblquestionList1;
    @OneToMany(mappedBy = "assignedByTecherID", fetch = FetchType.LAZY)
    private List<Tblgroupseries> tblgroupseriesList;


    public Tblteacher()
    {
    }

    public Tblteacher(Long teacherID)
    {
        this.teacherID = teacherID;
    }

    public Tblteacher(Long teacherID, String teacherName)
    {
        this.teacherID = teacherID;
        this.teacherName = teacherName;
    }

    public Long getTeacherID()
    {
        return teacherID;
    }

    public void setTeacherID(Long teacherID)
    {
        this.teacherID = teacherID;
    }

    public String getTeacherName()
    {
        return teacherName;
    }
    

    public String getTeacherPassword() {
		return teacherPassword;
	}

	public void setTeacherPassword(String teacherPassword) {
		this.teacherPassword = teacherPassword;
	}

	public String getConfirmationTeacherPassword() {
		return confirmationTeacherPassword;
	}

	public void setConfirmationTeacherPassword(String confirmationTeacherPassword) {
		this.confirmationTeacherPassword = confirmationTeacherPassword;
	}

	public void setTeacherName(String teacherName)
    {
        this.teacherName = teacherName;
    }

    public String getTeacherType()
    {
        return teacherType;
    }

    public void setTeacherType(String teacherType)
    {
        this.teacherType = teacherType;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }


    public List<Tblteachstud> getTblteachstudList()
    {
        return tblteachstudList;
    }

    public List<Tblgroupseries> getTblgroupseriesList() {
		return tblgroupseriesList;
	}

	public void setTblgroupseriesList(List<Tblgroupseries> tblgroupseriesList) {
		this.tblgroupseriesList = tblgroupseriesList;
	}

	public void setTblteachstudList(List<Tblteachstud> tblteachstudList)
    {
        this.tblteachstudList = tblteachstudList;
    }

    public List<Tblteacherrole> getTblteacherroleList()
    {
        return tblteacherroleList;
    }

    public void setTblteacherroleList(List<Tblteacherrole> tblteacherroleList)
    {
        this.tblteacherroleList = tblteacherroleList;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (teacherID != null ? teacherID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblteacher))
        {
            return false;
        }
        Tblteacher other = (Tblteacher) object;
        if ((this.teacherID == null && other.teacherID != null) || (this.teacherID != null && !this.teacherID.equals(other.teacherID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblteacher[ teacherID=" + teacherID + " ]";
    }

	public List<Tblmodel> getTblmodelList() {
		return tblmodelList;
	}

	public void setTblmodelList(List<Tblmodel> tblmodelList) {
		this.tblmodelList = tblmodelList;
	}

	public List<Tblmodel> getTblmodelList1() {
		return tblmodelList1;
	}

	public void setTblmodelList1(List<Tblmodel> tblmodelList1) {
		this.tblmodelList1 = tblmodelList1;
	}

	public List<Tblstage> getTblstageList() {
		return tblstageList;
	}

	public void setTblstageList(List<Tblstage> tblstageList) {
		this.tblstageList = tblstageList;
	}

	public List<Tblstage> getTblstageList1() {
		return tblstageList1;
	}

	public void setTblstageList1(List<Tblstage> tblstageList1) {
		this.tblstageList1 = tblstageList1;
	}

	public List<Tblstudent> getTblstudentList() {
		return tblstudentList;
	}

	public void setTblstudentList(List<Tblstudent> tblstudentList) {
		this.tblstudentList = tblstudentList;
	}

	public List<Tblstudent> getTblstudentList1() {
		return tblstudentList1;
	}

	public void setTblstudentList1(List<Tblstudent> tblstudentList1) {
		this.tblstudentList1 = tblstudentList1;
	}

	public List<Tblsection> getTblsectionList() {
		return tblsectionList;
	}

	public void setTblsectionList(List<Tblsection> tblsectionList) {
		this.tblsectionList = tblsectionList;
	}

	public List<Tblsection> getTblsectionList1() {
		return tblsectionList1;
	}

	public void setTblsectionList1(List<Tblsection> tblsectionList1) {
		this.tblsectionList1 = tblsectionList1;
	}

	public List<Tblgroup> getTblgroupList() {
		return tblgroupList;
	}

	public void setTblgroupList(List<Tblgroup> tblgroupList) {
		this.tblgroupList = tblgroupList;
	}

	public List<Tblgroup> getTblgroupList1() {
		return tblgroupList1;
	}

	public void setTblgroupList1(List<Tblgroup> tblgroupList1) {
		this.tblgroupList1 = tblgroupList1;
	}

	public List<Tblquestion> getTblquestionList() {
		return tblquestionList;
	}

	public void setTblquestionList(List<Tblquestion> tblquestionList) {
		this.tblquestionList = tblquestionList;
	}

	public List<Tblquestion> getTblquestionList1() {
		return tblquestionList1;
	}

	public void setTblquestionList1(List<Tblquestion> tblquestionList1) {
		this.tblquestionList1 = tblquestionList1;
	}
    

}
