/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.common.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;


/**
 *
 * @author hossam
 */
@Entity
@Table(name = "tblstudentanswer")
public class Tblstudentanswer implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "StudentAnswerID", nullable = false)
    private Long studentAnswerID;
    @Size(max = 4000)
    @Column(name = "Answer", length = 4000)
    private String answer;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Mark", precision = 22)
    private Double mark;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @JoinColumn(name = "StudentID", referencedColumnName = "StudentID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblstudent studentID;
    @JoinColumn(name = "QuestionID", referencedColumnName = "QuestionID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblquestion questionID;
    @JoinColumn(name = "SectionID", referencedColumnName = "SectionID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblsection sectionID;
    @JoinColumn(name = "ModelID", referencedColumnName = "ModelID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tblmodel modelID;

    public Tblstudentanswer()
    {
    }

    public Tblstudentanswer(Long studentAnswerID)
    {
        this.studentAnswerID = studentAnswerID;
    }

    public Long getStudentAnswerID()
    {
        return studentAnswerID;
    }

    public void setStudentAnswerID(Long studentAnswerID)
    {
        this.studentAnswerID = studentAnswerID;
    }

    public String getAnswer()
    {
        return answer;
    }

    public void setAnswer(String answer)
    {
        this.answer = answer;
    }

    public Double getMark()
    {
        return mark;
    }

    public void setMark(Double mark)
    {
        this.mark = mark;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Tblstudent getStudentID()
    {
        return studentID;
    }

    public void setStudentID(Tblstudent studentID)
    {
        this.studentID = studentID;
    }

    public Tblquestion getQuestionID()
    {
        return questionID;
    }

    public void setQuestionID(Tblquestion questionID)
    {
        this.questionID = questionID;
    }

    public Tblmodel getModelID()
    {
        return modelID;
    }

    public void setModelID(Tblmodel modelID)
    {
        this.modelID = modelID;
    }
    
    

    public Tblsection getSectionID() {
		return sectionID;
	}

	public void setSectionID(Tblsection sectionID) {
		this.sectionID = sectionID;
	}

	@Override
    public int hashCode()
    {
        int hash = 0;
        hash += (studentAnswerID != null ? studentAnswerID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblstudentanswer))
        {
            return false;
        }
        Tblstudentanswer other = (Tblstudentanswer) object;
        if ((this.studentAnswerID == null && other.studentAnswerID != null) || (this.studentAnswerID != null && !this.studentAnswerID.equals(other.studentAnswerID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.test.Tblstudentanswer[ studentAnswerID=" + studentAnswerID + " ]";
    }

}
